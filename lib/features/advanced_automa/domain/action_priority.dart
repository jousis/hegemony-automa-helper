import 'package:flutter/widgets.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

part 'action_priority.freezed.dart';

@freezed
class ActionPriority with _$ActionPriority {
  const factory ActionPriority({
    required RoleName role,
    required String name,
    required String code,
    required Icon icon,
    required int color,
  }) = _ActionPriority;
}
