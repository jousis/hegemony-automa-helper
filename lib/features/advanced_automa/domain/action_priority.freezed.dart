// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'action_priority.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ActionPriority {
  RoleName get role => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get code => throw _privateConstructorUsedError;
  Icon get icon => throw _privateConstructorUsedError;
  int get color => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ActionPriorityCopyWith<ActionPriority> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActionPriorityCopyWith<$Res> {
  factory $ActionPriorityCopyWith(
          ActionPriority value, $Res Function(ActionPriority) then) =
      _$ActionPriorityCopyWithImpl<$Res, ActionPriority>;
  @useResult
  $Res call({RoleName role, String name, String code, Icon icon, int color});
}

/// @nodoc
class _$ActionPriorityCopyWithImpl<$Res, $Val extends ActionPriority>
    implements $ActionPriorityCopyWith<$Res> {
  _$ActionPriorityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? name = null,
    Object? code = null,
    Object? icon = null,
    Object? color = null,
  }) {
    return _then(_value.copyWith(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      icon: null == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as Icon,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ActionPriorityCopyWith<$Res>
    implements $ActionPriorityCopyWith<$Res> {
  factory _$$_ActionPriorityCopyWith(
          _$_ActionPriority value, $Res Function(_$_ActionPriority) then) =
      __$$_ActionPriorityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({RoleName role, String name, String code, Icon icon, int color});
}

/// @nodoc
class __$$_ActionPriorityCopyWithImpl<$Res>
    extends _$ActionPriorityCopyWithImpl<$Res, _$_ActionPriority>
    implements _$$_ActionPriorityCopyWith<$Res> {
  __$$_ActionPriorityCopyWithImpl(
      _$_ActionPriority _value, $Res Function(_$_ActionPriority) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? name = null,
    Object? code = null,
    Object? icon = null,
    Object? color = null,
  }) {
    return _then(_$_ActionPriority(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      icon: null == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as Icon,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_ActionPriority implements _ActionPriority {
  const _$_ActionPriority(
      {required this.role,
      required this.name,
      required this.code,
      required this.icon,
      required this.color});

  @override
  final RoleName role;
  @override
  final String name;
  @override
  final String code;
  @override
  final Icon icon;
  @override
  final int color;

  @override
  String toString() {
    return 'ActionPriority(role: $role, name: $name, code: $code, icon: $icon, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ActionPriority &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.icon, icon) || other.icon == icon) &&
            (identical(other.color, color) || other.color == color));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, name, code, icon, color);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ActionPriorityCopyWith<_$_ActionPriority> get copyWith =>
      __$$_ActionPriorityCopyWithImpl<_$_ActionPriority>(this, _$identity);
}

abstract class _ActionPriority implements ActionPriority {
  const factory _ActionPriority(
      {required final RoleName role,
      required final String name,
      required final String code,
      required final Icon icon,
      required final int color}) = _$_ActionPriority;

  @override
  RoleName get role;
  @override
  String get name;
  @override
  String get code;
  @override
  Icon get icon;
  @override
  int get color;
  @override
  @JsonKey(ignore: true)
  _$$_ActionPriorityCopyWith<_$_ActionPriority> get copyWith =>
      throw _privateConstructorUsedError;
}
