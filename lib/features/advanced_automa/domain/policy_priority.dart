import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

part 'policy_priority.freezed.dart';

@freezed
class PolicyPriority with _$PolicyPriority {
  const factory PolicyPriority({
    required RoleName role,
    required Policy policy,
  }) = _PolicyPriority;
}
