// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'policy_priority.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PolicyPriority {
  RoleName get role => throw _privateConstructorUsedError;
  Policy get policy => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PolicyPriorityCopyWith<PolicyPriority> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PolicyPriorityCopyWith<$Res> {
  factory $PolicyPriorityCopyWith(
          PolicyPriority value, $Res Function(PolicyPriority) then) =
      _$PolicyPriorityCopyWithImpl<$Res, PolicyPriority>;
  @useResult
  $Res call({RoleName role, Policy policy});

  $PolicyCopyWith<$Res> get policy;
}

/// @nodoc
class _$PolicyPriorityCopyWithImpl<$Res, $Val extends PolicyPriority>
    implements $PolicyPriorityCopyWith<$Res> {
  _$PolicyPriorityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? policy = null,
  }) {
    return _then(_value.copyWith(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      policy: null == policy
          ? _value.policy
          : policy // ignore: cast_nullable_to_non_nullable
              as Policy,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $PolicyCopyWith<$Res> get policy {
    return $PolicyCopyWith<$Res>(_value.policy, (value) {
      return _then(_value.copyWith(policy: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_PolicyPriorityCopyWith<$Res>
    implements $PolicyPriorityCopyWith<$Res> {
  factory _$$_PolicyPriorityCopyWith(
          _$_PolicyPriority value, $Res Function(_$_PolicyPriority) then) =
      __$$_PolicyPriorityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({RoleName role, Policy policy});

  @override
  $PolicyCopyWith<$Res> get policy;
}

/// @nodoc
class __$$_PolicyPriorityCopyWithImpl<$Res>
    extends _$PolicyPriorityCopyWithImpl<$Res, _$_PolicyPriority>
    implements _$$_PolicyPriorityCopyWith<$Res> {
  __$$_PolicyPriorityCopyWithImpl(
      _$_PolicyPriority _value, $Res Function(_$_PolicyPriority) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? policy = null,
  }) {
    return _then(_$_PolicyPriority(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      policy: null == policy
          ? _value.policy
          : policy // ignore: cast_nullable_to_non_nullable
              as Policy,
    ));
  }
}

/// @nodoc

class _$_PolicyPriority implements _PolicyPriority {
  const _$_PolicyPriority({required this.role, required this.policy});

  @override
  final RoleName role;
  @override
  final Policy policy;

  @override
  String toString() {
    return 'PolicyPriority(role: $role, policy: $policy)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PolicyPriority &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.policy, policy) || other.policy == policy));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, policy);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PolicyPriorityCopyWith<_$_PolicyPriority> get copyWith =>
      __$$_PolicyPriorityCopyWithImpl<_$_PolicyPriority>(this, _$identity);
}

abstract class _PolicyPriority implements PolicyPriority {
  const factory _PolicyPriority(
      {required final RoleName role,
      required final Policy policy}) = _$_PolicyPriority;

  @override
  RoleName get role;
  @override
  Policy get policy;
  @override
  @JsonKey(ignore: true)
  _$$_PolicyPriorityCopyWith<_$_PolicyPriority> get copyWith =>
      throw _privateConstructorUsedError;
}
