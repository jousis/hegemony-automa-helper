import 'package:freezed_annotation/freezed_annotation.dart';

part 'ai_card.freezed.dart';

@freezed
class AiCard with _$AiCard {
  const factory AiCard({
    required int id, //the card number on the bottom
    required String filename,
  }) = _AiCard;
}
