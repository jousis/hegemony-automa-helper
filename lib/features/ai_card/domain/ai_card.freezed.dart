// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ai_card.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AiCard {
  int get id =>
      throw _privateConstructorUsedError; //the card number on the bottom
  String get filename => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AiCardCopyWith<AiCard> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AiCardCopyWith<$Res> {
  factory $AiCardCopyWith(AiCard value, $Res Function(AiCard) then) =
      _$AiCardCopyWithImpl<$Res, AiCard>;
  @useResult
  $Res call({int id, String filename});
}

/// @nodoc
class _$AiCardCopyWithImpl<$Res, $Val extends AiCard>
    implements $AiCardCopyWith<$Res> {
  _$AiCardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? filename = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AiCardCopyWith<$Res> implements $AiCardCopyWith<$Res> {
  factory _$$_AiCardCopyWith(_$_AiCard value, $Res Function(_$_AiCard) then) =
      __$$_AiCardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String filename});
}

/// @nodoc
class __$$_AiCardCopyWithImpl<$Res>
    extends _$AiCardCopyWithImpl<$Res, _$_AiCard>
    implements _$$_AiCardCopyWith<$Res> {
  __$$_AiCardCopyWithImpl(_$_AiCard _value, $Res Function(_$_AiCard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? filename = null,
  }) {
    return _then(_$_AiCard(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_AiCard implements _AiCard {
  const _$_AiCard({required this.id, required this.filename});

  @override
  final int id;
//the card number on the bottom
  @override
  final String filename;

  @override
  String toString() {
    return 'AiCard(id: $id, filename: $filename)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AiCard &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.filename, filename) ||
                other.filename == filename));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, filename);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AiCardCopyWith<_$_AiCard> get copyWith =>
      __$$_AiCardCopyWithImpl<_$_AiCard>(this, _$identity);
}

abstract class _AiCard implements AiCard {
  const factory _AiCard(
      {required final int id, required final String filename}) = _$_AiCard;

  @override
  int get id;
  @override //the card number on the bottom
  String get filename;
  @override
  @JsonKey(ignore: true)
  _$$_AiCardCopyWith<_$_AiCard> get copyWith =>
      throw _privateConstructorUsedError;
}
