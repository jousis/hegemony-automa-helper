import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';

abstract class IAiCardRepository {
  List<AiCard> get wcAiCards;
  List<AiCard> get mcAiCards;
  List<AiCard> get ccAiCards;
}
