import 'package:dartz/dartz.dart';
import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';
import 'package:hegemony_automa/features/ai_card/domain/i_ai_card_repository.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: IAiCardRepository)
class AiCardRepository implements IAiCardRepository {
  final Option<List<AiCard>> _wcAiCards = none();
  final Option<List<AiCard>> _mcAiCards = none();
  final Option<List<AiCard>> _ccAiCards = none();

  List<AiCard> resetCardList(String classIdentifier) {
    final aiCards = <AiCard>[];
    for (var i = 1; i < 31; i++) {
      final suffix = i.toString().padLeft(2, '0');
      final aiCard = AiCard(id: i, filename: '$classIdentifier-ai-$suffix.jpg');
      aiCards.add(aiCard);
    }
    return aiCards;
  }

  @override
  List<AiCard> get wcAiCards => _wcAiCards.fold(
        () => resetCardList('wc'),
        id,
      );
  @override
  List<AiCard> get mcAiCards => _mcAiCards.fold(
        () => resetCardList('mc'),
        id,
      );
  @override
  List<AiCard> get ccAiCards => _ccAiCards.fold(
        () => resetCardList('cc'),
        id,
      );
}
