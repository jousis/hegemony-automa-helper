import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

part 'company_card.freezed.dart';

@freezed
class CompanyCard with _$CompanyCard {
  const factory CompanyCard({
    required RoleName role,
    required String filename,
  }) = _CompanyCard;
}
