// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'company_card.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CompanyCard {
  RoleName get role => throw _privateConstructorUsedError;
  String get filename => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CompanyCardCopyWith<CompanyCard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CompanyCardCopyWith<$Res> {
  factory $CompanyCardCopyWith(
          CompanyCard value, $Res Function(CompanyCard) then) =
      _$CompanyCardCopyWithImpl<$Res, CompanyCard>;
  @useResult
  $Res call({RoleName role, String filename});
}

/// @nodoc
class _$CompanyCardCopyWithImpl<$Res, $Val extends CompanyCard>
    implements $CompanyCardCopyWith<$Res> {
  _$CompanyCardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? filename = null,
  }) {
    return _then(_value.copyWith(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CompanyCardCopyWith<$Res>
    implements $CompanyCardCopyWith<$Res> {
  factory _$$_CompanyCardCopyWith(
          _$_CompanyCard value, $Res Function(_$_CompanyCard) then) =
      __$$_CompanyCardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({RoleName role, String filename});
}

/// @nodoc
class __$$_CompanyCardCopyWithImpl<$Res>
    extends _$CompanyCardCopyWithImpl<$Res, _$_CompanyCard>
    implements _$$_CompanyCardCopyWith<$Res> {
  __$$_CompanyCardCopyWithImpl(
      _$_CompanyCard _value, $Res Function(_$_CompanyCard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? filename = null,
  }) {
    return _then(_$_CompanyCard(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_CompanyCard implements _CompanyCard {
  const _$_CompanyCard({required this.role, required this.filename});

  @override
  final RoleName role;
  @override
  final String filename;

  @override
  String toString() {
    return 'CompanyCard(role: $role, filename: $filename)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CompanyCard &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.filename, filename) ||
                other.filename == filename));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, filename);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CompanyCardCopyWith<_$_CompanyCard> get copyWith =>
      __$$_CompanyCardCopyWithImpl<_$_CompanyCard>(this, _$identity);
}

abstract class _CompanyCard implements CompanyCard {
  const factory _CompanyCard(
      {required final RoleName role,
      required final String filename}) = _$_CompanyCard;

  @override
  RoleName get role;
  @override
  String get filename;
  @override
  @JsonKey(ignore: true)
  _$$_CompanyCardCopyWith<_$_CompanyCard> get copyWith =>
      throw _privateConstructorUsedError;
}
