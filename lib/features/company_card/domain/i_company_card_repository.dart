import 'package:hegemony_automa/features/company_card/domain/company_card.dart';

abstract class ICompanyCardRepository {
  //Will return all the NON STARTING company cards
  List<CompanyCard> get mcCompanyCards;
  //Will return all the NON STARTING company cards
  List<CompanyCard> get ccCompanyCards;
}
