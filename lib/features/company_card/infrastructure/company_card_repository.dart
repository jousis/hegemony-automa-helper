import 'package:dartz/dartz.dart';
import 'package:hegemony_automa/features/company_card/domain/company_card.dart';
import 'package:hegemony_automa/features/company_card/domain/i_company_card_repository.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: ICompanyCardRepository)
class CompanyCardRepository implements ICompanyCardRepository {
  //2 starting(not included) + 15 for display
  final Option<List<CompanyCard>> _mcCompanies = none();
  //4 starting(not included) + 24 for display
  final Option<List<CompanyCard>> _ccCompanies = none();

  List<CompanyCard> resetCompanyList(RoleName role, String classIdentifier) {
    final ccCompanies = <CompanyCard>[];
    for (var i = 1; i < 25; i++) {
      final suffix = i.toString().padLeft(2, '0');
      final company = CompanyCard(role: role, filename: '$classIdentifier-company-$suffix.jpg');
      ccCompanies.add(company);
    }
    return ccCompanies;
  }

  @override
  List<CompanyCard> get mcCompanyCards => _mcCompanies.fold(
        () => resetCompanyList(RoleName.middleClass, 'mc'),
        id,
      );
  @override
  List<CompanyCard> get ccCompanyCards => _ccCompanies.fold(
        () => resetCompanyList(RoleName.capitalistClass, 'cc'),
        id,
      );
}
