import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/game/presentation/game_page_scaffold.dart';
import 'package:hegemony_automa/features/game_board/application/bloc/game_board_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/injection.dart';

class GamePage extends StatelessWidget {
  const GamePage({
    super.key,
    required this.child,
  });

  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CcBloc>(
          create: (context) => getIt()..add(const CcEvent.started()),
          lazy: false,
        ),
        BlocProvider<GameBoardBloc>(
          create: (context) => getIt()..add(const GameBoardEvent.started()),
          lazy: false,
        ),
      ],
      child: GamePageScaffold(
        child: child,
      ),
    );
  }
}
