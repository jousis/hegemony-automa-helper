import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class GamePageScaffold extends StatelessWidget {
  const GamePageScaffold({
    super.key,
    required this.child,
  });

  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
      bottomNavigationBar: BottomNavigationBar(
        showUnselectedLabels: false,
        showSelectedLabels: false,
        type: BottomNavigationBarType.shifting,
        items: <BottomNavigationBarItem>[
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.construction,
              color: Colors.red,
            ),
            label: 'Working Class',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.store,
              color: Colors.yellow.shade600,
            ),
            label: 'Middle Class',
          ),
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.work,
              color: Colors.blue,
            ),
            label: 'Capitalist Class',
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.ballot),
            label: 'Policies',
          ),
        ],
        currentIndex: _calculateSelectedIndex(context),
        onTap: (int idx) => _onItemTapped(idx, context),
      ),
    );
  }

  static int _calculateSelectedIndex(BuildContext context) {
    final String location = GoRouterState.of(context).location;
    if (location.startsWith('/game/wc')) {
      return 0;
    }
    if (location.startsWith('/game/mc')) {
      return 1;
    }
    if (location.startsWith('/game/cc')) {
      return 2;
    }
    if (location.startsWith('/game/policies')) {
      return 3;
    }
    return 0;
  }

  void _onItemTapped(int index, BuildContext context) {
    switch (index) {
      case 0:
        GoRouter.of(context).go('/game/wc');
        break;
      case 1:
        GoRouter.of(context).go('/game/mc');
        break;
      case 2:
        GoRouter.of(context).go('/game/cc');
        break;
      case 3:
        GoRouter.of(context).go('/game/policies');
        break;
    }
  }
}
