import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/game_board/domain/game_board.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_facade.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

part 'game_board_bloc.freezed.dart';
part 'game_board_event.dart';
part 'game_board_state.dart';

@injectable
class GameBoardBloc extends Bloc<GameBoardEvent, GameBoardState> {
  GameBoardBloc(
    this._gameBoardFacade,
  ) : super(GameBoardState.initial()) {
    on<GameBoardEvent>(_onGameBoardEvent);
  }

  final IGameBoardFacade _gameBoardFacade;

  FutureOr<void> _onGameBoardEvent(GameBoardEvent event, Emitter<GameBoardState> emit) async {
    await event.map(
      started: (e) => _onStarted(e, emit),
      setScore: (e) => _onSetScore(e, emit),
      changePolicyStatus: (e) => _onChangePolicyStatus(e, emit),
    );
  }

  FutureOr<void> _onStarted(_Started e, Emitter<GameBoardState> emit) async {
    final board = _gameBoardFacade.board;
    emit(
      GameBoardState(gameBoard: board),
    );
  }

  FutureOr<void> _onChangePolicyStatus(_ChangePolicyStatus e, Emitter<GameBoardState> emit) async {
    _gameBoardFacade.changePolicyStatus(e.id, e.status);
    final board = _gameBoardFacade.board;
    emit(
      GameBoardState(gameBoard: board),
    );
  }

  FutureOr<void> _onSetScore(_SetScore e, Emitter<GameBoardState> emit) async {
    _gameBoardFacade.setScore(e.role, e.score);
    final board = _gameBoardFacade.board;
    emit(
      GameBoardState(gameBoard: board),
    );
  }
}
