// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'game_board_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$GameBoardEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(RoleName role, int score) setScore,
    required TResult Function(PolicyId id, PolicyStatus status)
        changePolicyStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(RoleName role, int score)? setScore,
    TResult? Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(RoleName role, int score)? setScore,
    TResult Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_SetScore value) setScore,
    required TResult Function(_ChangePolicyStatus value) changePolicyStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_SetScore value)? setScore,
    TResult? Function(_ChangePolicyStatus value)? changePolicyStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_SetScore value)? setScore,
    TResult Function(_ChangePolicyStatus value)? changePolicyStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameBoardEventCopyWith<$Res> {
  factory $GameBoardEventCopyWith(
          GameBoardEvent value, $Res Function(GameBoardEvent) then) =
      _$GameBoardEventCopyWithImpl<$Res, GameBoardEvent>;
}

/// @nodoc
class _$GameBoardEventCopyWithImpl<$Res, $Val extends GameBoardEvent>
    implements $GameBoardEventCopyWith<$Res> {
  _$GameBoardEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$GameBoardEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'GameBoardEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(RoleName role, int score) setScore,
    required TResult Function(PolicyId id, PolicyStatus status)
        changePolicyStatus,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(RoleName role, int score)? setScore,
    TResult? Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(RoleName role, int score)? setScore,
    TResult Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_SetScore value) setScore,
    required TResult Function(_ChangePolicyStatus value) changePolicyStatus,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_SetScore value)? setScore,
    TResult? Function(_ChangePolicyStatus value)? changePolicyStatus,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_SetScore value)? setScore,
    TResult Function(_ChangePolicyStatus value)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements GameBoardEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$$_SetScoreCopyWith<$Res> {
  factory _$$_SetScoreCopyWith(
          _$_SetScore value, $Res Function(_$_SetScore) then) =
      __$$_SetScoreCopyWithImpl<$Res>;
  @useResult
  $Res call({RoleName role, int score});
}

/// @nodoc
class __$$_SetScoreCopyWithImpl<$Res>
    extends _$GameBoardEventCopyWithImpl<$Res, _$_SetScore>
    implements _$$_SetScoreCopyWith<$Res> {
  __$$_SetScoreCopyWithImpl(
      _$_SetScore _value, $Res Function(_$_SetScore) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? score = null,
  }) {
    return _then(_$_SetScore(
      null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      null == score
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetScore implements _SetScore {
  const _$_SetScore(this.role, this.score);

  @override
  final RoleName role;
  @override
  final int score;

  @override
  String toString() {
    return 'GameBoardEvent.setScore(role: $role, score: $score)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetScore &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.score, score) || other.score == score));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, score);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetScoreCopyWith<_$_SetScore> get copyWith =>
      __$$_SetScoreCopyWithImpl<_$_SetScore>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(RoleName role, int score) setScore,
    required TResult Function(PolicyId id, PolicyStatus status)
        changePolicyStatus,
  }) {
    return setScore(role, score);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(RoleName role, int score)? setScore,
    TResult? Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
  }) {
    return setScore?.call(role, score);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(RoleName role, int score)? setScore,
    TResult Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (setScore != null) {
      return setScore(role, score);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_SetScore value) setScore,
    required TResult Function(_ChangePolicyStatus value) changePolicyStatus,
  }) {
    return setScore(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_SetScore value)? setScore,
    TResult? Function(_ChangePolicyStatus value)? changePolicyStatus,
  }) {
    return setScore?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_SetScore value)? setScore,
    TResult Function(_ChangePolicyStatus value)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (setScore != null) {
      return setScore(this);
    }
    return orElse();
  }
}

abstract class _SetScore implements GameBoardEvent {
  const factory _SetScore(final RoleName role, final int score) = _$_SetScore;

  RoleName get role;
  int get score;
  @JsonKey(ignore: true)
  _$$_SetScoreCopyWith<_$_SetScore> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ChangePolicyStatusCopyWith<$Res> {
  factory _$$_ChangePolicyStatusCopyWith(_$_ChangePolicyStatus value,
          $Res Function(_$_ChangePolicyStatus) then) =
      __$$_ChangePolicyStatusCopyWithImpl<$Res>;
  @useResult
  $Res call({PolicyId id, PolicyStatus status});
}

/// @nodoc
class __$$_ChangePolicyStatusCopyWithImpl<$Res>
    extends _$GameBoardEventCopyWithImpl<$Res, _$_ChangePolicyStatus>
    implements _$$_ChangePolicyStatusCopyWith<$Res> {
  __$$_ChangePolicyStatusCopyWithImpl(
      _$_ChangePolicyStatus _value, $Res Function(_$_ChangePolicyStatus) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? status = null,
  }) {
    return _then(_$_ChangePolicyStatus(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as PolicyId,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PolicyStatus,
    ));
  }
}

/// @nodoc

class _$_ChangePolicyStatus implements _ChangePolicyStatus {
  const _$_ChangePolicyStatus({required this.id, required this.status});

  @override
  final PolicyId id;
  @override
  final PolicyStatus status;

  @override
  String toString() {
    return 'GameBoardEvent.changePolicyStatus(id: $id, status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangePolicyStatus &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.status, status) || other.status == status));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChangePolicyStatusCopyWith<_$_ChangePolicyStatus> get copyWith =>
      __$$_ChangePolicyStatusCopyWithImpl<_$_ChangePolicyStatus>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(RoleName role, int score) setScore,
    required TResult Function(PolicyId id, PolicyStatus status)
        changePolicyStatus,
  }) {
    return changePolicyStatus(id, status);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(RoleName role, int score)? setScore,
    TResult? Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
  }) {
    return changePolicyStatus?.call(id, status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(RoleName role, int score)? setScore,
    TResult Function(PolicyId id, PolicyStatus status)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (changePolicyStatus != null) {
      return changePolicyStatus(id, status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_SetScore value) setScore,
    required TResult Function(_ChangePolicyStatus value) changePolicyStatus,
  }) {
    return changePolicyStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_SetScore value)? setScore,
    TResult? Function(_ChangePolicyStatus value)? changePolicyStatus,
  }) {
    return changePolicyStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_SetScore value)? setScore,
    TResult Function(_ChangePolicyStatus value)? changePolicyStatus,
    required TResult orElse(),
  }) {
    if (changePolicyStatus != null) {
      return changePolicyStatus(this);
    }
    return orElse();
  }
}

abstract class _ChangePolicyStatus implements GameBoardEvent {
  const factory _ChangePolicyStatus(
      {required final PolicyId id,
      required final PolicyStatus status}) = _$_ChangePolicyStatus;

  PolicyId get id;
  PolicyStatus get status;
  @JsonKey(ignore: true)
  _$$_ChangePolicyStatusCopyWith<_$_ChangePolicyStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GameBoardState {
  GameBoard get gameBoard => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GameBoardStateCopyWith<GameBoardState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameBoardStateCopyWith<$Res> {
  factory $GameBoardStateCopyWith(
          GameBoardState value, $Res Function(GameBoardState) then) =
      _$GameBoardStateCopyWithImpl<$Res, GameBoardState>;
  @useResult
  $Res call({GameBoard gameBoard});

  $GameBoardCopyWith<$Res> get gameBoard;
}

/// @nodoc
class _$GameBoardStateCopyWithImpl<$Res, $Val extends GameBoardState>
    implements $GameBoardStateCopyWith<$Res> {
  _$GameBoardStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gameBoard = null,
  }) {
    return _then(_value.copyWith(
      gameBoard: null == gameBoard
          ? _value.gameBoard
          : gameBoard // ignore: cast_nullable_to_non_nullable
              as GameBoard,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $GameBoardCopyWith<$Res> get gameBoard {
    return $GameBoardCopyWith<$Res>(_value.gameBoard, (value) {
      return _then(_value.copyWith(gameBoard: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_GameBoardStateCopyWith<$Res>
    implements $GameBoardStateCopyWith<$Res> {
  factory _$$_GameBoardStateCopyWith(
          _$_GameBoardState value, $Res Function(_$_GameBoardState) then) =
      __$$_GameBoardStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({GameBoard gameBoard});

  @override
  $GameBoardCopyWith<$Res> get gameBoard;
}

/// @nodoc
class __$$_GameBoardStateCopyWithImpl<$Res>
    extends _$GameBoardStateCopyWithImpl<$Res, _$_GameBoardState>
    implements _$$_GameBoardStateCopyWith<$Res> {
  __$$_GameBoardStateCopyWithImpl(
      _$_GameBoardState _value, $Res Function(_$_GameBoardState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gameBoard = null,
  }) {
    return _then(_$_GameBoardState(
      gameBoard: null == gameBoard
          ? _value.gameBoard
          : gameBoard // ignore: cast_nullable_to_non_nullable
              as GameBoard,
    ));
  }
}

/// @nodoc

class _$_GameBoardState implements _GameBoardState {
  const _$_GameBoardState({required this.gameBoard});

  @override
  final GameBoard gameBoard;

  @override
  String toString() {
    return 'GameBoardState(gameBoard: $gameBoard)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GameBoardState &&
            (identical(other.gameBoard, gameBoard) ||
                other.gameBoard == gameBoard));
  }

  @override
  int get hashCode => Object.hash(runtimeType, gameBoard);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GameBoardStateCopyWith<_$_GameBoardState> get copyWith =>
      __$$_GameBoardStateCopyWithImpl<_$_GameBoardState>(this, _$identity);
}

abstract class _GameBoardState implements GameBoardState {
  const factory _GameBoardState({required final GameBoard gameBoard}) =
      _$_GameBoardState;

  @override
  GameBoard get gameBoard;
  @override
  @JsonKey(ignore: true)
  _$$_GameBoardStateCopyWith<_$_GameBoardState> get copyWith =>
      throw _privateConstructorUsedError;
}
