part of 'game_board_bloc.dart';

@freezed
class GameBoardEvent with _$GameBoardEvent {
  const factory GameBoardEvent.started() = _Started;
  const factory GameBoardEvent.setScore(RoleName role, int score) = _SetScore;
  const factory GameBoardEvent.changePolicyStatus({
    required PolicyId id,
    required PolicyStatus status,
  }) = _ChangePolicyStatus;
}
