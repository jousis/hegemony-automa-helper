part of 'game_board_bloc.dart';

@freezed
class GameBoardState with _$GameBoardState {
  const factory GameBoardState({
    required GameBoard gameBoard,
  }) = _GameBoardState;

  factory GameBoardState.initial() => GameBoardState(
        gameBoard: GameBoard.newGame(),
      );
}
