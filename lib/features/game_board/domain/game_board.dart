import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

part 'game_board.freezed.dart';

@freezed
class ScoreTrack with _$ScoreTrack {
  const factory ScoreTrack({
    required RoleName role,
    required int score,
    required int color,
  }) = _Score;
}

@freezed
class GameBoard with _$GameBoard {
  const GameBoard._();
  const factory GameBoard({
    required List<Policy> policies,
    required int round,
    required int taxMultiplier,
    required List<ScoreTrack> scores,
  }) = _GameBoard;

  factory GameBoard.newGame() => GameBoard(
      policies: [
        const Policy(id: PolicyId.fiscalPolicy, number: '1', name: 'Fiscal Policy', status: PolicyStatus.C, color: 0xFF0184B8),
        const Policy(id: PolicyId.laborMarket, number: '2', name: 'Labor market', status: PolicyStatus.B, color: 0xFF7E72A2),
        const Policy(id: PolicyId.taxation, number: '3', name: 'Taxation', status: PolicyStatus.A, color: 0xFFB14F9A),
        const Policy(id: PolicyId.healthcare, number: '4', name: 'Healthcare', status: PolicyStatus.B, color: 0xFFC00E0E),
        const Policy(id: PolicyId.education, number: '5', name: 'Education', status: PolicyStatus.C, color: 0xFFEA8D01),
        const Policy(id: PolicyId.foreignTrade, number: '6', name: 'Foreign Trade', status: PolicyStatus.B, color: 0xFFAB895B),
        const Policy(id: PolicyId.immigration, number: '7', name: 'Immigration', status: PolicyStatus.B, color: 0xFF70706E),
      ],
      round: 1,
      taxMultiplier: 5,
      scores: [
        ScoreTrack(role: RoleName.workingClass, score: 0, color: Colors.red.value),
        ScoreTrack(role: RoleName.middleClass, score: 0, color: Colors.yellow.value),
        ScoreTrack(role: RoleName.capitalistClass, score: 0, color: Colors.blue.value),
      ]);
}
