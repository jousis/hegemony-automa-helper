// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'game_board.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ScoreTrack {
  RoleName get role => throw _privateConstructorUsedError;
  int get score => throw _privateConstructorUsedError;
  int get color => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ScoreTrackCopyWith<ScoreTrack> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ScoreTrackCopyWith<$Res> {
  factory $ScoreTrackCopyWith(
          ScoreTrack value, $Res Function(ScoreTrack) then) =
      _$ScoreTrackCopyWithImpl<$Res, ScoreTrack>;
  @useResult
  $Res call({RoleName role, int score, int color});
}

/// @nodoc
class _$ScoreTrackCopyWithImpl<$Res, $Val extends ScoreTrack>
    implements $ScoreTrackCopyWith<$Res> {
  _$ScoreTrackCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? score = null,
    Object? color = null,
  }) {
    return _then(_value.copyWith(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      score: null == score
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ScoreCopyWith<$Res> implements $ScoreTrackCopyWith<$Res> {
  factory _$$_ScoreCopyWith(_$_Score value, $Res Function(_$_Score) then) =
      __$$_ScoreCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({RoleName role, int score, int color});
}

/// @nodoc
class __$$_ScoreCopyWithImpl<$Res>
    extends _$ScoreTrackCopyWithImpl<$Res, _$_Score>
    implements _$$_ScoreCopyWith<$Res> {
  __$$_ScoreCopyWithImpl(_$_Score _value, $Res Function(_$_Score) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? score = null,
    Object? color = null,
  }) {
    return _then(_$_Score(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      score: null == score
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_Score implements _Score {
  const _$_Score(
      {required this.role, required this.score, required this.color});

  @override
  final RoleName role;
  @override
  final int score;
  @override
  final int color;

  @override
  String toString() {
    return 'ScoreTrack(role: $role, score: $score, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Score &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.score, score) || other.score == score) &&
            (identical(other.color, color) || other.color == color));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, score, color);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ScoreCopyWith<_$_Score> get copyWith =>
      __$$_ScoreCopyWithImpl<_$_Score>(this, _$identity);
}

abstract class _Score implements ScoreTrack {
  const factory _Score(
      {required final RoleName role,
      required final int score,
      required final int color}) = _$_Score;

  @override
  RoleName get role;
  @override
  int get score;
  @override
  int get color;
  @override
  @JsonKey(ignore: true)
  _$$_ScoreCopyWith<_$_Score> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GameBoard {
  List<Policy> get policies => throw _privateConstructorUsedError;
  int get round => throw _privateConstructorUsedError;
  int get taxMultiplier => throw _privateConstructorUsedError;
  List<ScoreTrack> get scores => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GameBoardCopyWith<GameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameBoardCopyWith<$Res> {
  factory $GameBoardCopyWith(GameBoard value, $Res Function(GameBoard) then) =
      _$GameBoardCopyWithImpl<$Res, GameBoard>;
  @useResult
  $Res call(
      {List<Policy> policies,
      int round,
      int taxMultiplier,
      List<ScoreTrack> scores});
}

/// @nodoc
class _$GameBoardCopyWithImpl<$Res, $Val extends GameBoard>
    implements $GameBoardCopyWith<$Res> {
  _$GameBoardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? policies = null,
    Object? round = null,
    Object? taxMultiplier = null,
    Object? scores = null,
  }) {
    return _then(_value.copyWith(
      policies: null == policies
          ? _value.policies
          : policies // ignore: cast_nullable_to_non_nullable
              as List<Policy>,
      round: null == round
          ? _value.round
          : round // ignore: cast_nullable_to_non_nullable
              as int,
      taxMultiplier: null == taxMultiplier
          ? _value.taxMultiplier
          : taxMultiplier // ignore: cast_nullable_to_non_nullable
              as int,
      scores: null == scores
          ? _value.scores
          : scores // ignore: cast_nullable_to_non_nullable
              as List<ScoreTrack>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GameBoardCopyWith<$Res> implements $GameBoardCopyWith<$Res> {
  factory _$$_GameBoardCopyWith(
          _$_GameBoard value, $Res Function(_$_GameBoard) then) =
      __$$_GameBoardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<Policy> policies,
      int round,
      int taxMultiplier,
      List<ScoreTrack> scores});
}

/// @nodoc
class __$$_GameBoardCopyWithImpl<$Res>
    extends _$GameBoardCopyWithImpl<$Res, _$_GameBoard>
    implements _$$_GameBoardCopyWith<$Res> {
  __$$_GameBoardCopyWithImpl(
      _$_GameBoard _value, $Res Function(_$_GameBoard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? policies = null,
    Object? round = null,
    Object? taxMultiplier = null,
    Object? scores = null,
  }) {
    return _then(_$_GameBoard(
      policies: null == policies
          ? _value._policies
          : policies // ignore: cast_nullable_to_non_nullable
              as List<Policy>,
      round: null == round
          ? _value.round
          : round // ignore: cast_nullable_to_non_nullable
              as int,
      taxMultiplier: null == taxMultiplier
          ? _value.taxMultiplier
          : taxMultiplier // ignore: cast_nullable_to_non_nullable
              as int,
      scores: null == scores
          ? _value._scores
          : scores // ignore: cast_nullable_to_non_nullable
              as List<ScoreTrack>,
    ));
  }
}

/// @nodoc

class _$_GameBoard extends _GameBoard {
  const _$_GameBoard(
      {required final List<Policy> policies,
      required this.round,
      required this.taxMultiplier,
      required final List<ScoreTrack> scores})
      : _policies = policies,
        _scores = scores,
        super._();

  final List<Policy> _policies;
  @override
  List<Policy> get policies {
    if (_policies is EqualUnmodifiableListView) return _policies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_policies);
  }

  @override
  final int round;
  @override
  final int taxMultiplier;
  final List<ScoreTrack> _scores;
  @override
  List<ScoreTrack> get scores {
    if (_scores is EqualUnmodifiableListView) return _scores;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_scores);
  }

  @override
  String toString() {
    return 'GameBoard(policies: $policies, round: $round, taxMultiplier: $taxMultiplier, scores: $scores)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GameBoard &&
            const DeepCollectionEquality().equals(other._policies, _policies) &&
            (identical(other.round, round) || other.round == round) &&
            (identical(other.taxMultiplier, taxMultiplier) ||
                other.taxMultiplier == taxMultiplier) &&
            const DeepCollectionEquality().equals(other._scores, _scores));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_policies),
      round,
      taxMultiplier,
      const DeepCollectionEquality().hash(_scores));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GameBoardCopyWith<_$_GameBoard> get copyWith =>
      __$$_GameBoardCopyWithImpl<_$_GameBoard>(this, _$identity);
}

abstract class _GameBoard extends GameBoard {
  const factory _GameBoard(
      {required final List<Policy> policies,
      required final int round,
      required final int taxMultiplier,
      required final List<ScoreTrack> scores}) = _$_GameBoard;
  const _GameBoard._() : super._();

  @override
  List<Policy> get policies;
  @override
  int get round;
  @override
  int get taxMultiplier;
  @override
  List<ScoreTrack> get scores;
  @override
  @JsonKey(ignore: true)
  _$$_GameBoardCopyWith<_$_GameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}
