// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:hegemony_automa/features/game_board/domain/game_board.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_facade.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_repository.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: IGameBoardFacade)
class GameBoardFacade implements IGameBoardFacade {
  GameBoardFacade(
    this._gameBoardRepository,
  );

  Option<GameBoard> _board = none();

  final IGameBoardRepository _gameBoardRepository;
  final _gameBoardUpdatedStreamController = StreamController<bool>.broadcast();

  @override
  get board => _board.fold(
        () => _gameBoardRepository.board,
        id,
      );

  @override
  void changePolicyStatus(PolicyId id, PolicyStatus status) {
    final newBoardPolicies = board.policies.map(
      (e) {
        if (e.id != id) {
          return e;
        }
        return e.copyWith(status: status);
      },
    ).toList();
    _board = some(board.copyWith(policies: newBoardPolicies));
    _gameBoardUpdatedStreamController.sink.add(true);
  }

  @override
  set taxMultiplier(int taxMultiplier) {
    _board = some(board.copyWith(taxMultiplier: taxMultiplier));
    _gameBoardUpdatedStreamController.sink.add(true);
  }

  @override
  void nextRound() {
    final currentRound = board.round;
    if (currentRound >= 5) {
      return;
    }
    _board = some(board.copyWith(round: currentRound + 1));
    _gameBoardUpdatedStreamController.sink.add(true);
  }

  @override
  void resetGame() {
    _gameBoardRepository.resetGame();
    _board = none();
    _gameBoardUpdatedStreamController.sink.add(true);
  }

  @override
  Stream<bool> get gameBoardUpdated => _gameBoardUpdatedStreamController.stream;

  @override
  void setScore(RoleName role, int score) {
    final scores = board.scores.map((s) {
      if (s.role == role) {
        return s.copyWith(score: score);
      }
      return s;
    }).toList();
    _board = some(board.copyWith(scores: scores));
    _gameBoardUpdatedStreamController.sink.add(true);
  }
}
