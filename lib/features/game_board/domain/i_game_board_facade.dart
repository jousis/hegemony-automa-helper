import 'package:hegemony_automa/features/game_board/domain/game_board.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

abstract class IGameBoardFacade {
  GameBoard get board;

  void changePolicyStatus(PolicyId id, PolicyStatus status);
  void setScore(RoleName role, int score);

  void nextRound();
  set taxMultiplier(int taxMultiplier);

  Stream<bool> get gameBoardUpdated;

  void resetGame();
}
