import 'package:hegemony_automa/features/game_board/domain/game_board.dart';

abstract class IGameBoardRepository {
  GameBoard get board;
  void resetGame();
}
