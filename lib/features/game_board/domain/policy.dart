import 'package:freezed_annotation/freezed_annotation.dart';

part 'policy.freezed.dart';

enum PolicyId {
  fiscalPolicy,
  laborMarket,
  taxation,
  healthcare,
  education,
  foreignTrade,
  immigration,
}

enum PolicyStatus { A, B, C }

@freezed
class Policy with _$Policy {
  const factory Policy({
    required PolicyId id,
    required String number,
    required String name,
    required PolicyStatus status,
    required int color,
  }) = _Policy;
}
