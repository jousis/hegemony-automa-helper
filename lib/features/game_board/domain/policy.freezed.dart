// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'policy.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Policy {
  PolicyId get id => throw _privateConstructorUsedError;
  String get number => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  PolicyStatus get status => throw _privateConstructorUsedError;
  int get color => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PolicyCopyWith<Policy> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PolicyCopyWith<$Res> {
  factory $PolicyCopyWith(Policy value, $Res Function(Policy) then) =
      _$PolicyCopyWithImpl<$Res, Policy>;
  @useResult
  $Res call(
      {PolicyId id,
      String number,
      String name,
      PolicyStatus status,
      int color});
}

/// @nodoc
class _$PolicyCopyWithImpl<$Res, $Val extends Policy>
    implements $PolicyCopyWith<$Res> {
  _$PolicyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? number = null,
    Object? name = null,
    Object? status = null,
    Object? color = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as PolicyId,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PolicyStatus,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PolicyCopyWith<$Res> implements $PolicyCopyWith<$Res> {
  factory _$$_PolicyCopyWith(_$_Policy value, $Res Function(_$_Policy) then) =
      __$$_PolicyCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {PolicyId id,
      String number,
      String name,
      PolicyStatus status,
      int color});
}

/// @nodoc
class __$$_PolicyCopyWithImpl<$Res>
    extends _$PolicyCopyWithImpl<$Res, _$_Policy>
    implements _$$_PolicyCopyWith<$Res> {
  __$$_PolicyCopyWithImpl(_$_Policy _value, $Res Function(_$_Policy) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? number = null,
    Object? name = null,
    Object? status = null,
    Object? color = null,
  }) {
    return _then(_$_Policy(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as PolicyId,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PolicyStatus,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_Policy implements _Policy {
  const _$_Policy(
      {required this.id,
      required this.number,
      required this.name,
      required this.status,
      required this.color});

  @override
  final PolicyId id;
  @override
  final String number;
  @override
  final String name;
  @override
  final PolicyStatus status;
  @override
  final int color;

  @override
  String toString() {
    return 'Policy(id: $id, number: $number, name: $name, status: $status, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Policy &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.color, color) || other.color == color));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, number, name, status, color);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PolicyCopyWith<_$_Policy> get copyWith =>
      __$$_PolicyCopyWithImpl<_$_Policy>(this, _$identity);
}

abstract class _Policy implements Policy {
  const factory _Policy(
      {required final PolicyId id,
      required final String number,
      required final String name,
      required final PolicyStatus status,
      required final int color}) = _$_Policy;

  @override
  PolicyId get id;
  @override
  String get number;
  @override
  String get name;
  @override
  PolicyStatus get status;
  @override
  int get color;
  @override
  @JsonKey(ignore: true)
  _$$_PolicyCopyWith<_$_Policy> get copyWith =>
      throw _privateConstructorUsedError;
}
