import 'package:hegemony_automa/features/game_board/domain/game_board.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_repository.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: IGameBoardRepository)
class GameBoardRepository implements IGameBoardRepository {
  GameBoard _board = GameBoard.newGame();

  @override
  GameBoard get board => _board;

  @override
  void resetGame() {
    _board = GameBoard.newGame();
  }
}
