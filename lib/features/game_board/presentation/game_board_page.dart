import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/game_board/application/bloc/game_board_bloc.dart';
import 'package:hegemony_automa/features/game_board/domain/game_board.dart';
import 'package:hegemony_automa/features/game_board/presentation/policy_card.dart';
import 'package:hegemony_automa/features/game_board/presentation/score_track_widget.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

class GameBoardPage extends StatelessWidget {
  const GameBoardPage({super.key});

  Map<RoleName, int> getScoreOffsetsMap(List<ScoreTrack> scores) {
    List<ScoreTrack> sortedScoreList = scores.toList()
      ..sort(
        (a, b) => a.score.compareTo(b.score),
      );
    final offsetScoreList = sortedScoreList
        .mapIndexed(
          (index, s) => ScoreTrack(role: s.role, score: -3 + index * 3, color: s.color),
        )
        .toList();

    return {
      for (var score in offsetScoreList) score.role: score.score,
    };
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: BlocProvider.of<GameBoardBloc>(context),
      child: BlocBuilder<GameBoardBloc, GameBoardState>(
        builder: (context, state) {
          final scoreOffsets = getScoreOffsetsMap(state.gameBoard.scores);
          return Scaffold(
            appBar: AppBar(),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: state.gameBoard.scores
                        .map(
                          (s) => Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: ScoreTrackWidget(
                              onTap: (score) {
                                context.read<GameBoardBloc>().add(
                                      GameBoardEvent.setScore(s.role, score),
                                    );
                              },
                              offset: scoreOffsets[s.role] ?? 0,
                              score: s.score,
                              color: Color(s.color),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: state.gameBoard.policies
                              .map(
                                (e) => SizedBox(
                                  width: 300,
                                  height: 180,
                                  // constraints: const BoxConstraints(maxWidth: 400),
                                  child: PolicyCard(policy: e),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
