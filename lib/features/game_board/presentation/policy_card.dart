import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/game_board/application/bloc/game_board_bloc.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/game_board/presentation/policy_tile.dart';

class PolicyCard extends StatelessWidget {
  const PolicyCard({
    required this.policy,
    super.key,
  });

  final Policy policy;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(policy.color),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AutoSizeText(
              '${policy.number} - ${policy.name}',
              minFontSize: 12,
              maxLines: 1,
              style: const TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                ...PolicyStatus.values.map(
                  (p) => Expanded(
                    child: PolicyTile(
                      policyStatus: p,
                      selected: policy.status == p,
                      onTap: () => context.read<GameBoardBloc>().add(
                            GameBoardEvent.changePolicyStatus(
                              id: policy.id,
                              status: p,
                            ),
                          ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
