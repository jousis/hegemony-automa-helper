import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/shared/presentation/selection_cube.dart';

class PolicyTile extends StatelessWidget {
  const PolicyTile({
    required this.policyStatus,
    required this.selected,
    required this.onTap,
    super.key,
  });

  final PolicyStatus policyStatus;
  final bool selected;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: onTap,
          child: DecoratedBox(
            decoration: BoxDecoration(
              border: Border.all(
                width: 2,
                color: Colors.white,
              ),
            ),
            child: SizedBox(
              width: 100,
              height: 100,
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: [
                  FittedBox(
                    child: AutoSizeText(
                      policyStatus.name,
                      maxLines: 1,
                      minFontSize: 12,
                      maxFontSize: 32,
                      style: TextStyle(
                        // fontSize: 64,
                        fontWeight: FontWeight.bold,
                        color: selected ? Colors.white.withAlpha(128) : Colors.white,
                      ),
                    ),
                  ),
                  if (selected)
                    const SelectionCube(
                      color: Colors.white,
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
