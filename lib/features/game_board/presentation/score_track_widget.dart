import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hegemony_automa/features/shared/presentation/selection_cube.dart';

class ScoreTrackWidget extends StatelessWidget {
  const ScoreTrackWidget({
    required this.offset,
    required this.score,
    required this.color,
    required this.onTap,
    super.key,
  });

  final int score;
  final Color color;
  final int offset;

  final void Function(int) onTap;

  final _totalLength = 15;

  @override
  Widget build(BuildContext context) {
    final before = (_totalLength / 2).floor() + offset - 3;
    final itemsBefore = min(before, score);
    final itemsAfter = max(_totalLength - itemsBefore, 0);
    return DecoratedBox(
      decoration: BoxDecoration(color: color),
      child: SizedBox(
        height: 50,
        child: ListView(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          children: [
            if (itemsBefore > 0)
              ...List.generate(
                itemsBefore,
                (index) {
                  final value = score - itemsBefore + index;
                  return InkWell(
                    onTap: () => onTap(value),
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: Center(
                        child: Text(
                          value.toStringAsFixed(0),
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            SizedBox(
              width: 50,
              height: 50,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  const SelectionCube(color: Colors.white),
                  Padding(
                    padding: const EdgeInsets.all(3.5),
                    child: Center(
                      child: Text(
                        score.toStringAsFixed(0),
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (itemsAfter > 0)
              ...List.generate(
                itemsAfter,
                (index) {
                  final value = score + 1 + index;
                  return InkWell(
                    onTap: () => onTap(value),
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: Center(
                        child: Text(
                          value.toStringAsFixed(0),
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
          ],
        ),
      ),
    );
  }
}
