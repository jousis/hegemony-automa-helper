import 'package:hegemony_automa/features/instruction_card/domain/instruction_card.dart';

abstract class IInstructionCardRepository {
  List<InstructionCard> get wcInstructionCards;
  List<InstructionCard> get mcInstructionCards;
  List<InstructionCard> get ccInstructionCards;
}
