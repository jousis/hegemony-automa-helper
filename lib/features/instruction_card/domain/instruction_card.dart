import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

part 'instruction_card.freezed.dart';

@freezed
class InstructionCard with _$InstructionCard {
  const factory InstructionCard({
    required RoleName role,
    required String title,
    required List<String> tags,
    required String filename,
  }) = _InstructionCard;
}
