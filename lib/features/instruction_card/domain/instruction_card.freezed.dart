// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'instruction_card.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InstructionCard {
  RoleName get role => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  List<String> get tags => throw _privateConstructorUsedError;
  String get filename => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InstructionCardCopyWith<InstructionCard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InstructionCardCopyWith<$Res> {
  factory $InstructionCardCopyWith(
          InstructionCard value, $Res Function(InstructionCard) then) =
      _$InstructionCardCopyWithImpl<$Res, InstructionCard>;
  @useResult
  $Res call({RoleName role, String title, List<String> tags, String filename});
}

/// @nodoc
class _$InstructionCardCopyWithImpl<$Res, $Val extends InstructionCard>
    implements $InstructionCardCopyWith<$Res> {
  _$InstructionCardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? title = null,
    Object? tags = null,
    Object? filename = null,
  }) {
    return _then(_value.copyWith(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InstructionCardCopyWith<$Res>
    implements $InstructionCardCopyWith<$Res> {
  factory _$$_InstructionCardCopyWith(
          _$_InstructionCard value, $Res Function(_$_InstructionCard) then) =
      __$$_InstructionCardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({RoleName role, String title, List<String> tags, String filename});
}

/// @nodoc
class __$$_InstructionCardCopyWithImpl<$Res>
    extends _$InstructionCardCopyWithImpl<$Res, _$_InstructionCard>
    implements _$$_InstructionCardCopyWith<$Res> {
  __$$_InstructionCardCopyWithImpl(
      _$_InstructionCard _value, $Res Function(_$_InstructionCard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? role = null,
    Object? title = null,
    Object? tags = null,
    Object? filename = null,
  }) {
    return _then(_$_InstructionCard(
      role: null == role
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as RoleName,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value._tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>,
      filename: null == filename
          ? _value.filename
          : filename // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InstructionCard implements _InstructionCard {
  const _$_InstructionCard(
      {required this.role,
      required this.title,
      required final List<String> tags,
      required this.filename})
      : _tags = tags;

  @override
  final RoleName role;
  @override
  final String title;
  final List<String> _tags;
  @override
  List<String> get tags {
    if (_tags is EqualUnmodifiableListView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tags);
  }

  @override
  final String filename;

  @override
  String toString() {
    return 'InstructionCard(role: $role, title: $title, tags: $tags, filename: $filename)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InstructionCard &&
            (identical(other.role, role) || other.role == role) &&
            (identical(other.title, title) || other.title == title) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.filename, filename) ||
                other.filename == filename));
  }

  @override
  int get hashCode => Object.hash(runtimeType, role, title,
      const DeepCollectionEquality().hash(_tags), filename);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InstructionCardCopyWith<_$_InstructionCard> get copyWith =>
      __$$_InstructionCardCopyWithImpl<_$_InstructionCard>(this, _$identity);
}

abstract class _InstructionCard implements InstructionCard {
  const factory _InstructionCard(
      {required final RoleName role,
      required final String title,
      required final List<String> tags,
      required final String filename}) = _$_InstructionCard;

  @override
  RoleName get role;
  @override
  String get title;
  @override
  List<String> get tags;
  @override
  String get filename;
  @override
  @JsonKey(ignore: true)
  _$$_InstructionCardCopyWith<_$_InstructionCard> get copyWith =>
      throw _privateConstructorUsedError;
}
