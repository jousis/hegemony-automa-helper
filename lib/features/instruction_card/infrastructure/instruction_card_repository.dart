import 'package:dartz/dartz.dart';
import 'package:hegemony_automa/features/instruction_card/domain/i_instruction_card_repository.dart';
import 'package:hegemony_automa/features/instruction_card/domain/instruction_card.dart';
import 'package:hegemony_automa/features/instruction_card/infrastructure/instruction_cards.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: IInstructionCardRepository)
class InstructionCardRepository implements IInstructionCardRepository {
  final Option<List<InstructionCard>> _wcInstructionCards = none();
  final Option<List<InstructionCard>> _mcInstructionCards = none();
  final Option<List<InstructionCard>> _ccInstructionCards = none();

  List<InstructionCard> resetCardList() {
    return instructionCards;
  }

  @override
  List<InstructionCard> get wcInstructionCards => _wcInstructionCards
      .fold(
        () => resetCardList(),
        id,
      )
      .where((ic) => ic.role == RoleName.workingClass)
      .toList();
  @override
  List<InstructionCard> get mcInstructionCards => _mcInstructionCards
      .fold(
        () => resetCardList(),
        id,
      )
      .where((ic) => ic.role == RoleName.middleClass)
      .toList();
  @override
  List<InstructionCard> get ccInstructionCards => _ccInstructionCards
      .fold(
        () => resetCardList(),
        id,
      )
      .where((ic) => ic.role == RoleName.capitalistClass)
      .toList();
}
