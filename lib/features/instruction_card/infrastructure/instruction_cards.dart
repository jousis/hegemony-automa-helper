import 'package:hegemony_automa/features/instruction_card/domain/instruction_card.dart';
import 'package:hegemony_automa/features/shared/enums.dart';

const instructionCards = <InstructionCard>[
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Action Criteria (Bill, Strike)',
    tags: ['action', 'criteria', 'bill', 'propose', 'strike'],
    filename: 'wc_ac_01.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Action Criteria (Assign, Buy G&S)',
    tags: ['action', 'criteria', 'assign', 'worker', 'buy', 'goods', 'services', 'demonstration'],
    filename: 'wc_ac_01.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Place Strike',
    tags: ['criteria', 'strike'],
    filename: 'wc_criteria_place_strike.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Assign Workers Criteria',
    tags: ['criteria', 'assign', 'trade', 'union'],
    filename: 'wc_criteria_assign_workers.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Buy G&S Criteria',
    tags: ['criteria', 'buy', 'goods', 'services'],
    filename: 'wc_criteria_buy_gs.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Skilled Worker Criteria',
    tags: ['criteria', 'worker', 'place', 'skilled', 'unskilled', 'trade', 'union'],
    filename: 'wc_criteria_skilled_workers.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Check G&S',
    tags: ['check', 'goods', 'services'],
    filename: 'cc_check_gs.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Check Policies',
    tags: ['check', 'polic'],
    filename: 'cc_check_policies.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Check Strike',
    tags: ['check', 'strike'],
    filename: 'cc_check_strike.jpg',
  ),
  InstructionCard(
    role: RoleName.workingClass,
    title: 'Check Workers',
    tags: ['check', 'worker', 'skilled', 'unskilled', 'unemployed'],
    filename: 'cc_check_workers.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Action Criteria (Bill, Lobby, Special)',
    tags: ['action', 'criteria', 'bill', 'propose', 'lobby', 'influence', 'special'],
    filename: 'cc_ac_01.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Action Criteria (Build, Sell)',
    tags: ['action', 'criteria', 'build', 'sell', 'compan', 'foreign', 'market', 'export'],
    filename: 'cc_ac_02.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Building Company Criteria',
    tags: ['criteria', 'build', 'compan', 'trade', 'union', 'operational', 'demonstration'],
    filename: 'cc_criteria_build_company.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Sell Company Criteria',
    tags: ['criteria', 'sell', 'compan', 'trade', 'union'],
    filename: 'cc_criteria_sell_company.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Assign Workers Criteria',
    tags: ['criteria', 'assign', 'worker', 'operational', 'compan'],
    filename: 'cc_criteria_assign_workers.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Place Machinery Criteria',
    tags: ['criteria', 'place', 'machinery', 'gear', 'compan'],
    filename: 'cc_criteria_machinery.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Response to Strike',
    tags: ['response', 'strike', 'token'],
    filename: 'cc_response_to_strike.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Check Foreign Market',
    tags: ['foreign', 'market', 'check'],
    filename: 'cc_check_foreign_market.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Check Policies',
    tags: ['check', 'polic'],
    filename: 'cc_check_policies.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Check Influence',
    tags: ['check', 'influence'],
    filename: 'cc_check_influence.jpg',
  ),
  InstructionCard(
    role: RoleName.capitalistClass,
    title: 'Check Companies',
    tags: ['check', 'compan', 'build'],
    filename: 'cc_check_companies.jpg',
  ),
];
