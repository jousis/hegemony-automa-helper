import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/instruction_card/domain/instruction_card.dart';

part 'instruction_card_vm.freezed.dart';

@freezed
class InstructionCardVm with _$InstructionCardVm {
  const InstructionCardVm._();
  const factory InstructionCardVm({
    required String title,
    required List<String> tags,
    required String assetPath,
  }) = _InstructionCardVm;

  factory InstructionCardVm.fromDomain(InstructionCard card) => InstructionCardVm(
        title: card.title,
        tags: card.tags,
        assetPath: 'assets/cc/instructions/${card.filename}',
      );
}
