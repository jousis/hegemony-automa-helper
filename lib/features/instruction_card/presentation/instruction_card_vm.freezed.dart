// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'instruction_card_vm.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$InstructionCardVm {
  String get title => throw _privateConstructorUsedError;
  List<String> get tags => throw _privateConstructorUsedError;
  String get assetPath => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $InstructionCardVmCopyWith<InstructionCardVm> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InstructionCardVmCopyWith<$Res> {
  factory $InstructionCardVmCopyWith(
          InstructionCardVm value, $Res Function(InstructionCardVm) then) =
      _$InstructionCardVmCopyWithImpl<$Res, InstructionCardVm>;
  @useResult
  $Res call({String title, List<String> tags, String assetPath});
}

/// @nodoc
class _$InstructionCardVmCopyWithImpl<$Res, $Val extends InstructionCardVm>
    implements $InstructionCardVmCopyWith<$Res> {
  _$InstructionCardVmCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? tags = null,
    Object? assetPath = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>,
      assetPath: null == assetPath
          ? _value.assetPath
          : assetPath // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_InstructionCardVmCopyWith<$Res>
    implements $InstructionCardVmCopyWith<$Res> {
  factory _$$_InstructionCardVmCopyWith(_$_InstructionCardVm value,
          $Res Function(_$_InstructionCardVm) then) =
      __$$_InstructionCardVmCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, List<String> tags, String assetPath});
}

/// @nodoc
class __$$_InstructionCardVmCopyWithImpl<$Res>
    extends _$InstructionCardVmCopyWithImpl<$Res, _$_InstructionCardVm>
    implements _$$_InstructionCardVmCopyWith<$Res> {
  __$$_InstructionCardVmCopyWithImpl(
      _$_InstructionCardVm _value, $Res Function(_$_InstructionCardVm) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? tags = null,
    Object? assetPath = null,
  }) {
    return _then(_$_InstructionCardVm(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      tags: null == tags
          ? _value._tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>,
      assetPath: null == assetPath
          ? _value.assetPath
          : assetPath // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_InstructionCardVm extends _InstructionCardVm {
  const _$_InstructionCardVm(
      {required this.title,
      required final List<String> tags,
      required this.assetPath})
      : _tags = tags,
        super._();

  @override
  final String title;
  final List<String> _tags;
  @override
  List<String> get tags {
    if (_tags is EqualUnmodifiableListView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tags);
  }

  @override
  final String assetPath;

  @override
  String toString() {
    return 'InstructionCardVm(title: $title, tags: $tags, assetPath: $assetPath)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InstructionCardVm &&
            (identical(other.title, title) || other.title == title) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.assetPath, assetPath) ||
                other.assetPath == assetPath));
  }

  @override
  int get hashCode => Object.hash(runtimeType, title,
      const DeepCollectionEquality().hash(_tags), assetPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InstructionCardVmCopyWith<_$_InstructionCardVm> get copyWith =>
      __$$_InstructionCardVmCopyWithImpl<_$_InstructionCardVm>(
          this, _$identity);
}

abstract class _InstructionCardVm extends InstructionCardVm {
  const factory _InstructionCardVm(
      {required final String title,
      required final List<String> tags,
      required final String assetPath}) = _$_InstructionCardVm;
  const _InstructionCardVm._() : super._();

  @override
  String get title;
  @override
  List<String> get tags;
  @override
  String get assetPath;
  @override
  @JsonKey(ignore: true)
  _$$_InstructionCardVmCopyWith<_$_InstructionCardVm> get copyWith =>
      throw _privateConstructorUsedError;
}
