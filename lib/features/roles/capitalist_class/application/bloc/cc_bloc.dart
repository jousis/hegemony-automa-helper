import 'dart:async';
import 'dart:math';

import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_facade.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/instruction_card/domain/i_instruction_card_repository.dart';
import 'package:hegemony_automa/features/instruction_card/presentation/instruction_card_vm.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/cc_game_board.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/i_cc_facade.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_end_of_game_scoring.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_player_board_vm.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/vms/cc_page_vm.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:hegemony_automa/features/voting_bag/domain/voting_bag.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

part 'cc_bloc.freezed.dart';
part 'cc_event.dart';
part 'cc_state.dart';

@injectable
class CcBloc extends Bloc<CcEvent, CcState> {
  CcBloc(
    this._ccFacade,
    this._gameBoardFacade,
    this._instructionCardRepository,
    this._votingBag,
  ) : super(CcState.initial()) {
    on<CcEvent>(
      _onCCEvent,
      // transformer: (events, mapper) {
      //   final debouncedEvents = events
      //       .where(
      //         (event) => event is _SetFood,
      //       )
      //       .debounceTime(
      //         const Duration(milliseconds: 300),
      //       );
      //   final notDebouncedEvents = events.where(
      //     (event) => event is! _SetFood,
      //   );
      //   return MergeStream([
      //     debouncedEvents,
      //     notDebouncedEvents,
      //   ]).flatMap(mapper);
      // },
    );
  }

  EventTransformer<RegistrationEvent> debounceRestartable<RegistrationEvent>(
    Duration duration,
  ) {
    // This feeds the debounced event stream to restartable() and returns that
    // as a transformer.
    return (events, mapper) => restartable<RegistrationEvent>().call(
        events.debounceTime(
          duration,
        ),
        mapper);
  }

  final ICcFacade _ccFacade;
  final IGameBoardFacade _gameBoardFacade;
  final IInstructionCardRepository _instructionCardRepository;
  final VotingBag _votingBag;
  StreamSubscription<bool>? _gameBoardUpdated;

  FutureOr<void> _onCCEvent(
    CcEvent event,
    Emitter<CcState> emit,
  ) async {
    await event.map(
      started: (e) => _onStarted(e, emit),
      gameBoardUpdated: (e) => _onGameBoardUpdated(e, emit),
      setRevenue: (e) => _onSetRevenue(e, emit),
      setCapital: (e) => _onSetCapital(e, emit),
      moveRevenueToCapital: (e) => _onMoveRevenueToCapital(e, emit),
      drawCompanies: (e) => _onDrawCompanies(e, emit),
      drawCompanyOnIndex: (e) => _onDrawCompanyOnIndex(e, emit),
      removeCompany: (e) => _onRemoveCompany(e, emit),
      reshuffleDiscartedCompanies: (e) => _onReshuffleDiscartedCompanies(e, emit),
      drawAiCard: (e) => _onDrawAiCard(e, emit),
      checkAiCard: (e) => _onCheckAiCard(e, emit),
      reshuffleDiscartedAiCards: (e) => _onReshuffleDiscartedAiCards(e, emit),
      wealthTrackPositionSelected: (e) => _onWealthTrackPositionSelected(e, emit),
      buyStorage: (e) => _onBuyStorage(e, emit),
      setFood: (e) => _onSetFood(e, emit),
      setLuxury: (e) => _onSetLuxury(e, emit),
      setHealthcare: (e) => _onSetHealthcare(e, emit),
      setEducation: (e) => _onSetEducation(e, emit),
      setInfluence: (e) => _onSetInfluence(e, emit),
      setPrice: (e) => _onSetPrice(e, emit),
      takeLoan: (e) => _onTakeLoan(e, emit),
      payOffLoan: (e) => _onPayOffLoan(e, emit),
    );
  }

  CcEndOfGameScoringVm getEoGScoring(
    List<Policy> policies,
  ) {
    final playerBoard = playerBoardVm;
    final vpList = [0, 1, 4, 8, 12, 18, 0, 0];
    final neolibPolicies = policies
        .where(
          (p) => p.id != PolicyId.foreignTrade && p.id != PolicyId.immigration,
        )
        .where(
          (p) => p.status == PolicyStatus.C,
        )
        .length;
    final fromPolicies = vpList[neolibPolicies];
    final fromFood = ((playerBoard.food + playerBoard.extraFood) / 2).floor();
    final fromLuxury = ((playerBoard.luxury + playerBoard.extraLuxury) / 3).floor();
    final fromHealthcare = ((playerBoard.healthcare + playerBoard.extraHealthcare) / 3).floor();
    final fromEducation = ((playerBoard.education + playerBoard.extraEducation) / 3).floor();

    final fromLoans = playerBoard.loans * (-5);
    final total = fromPolicies + fromFood + fromLuxury + fromHealthcare + fromEducation + fromLoans;
    return CcEndOfGameScoringVm(
      fromPolicies: '+$fromPolicies VP',
      fromFood: fromFood == 0 ? '' : '+$fromFood VP',
      fromLuxury: fromLuxury == 0 ? '' : '+$fromLuxury VP',
      fromHealthcare: fromHealthcare == 0 ? '' : '+$fromHealthcare VP',
      fromEducation: fromEducation == 0 ? '' : '+$fromEducation VP',
      fromLoans: fromLoans == 0 ? '' : '$fromLoans VP',
      total: total > 0 ? '+$total VP' : '$total VP',
    );
  }

  CcState getBasicStateObject() => CcState(
        reminders: getReminders(),
        instructionCards: _instructionCardRepository.ccInstructionCards
            .map(
              (ic) => InstructionCardVm.fromDomain(
                ic,
              ),
            )
            .toList(),
        corporateTax: _ccFacade.corporateTax,
        ccPageVm: pageVm,
        ccPlayerBoardVm: playerBoardVm,
        endOfGameScoring: getEoGScoring(
          _gameBoardFacade.board.policies,
        ),
      );

  CcPageVm get pageVm {
    final rCubes = _votingBag.remainingCubes(RoleName.capitalistClass);
    final ccGameBoard = _ccFacade.gameBoard;

    return CcPageVm.fromDomain(
      ccGameBoard,
      rCubes,
    );
  }

  CcGoodOrService goodOrService(
    GoodOrServiceType type,
  ) =>
      _ccFacade.gameBoard.goodsOrServices.firstWhereOrNull(
        (g) => g.type == type,
      ) ??
      CcGoodOrService.defaults(
        type,
      );

  CcPlayerBoardVm get playerBoardVm {
    return CcPlayerBoardVm.fromDomain(
      gameBoard: _ccFacade.gameBoard,
      wealthTrackMoney: _ccFacade.wealthTrackMoney,
      food: goodOrService(GoodOrServiceType.food),
      luxury: goodOrService(GoodOrServiceType.luxury),
      healthcare: goodOrService(GoodOrServiceType.healthcare),
      education: goodOrService(GoodOrServiceType.education),
      foodPrice: _ccFacade.price(GoodOrServiceType.food),
      luxuryPrice: _ccFacade.price(GoodOrServiceType.luxury),
      healthcarePrice: _ccFacade.price(GoodOrServiceType.healthcare),
      educationPrice: _ccFacade.price(GoodOrServiceType.education),
    );
  }

  PriceLevel getPriceLevel(
    GoodOrServiceType type,
  ) =>
      _ccFacade.gameBoard.goodsOrServices
          .firstWhereOrNull(
            (g) => g.type == type,
          )
          ?.priceLevel ??
      PriceLevel.mid;

  String get6aComplianceMessage() {
    final foodPriceLevel = getPriceLevel(GoodOrServiceType.food);
    final luxuryPriceLevel = getPriceLevel(GoodOrServiceType.luxury);
    final policyStatus = _gameBoardFacade.board.policies
            .firstWhereOrNull(
              (p) => p.id == PolicyId.foreignTrade,
            )
            ?.status ??
        PolicyStatus.C;
    if (policyStatus == PolicyStatus.A) {
      if (foodPriceLevel != PriceLevel.high || luxuryPriceLevel != PriceLevel.high) {
        return '6A is in effect! Set Food & Luxury prices to their highest values !';
      }
    } else {
      if (foodPriceLevel == PriceLevel.high || luxuryPriceLevel == PriceLevel.high) {
        return '6A is not in effect! Set Food & Luxury prices to normal (mid) !';
      }
    }
    return '';
  }

  String get4cComplianceMessage() {
    final healthPriceLevel = getPriceLevel(GoodOrServiceType.healthcare);
    final policyStatus = _gameBoardFacade.board.policies
            .firstWhereOrNull(
              (p) => p.id == PolicyId.healthcare,
            )
            ?.status ??
        PolicyStatus.C;
    if (policyStatus == PolicyStatus.C) {
      if (healthPriceLevel != PriceLevel.high || healthPriceLevel != PriceLevel.high) {
        return '4C is in effect! Set Health price to the highest value !';
      }
    } else {
      if (healthPriceLevel != PriceLevel.mid || healthPriceLevel != PriceLevel.mid) {
        return '4C is not in effect! Set Health price to normal (mid) !';
      }
    }
    return '';
  }

  String get5cComplianceMessage() {
    final educationPriceLevel = getPriceLevel(GoodOrServiceType.education);
    final policyStatus = _gameBoardFacade.board.policies
            .firstWhereOrNull(
              (p) => p.id == PolicyId.education,
            )
            ?.status ??
        PolicyStatus.C;
    if (policyStatus == PolicyStatus.C) {
      if (educationPriceLevel != PriceLevel.high || educationPriceLevel != PriceLevel.high) {
        return '5C is in effect! Set Education price to the highest value !';
      }
    } else {
      if (educationPriceLevel != PriceLevel.mid || educationPriceLevel != PriceLevel.mid) {
        return '5C is not in effect! Set Education price to normal (mid) !';
      }
    }
    return '';
  }

  List<String> getReminders() {
    List<String> reminders = [];
    final msg6a = get6aComplianceMessage();
    if (msg6a.isNotEmpty) {
      reminders.add(msg6a);
    }
    final msg4c = get4cComplianceMessage();
    if (msg4c.isNotEmpty) {
      reminders.add(msg4c);
    }
    final msg5c = get5cComplianceMessage();
    if (msg5c.isNotEmpty) {
      reminders.add(msg5c);
    }

    final capital = _ccFacade.gameBoard.capital;
    final loans = _ccFacade.gameBoard.activeLoans;
    if (capital >= 100 && loans > 0) {
      reminders.add('At the start of the turn, remember to pay off a loan !');
    }
    return reminders;
  }

  FutureOr<void> _onStarted(_Started e, Emitter<CcState> emit) async {
    _gameBoardUpdated?.cancel();
    _gameBoardUpdated = _gameBoardFacade.gameBoardUpdated.listen(
      (event) {
        add(const CcEvent.gameBoardUpdated());
      },
    );
    _ccFacade.reset();
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetRevenue(_SetRevenue e, Emitter<CcState> emit) async {
    // final revenue = _ccFacade.gameBoard.revenue;
    final remainingRevenue = e.amount;
    final capital = _ccFacade.gameBoard.capital;
    if (remainingRevenue + capital < 0) {
      const message = 'Not enough revenue/capital to pay. Take a loan first.';
      emit(getBasicStateObject().copyWith(showMessage: message));
      return;
    }
    //all good
    if (remainingRevenue < 0) {
      _ccFacade.setCapital(capital + remainingRevenue);
    }
    _ccFacade.setRevenue(max(remainingRevenue, 0));
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetCapital(_SetCapital e, Emitter<CcState> emit) async {
    if (e.amount < 0) {
      const message = 'Not enough capital to pay. Take a loan first.';
      emit(getBasicStateObject().copyWith(showMessage: message));
      return;
    }
    _ccFacade.setCapital(e.amount);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onDrawCompanies(_DrawCompanies e, Emitter<CcState> emit) async {
    _ccFacade.newCompanyDisplay(e.amount);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onDrawCompanyOnIndex(_DrawCompanyOnIndex e, Emitter<CcState> emit) async {
    _ccFacade.drawCompanyOnIndex(e.index);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onRemoveCompany(_RemoveCompany e, Emitter<CcState> emit) async {
    _ccFacade.removeCompany(e.index);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onReshuffleDiscartedCompanies(_ReshuffleDiscartedCompanies e, Emitter<CcState> emit) async {
    _ccFacade.reshuffleDiscartedCompanies();
    emit(getBasicStateObject());
  }

  FutureOr<void> _onWealthTrackPositionSelected(
    _WealthTrackPositionSelected e,
    Emitter<CcState> emit,
  ) async {
    final diff = e.position - _ccFacade.gameBoard.wealthTrackPosition;
    _ccFacade.wealthPosition = e.position;
    String? message;
    if (diff > 0) {
      message = 'You moved the marker $diff times, please score ${diff * 3 + e.position} points';
    }
    emit(getBasicStateObject().copyWith(showMessage: message));
  }

  FutureOr<void> _onBuyStorage(_BuyStorage e, Emitter<CcState> emit) async {
    if (_ccFacade.hasExtraStorage(e.type)) {
      emit(getBasicStateObject());
      return;
    }
    final revenue = _ccFacade.gameBoard.revenue;
    final remainingRevenue = revenue - 20;
    final capital = _ccFacade.gameBoard.capital;
    if (remainingRevenue + capital < 0) {
      const message = 'Not enough revenue/capital to buy storage. Take a loan first.';
      emit(getBasicStateObject().copyWith(showMessage: message));
      return;
    }
    //all good
    if (remainingRevenue < 0) {
      _ccFacade.setCapital(capital + remainingRevenue);
    }
    _ccFacade.setRevenue(max(remainingRevenue, 0));
    _ccFacade.setQuantity(type: e.type, extraStorage: 0);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetFood(_SetFood e, Emitter<CcState> emit) async {
    if (e.toExtraStorage && _ccFacade.hasExtraStorage(GoodOrServiceType.food)) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.food,
        extraStorage: e.amount,
      );
    } else if (e.toFreeTradeMarket) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.food,
        freeTradeZone: e.amount,
      );
    } else {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.food,
        main: e.amount,
      );
    }
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetLuxury(_SetLuxury e, Emitter<CcState> emit) async {
    if (e.toExtraStorage && _ccFacade.hasExtraStorage(GoodOrServiceType.luxury)) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.luxury,
        extraStorage: e.amount,
      );
    } else if (e.toFreeTradeMarket) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.luxury,
        freeTradeZone: e.amount,
      );
    } else {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.luxury,
        main: e.amount,
      );
    }
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetHealthcare(_SetHealthcare e, Emitter<CcState> emit) async {
    if (e.toExtraStorage && _ccFacade.hasExtraStorage(GoodOrServiceType.healthcare)) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.healthcare,
        extraStorage: e.amount,
      );
    } else {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.healthcare,
        main: e.amount,
      );
    }
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetEducation(_SetEducation e, Emitter<CcState> emit) async {
    if (e.toExtraStorage && _ccFacade.hasExtraStorage(GoodOrServiceType.education)) {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.education,
        extraStorage: e.amount,
      );
    } else {
      _ccFacade.setQuantity(
        type: GoodOrServiceType.education,
        main: e.amount,
      );
    }
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetInfluence(_SetInfluence e, Emitter<CcState> emit) async {
    _ccFacade.influence = e.amount;
    emit(getBasicStateObject());
  }

  FutureOr<void> _onSetPrice(_SetPrice e, Emitter<CcState> emit) async {
    _ccFacade.setPrice(e.type, e.price);
    emit(getBasicStateObject());
  }

  FutureOr<void> _onMoveRevenueToCapital(_MoveRevenueToCapital e, Emitter<CcState> emit) async {
    _ccFacade.moveRevenueToCapital();
    final wealthTrackIndex = playerBoardVm.wealthTrackMoney.lastWhereOrNull(
      (e) => e <= _ccFacade.gameBoard.capital,
    );
    String? message;
    if (wealthTrackIndex != null) {
      message = 'Please remember to move wealth track marker to the position that shows $wealthTrackIndex∀';
    }
    emit(getBasicStateObject().copyWith(showMessage: message));
  }

  FutureOr<void> _onDrawAiCard(_DrawAiCard e, Emitter<CcState> emit) async {
    _ccFacade.drawAiCard();
    emit(getBasicStateObject());
  }

  FutureOr<void> _onCheckAiCard(_CheckAiCard e, Emitter<CcState> emit) async {
    final aiCard = _ccFacade.checkAiCard();
    if (aiCard != null && aiCard.filename.isNotEmpty) {
      final stateObj = getBasicStateObject();
      final page = pageVm.copyWith(checkAiCardAssetPath: 'assets/cc/ai/${aiCard.filename}');
      emit(stateObj.copyWith(ccPageVm: page));
    }
  }

  FutureOr<void> _onReshuffleDiscartedAiCards(_ReshuffleDiscartedAiCards e, Emitter<CcState> emit) async {
    _ccFacade.reshuffleDiscartedAiCards();
    emit(getBasicStateObject());
  }

  FutureOr<void> _onGameBoardUpdated(_GameBoardUpdated e, Emitter<CcState> emit) async {
    emit(getBasicStateObject());
  }

  FutureOr<void> _onTakeLoan(_TakeLoan e, Emitter<CcState> emit) async {
    _ccFacade.takeLoan();
    emit(getBasicStateObject());
  }

  FutureOr<void> _onPayOffLoan(_PayOffLoan e, Emitter<CcState> emit) async {
    _ccFacade.payOffLoan();
    emit(getBasicStateObject());
  }
}
