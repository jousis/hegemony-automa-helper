// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cc_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CcEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcEventCopyWith<$Res> {
  factory $CcEventCopyWith(CcEvent value, $Res Function(CcEvent) then) =
      _$CcEventCopyWithImpl<$Res, CcEvent>;
}

/// @nodoc
class _$CcEventCopyWithImpl<$Res, $Val extends CcEvent>
    implements $CcEventCopyWith<$Res> {
  _$CcEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'CcEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements CcEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$$_GameBoardUpdatedCopyWith<$Res> {
  factory _$$_GameBoardUpdatedCopyWith(
          _$_GameBoardUpdated value, $Res Function(_$_GameBoardUpdated) then) =
      __$$_GameBoardUpdatedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GameBoardUpdatedCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_GameBoardUpdated>
    implements _$$_GameBoardUpdatedCopyWith<$Res> {
  __$$_GameBoardUpdatedCopyWithImpl(
      _$_GameBoardUpdated _value, $Res Function(_$_GameBoardUpdated) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_GameBoardUpdated implements _GameBoardUpdated {
  const _$_GameBoardUpdated();

  @override
  String toString() {
    return 'CcEvent.gameBoardUpdated()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GameBoardUpdated);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return gameBoardUpdated();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return gameBoardUpdated?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (gameBoardUpdated != null) {
      return gameBoardUpdated();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return gameBoardUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return gameBoardUpdated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (gameBoardUpdated != null) {
      return gameBoardUpdated(this);
    }
    return orElse();
  }
}

abstract class _GameBoardUpdated implements CcEvent {
  const factory _GameBoardUpdated() = _$_GameBoardUpdated;
}

/// @nodoc
abstract class _$$_SetRevenueCopyWith<$Res> {
  factory _$$_SetRevenueCopyWith(
          _$_SetRevenue value, $Res Function(_$_SetRevenue) then) =
      __$$_SetRevenueCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount});
}

/// @nodoc
class __$$_SetRevenueCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetRevenue>
    implements _$$_SetRevenueCopyWith<$Res> {
  __$$_SetRevenueCopyWithImpl(
      _$_SetRevenue _value, $Res Function(_$_SetRevenue) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
  }) {
    return _then(_$_SetRevenue(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetRevenue implements _SetRevenue {
  const _$_SetRevenue(this.amount);

  @override
  final int amount;

  @override
  String toString() {
    return 'CcEvent.setRevenue(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetRevenue &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetRevenueCopyWith<_$_SetRevenue> get copyWith =>
      __$$_SetRevenueCopyWithImpl<_$_SetRevenue>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setRevenue(amount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setRevenue?.call(amount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setRevenue != null) {
      return setRevenue(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setRevenue(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setRevenue?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setRevenue != null) {
      return setRevenue(this);
    }
    return orElse();
  }
}

abstract class _SetRevenue implements CcEvent {
  const factory _SetRevenue(final int amount) = _$_SetRevenue;

  int get amount;
  @JsonKey(ignore: true)
  _$$_SetRevenueCopyWith<_$_SetRevenue> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetCapitalCopyWith<$Res> {
  factory _$$_SetCapitalCopyWith(
          _$_SetCapital value, $Res Function(_$_SetCapital) then) =
      __$$_SetCapitalCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount});
}

/// @nodoc
class __$$_SetCapitalCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetCapital>
    implements _$$_SetCapitalCopyWith<$Res> {
  __$$_SetCapitalCopyWithImpl(
      _$_SetCapital _value, $Res Function(_$_SetCapital) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
  }) {
    return _then(_$_SetCapital(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetCapital implements _SetCapital {
  const _$_SetCapital(this.amount);

  @override
  final int amount;

  @override
  String toString() {
    return 'CcEvent.setCapital(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetCapital &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetCapitalCopyWith<_$_SetCapital> get copyWith =>
      __$$_SetCapitalCopyWithImpl<_$_SetCapital>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setCapital(amount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setCapital?.call(amount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setCapital != null) {
      return setCapital(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setCapital(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setCapital?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setCapital != null) {
      return setCapital(this);
    }
    return orElse();
  }
}

abstract class _SetCapital implements CcEvent {
  const factory _SetCapital(final int amount) = _$_SetCapital;

  int get amount;
  @JsonKey(ignore: true)
  _$$_SetCapitalCopyWith<_$_SetCapital> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_MoveRevenueToCapitalCopyWith<$Res> {
  factory _$$_MoveRevenueToCapitalCopyWith(_$_MoveRevenueToCapital value,
          $Res Function(_$_MoveRevenueToCapital) then) =
      __$$_MoveRevenueToCapitalCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_MoveRevenueToCapitalCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_MoveRevenueToCapital>
    implements _$$_MoveRevenueToCapitalCopyWith<$Res> {
  __$$_MoveRevenueToCapitalCopyWithImpl(_$_MoveRevenueToCapital _value,
      $Res Function(_$_MoveRevenueToCapital) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_MoveRevenueToCapital implements _MoveRevenueToCapital {
  const _$_MoveRevenueToCapital();

  @override
  String toString() {
    return 'CcEvent.moveRevenueToCapital()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_MoveRevenueToCapital);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return moveRevenueToCapital();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return moveRevenueToCapital?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (moveRevenueToCapital != null) {
      return moveRevenueToCapital();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return moveRevenueToCapital(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return moveRevenueToCapital?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (moveRevenueToCapital != null) {
      return moveRevenueToCapital(this);
    }
    return orElse();
  }
}

abstract class _MoveRevenueToCapital implements CcEvent {
  const factory _MoveRevenueToCapital() = _$_MoveRevenueToCapital;
}

/// @nodoc
abstract class _$$_SetFoodCopyWith<$Res> {
  factory _$$_SetFoodCopyWith(
          _$_SetFood value, $Res Function(_$_SetFood) then) =
      __$$_SetFoodCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount, bool toExtraStorage, bool toFreeTradeMarket});
}

/// @nodoc
class __$$_SetFoodCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetFood>
    implements _$$_SetFoodCopyWith<$Res> {
  __$$_SetFoodCopyWithImpl(_$_SetFood _value, $Res Function(_$_SetFood) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? toExtraStorage = null,
    Object? toFreeTradeMarket = null,
  }) {
    return _then(_$_SetFood(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      toExtraStorage: null == toExtraStorage
          ? _value.toExtraStorage
          : toExtraStorage // ignore: cast_nullable_to_non_nullable
              as bool,
      toFreeTradeMarket: null == toFreeTradeMarket
          ? _value.toFreeTradeMarket
          : toFreeTradeMarket // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetFood implements _SetFood {
  const _$_SetFood(this.amount,
      {this.toExtraStorage = false, this.toFreeTradeMarket = false});

  @override
  final int amount;
  @override
  @JsonKey()
  final bool toExtraStorage;
  @override
  @JsonKey()
  final bool toFreeTradeMarket;

  @override
  String toString() {
    return 'CcEvent.setFood(amount: $amount, toExtraStorage: $toExtraStorage, toFreeTradeMarket: $toFreeTradeMarket)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetFood &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.toExtraStorage, toExtraStorage) ||
                other.toExtraStorage == toExtraStorage) &&
            (identical(other.toFreeTradeMarket, toFreeTradeMarket) ||
                other.toFreeTradeMarket == toFreeTradeMarket));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, amount, toExtraStorage, toFreeTradeMarket);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetFoodCopyWith<_$_SetFood> get copyWith =>
      __$$_SetFoodCopyWithImpl<_$_SetFood>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setFood(amount, toExtraStorage, toFreeTradeMarket);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setFood?.call(amount, toExtraStorage, toFreeTradeMarket);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setFood != null) {
      return setFood(amount, toExtraStorage, toFreeTradeMarket);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setFood(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setFood?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setFood != null) {
      return setFood(this);
    }
    return orElse();
  }
}

abstract class _SetFood implements CcEvent {
  const factory _SetFood(final int amount,
      {final bool toExtraStorage, final bool toFreeTradeMarket}) = _$_SetFood;

  int get amount;
  bool get toExtraStorage;
  bool get toFreeTradeMarket;
  @JsonKey(ignore: true)
  _$$_SetFoodCopyWith<_$_SetFood> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetLuxuryCopyWith<$Res> {
  factory _$$_SetLuxuryCopyWith(
          _$_SetLuxury value, $Res Function(_$_SetLuxury) then) =
      __$$_SetLuxuryCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount, bool toExtraStorage, bool toFreeTradeMarket});
}

/// @nodoc
class __$$_SetLuxuryCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetLuxury>
    implements _$$_SetLuxuryCopyWith<$Res> {
  __$$_SetLuxuryCopyWithImpl(
      _$_SetLuxury _value, $Res Function(_$_SetLuxury) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? toExtraStorage = null,
    Object? toFreeTradeMarket = null,
  }) {
    return _then(_$_SetLuxury(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      toExtraStorage: null == toExtraStorage
          ? _value.toExtraStorage
          : toExtraStorage // ignore: cast_nullable_to_non_nullable
              as bool,
      toFreeTradeMarket: null == toFreeTradeMarket
          ? _value.toFreeTradeMarket
          : toFreeTradeMarket // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetLuxury implements _SetLuxury {
  const _$_SetLuxury(this.amount,
      {this.toExtraStorage = false, this.toFreeTradeMarket = false});

  @override
  final int amount;
  @override
  @JsonKey()
  final bool toExtraStorage;
  @override
  @JsonKey()
  final bool toFreeTradeMarket;

  @override
  String toString() {
    return 'CcEvent.setLuxury(amount: $amount, toExtraStorage: $toExtraStorage, toFreeTradeMarket: $toFreeTradeMarket)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetLuxury &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.toExtraStorage, toExtraStorage) ||
                other.toExtraStorage == toExtraStorage) &&
            (identical(other.toFreeTradeMarket, toFreeTradeMarket) ||
                other.toFreeTradeMarket == toFreeTradeMarket));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, amount, toExtraStorage, toFreeTradeMarket);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetLuxuryCopyWith<_$_SetLuxury> get copyWith =>
      __$$_SetLuxuryCopyWithImpl<_$_SetLuxury>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setLuxury(amount, toExtraStorage, toFreeTradeMarket);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setLuxury?.call(amount, toExtraStorage, toFreeTradeMarket);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setLuxury != null) {
      return setLuxury(amount, toExtraStorage, toFreeTradeMarket);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setLuxury(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setLuxury?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setLuxury != null) {
      return setLuxury(this);
    }
    return orElse();
  }
}

abstract class _SetLuxury implements CcEvent {
  const factory _SetLuxury(final int amount,
      {final bool toExtraStorage, final bool toFreeTradeMarket}) = _$_SetLuxury;

  int get amount;
  bool get toExtraStorage;
  bool get toFreeTradeMarket;
  @JsonKey(ignore: true)
  _$$_SetLuxuryCopyWith<_$_SetLuxury> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetHealthcareCopyWith<$Res> {
  factory _$$_SetHealthcareCopyWith(
          _$_SetHealthcare value, $Res Function(_$_SetHealthcare) then) =
      __$$_SetHealthcareCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount, bool toExtraStorage});
}

/// @nodoc
class __$$_SetHealthcareCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetHealthcare>
    implements _$$_SetHealthcareCopyWith<$Res> {
  __$$_SetHealthcareCopyWithImpl(
      _$_SetHealthcare _value, $Res Function(_$_SetHealthcare) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? toExtraStorage = null,
  }) {
    return _then(_$_SetHealthcare(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      toExtraStorage: null == toExtraStorage
          ? _value.toExtraStorage
          : toExtraStorage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetHealthcare implements _SetHealthcare {
  const _$_SetHealthcare(this.amount, {this.toExtraStorage = false});

  @override
  final int amount;
  @override
  @JsonKey()
  final bool toExtraStorage;

  @override
  String toString() {
    return 'CcEvent.setHealthcare(amount: $amount, toExtraStorage: $toExtraStorage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetHealthcare &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.toExtraStorage, toExtraStorage) ||
                other.toExtraStorage == toExtraStorage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount, toExtraStorage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetHealthcareCopyWith<_$_SetHealthcare> get copyWith =>
      __$$_SetHealthcareCopyWithImpl<_$_SetHealthcare>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setHealthcare(amount, toExtraStorage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setHealthcare?.call(amount, toExtraStorage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setHealthcare != null) {
      return setHealthcare(amount, toExtraStorage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setHealthcare(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setHealthcare?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setHealthcare != null) {
      return setHealthcare(this);
    }
    return orElse();
  }
}

abstract class _SetHealthcare implements CcEvent {
  const factory _SetHealthcare(final int amount, {final bool toExtraStorage}) =
      _$_SetHealthcare;

  int get amount;
  bool get toExtraStorage;
  @JsonKey(ignore: true)
  _$$_SetHealthcareCopyWith<_$_SetHealthcare> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetEducationCopyWith<$Res> {
  factory _$$_SetEducationCopyWith(
          _$_SetEducation value, $Res Function(_$_SetEducation) then) =
      __$$_SetEducationCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount, bool toExtraStorage});
}

/// @nodoc
class __$$_SetEducationCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetEducation>
    implements _$$_SetEducationCopyWith<$Res> {
  __$$_SetEducationCopyWithImpl(
      _$_SetEducation _value, $Res Function(_$_SetEducation) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? toExtraStorage = null,
  }) {
    return _then(_$_SetEducation(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      toExtraStorage: null == toExtraStorage
          ? _value.toExtraStorage
          : toExtraStorage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetEducation implements _SetEducation {
  const _$_SetEducation(this.amount, {this.toExtraStorage = false});

  @override
  final int amount;
  @override
  @JsonKey()
  final bool toExtraStorage;

  @override
  String toString() {
    return 'CcEvent.setEducation(amount: $amount, toExtraStorage: $toExtraStorage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetEducation &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.toExtraStorage, toExtraStorage) ||
                other.toExtraStorage == toExtraStorage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount, toExtraStorage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetEducationCopyWith<_$_SetEducation> get copyWith =>
      __$$_SetEducationCopyWithImpl<_$_SetEducation>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setEducation(amount, toExtraStorage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setEducation?.call(amount, toExtraStorage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setEducation != null) {
      return setEducation(amount, toExtraStorage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setEducation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setEducation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setEducation != null) {
      return setEducation(this);
    }
    return orElse();
  }
}

abstract class _SetEducation implements CcEvent {
  const factory _SetEducation(final int amount, {final bool toExtraStorage}) =
      _$_SetEducation;

  int get amount;
  bool get toExtraStorage;
  @JsonKey(ignore: true)
  _$$_SetEducationCopyWith<_$_SetEducation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetInfluenceCopyWith<$Res> {
  factory _$$_SetInfluenceCopyWith(
          _$_SetInfluence value, $Res Function(_$_SetInfluence) then) =
      __$$_SetInfluenceCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount});
}

/// @nodoc
class __$$_SetInfluenceCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetInfluence>
    implements _$$_SetInfluenceCopyWith<$Res> {
  __$$_SetInfluenceCopyWithImpl(
      _$_SetInfluence _value, $Res Function(_$_SetInfluence) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
  }) {
    return _then(_$_SetInfluence(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetInfluence implements _SetInfluence {
  const _$_SetInfluence(this.amount);

  @override
  final int amount;

  @override
  String toString() {
    return 'CcEvent.setInfluence(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetInfluence &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetInfluenceCopyWith<_$_SetInfluence> get copyWith =>
      __$$_SetInfluenceCopyWithImpl<_$_SetInfluence>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setInfluence(amount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setInfluence?.call(amount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setInfluence != null) {
      return setInfluence(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setInfluence(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setInfluence?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setInfluence != null) {
      return setInfluence(this);
    }
    return orElse();
  }
}

abstract class _SetInfluence implements CcEvent {
  const factory _SetInfluence(final int amount) = _$_SetInfluence;

  int get amount;
  @JsonKey(ignore: true)
  _$$_SetInfluenceCopyWith<_$_SetInfluence> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_WealthTrackPositionSelectedCopyWith<$Res> {
  factory _$$_WealthTrackPositionSelectedCopyWith(
          _$_WealthTrackPositionSelected value,
          $Res Function(_$_WealthTrackPositionSelected) then) =
      __$$_WealthTrackPositionSelectedCopyWithImpl<$Res>;
  @useResult
  $Res call({int position});
}

/// @nodoc
class __$$_WealthTrackPositionSelectedCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_WealthTrackPositionSelected>
    implements _$$_WealthTrackPositionSelectedCopyWith<$Res> {
  __$$_WealthTrackPositionSelectedCopyWithImpl(
      _$_WealthTrackPositionSelected _value,
      $Res Function(_$_WealthTrackPositionSelected) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? position = null,
  }) {
    return _then(_$_WealthTrackPositionSelected(
      null == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_WealthTrackPositionSelected implements _WealthTrackPositionSelected {
  const _$_WealthTrackPositionSelected(this.position);

  @override
  final int position;

  @override
  String toString() {
    return 'CcEvent.wealthTrackPositionSelected(position: $position)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WealthTrackPositionSelected &&
            (identical(other.position, position) ||
                other.position == position));
  }

  @override
  int get hashCode => Object.hash(runtimeType, position);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WealthTrackPositionSelectedCopyWith<_$_WealthTrackPositionSelected>
      get copyWith => __$$_WealthTrackPositionSelectedCopyWithImpl<
          _$_WealthTrackPositionSelected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return wealthTrackPositionSelected(position);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return wealthTrackPositionSelected?.call(position);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (wealthTrackPositionSelected != null) {
      return wealthTrackPositionSelected(position);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return wealthTrackPositionSelected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return wealthTrackPositionSelected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (wealthTrackPositionSelected != null) {
      return wealthTrackPositionSelected(this);
    }
    return orElse();
  }
}

abstract class _WealthTrackPositionSelected implements CcEvent {
  const factory _WealthTrackPositionSelected(final int position) =
      _$_WealthTrackPositionSelected;

  int get position;
  @JsonKey(ignore: true)
  _$$_WealthTrackPositionSelectedCopyWith<_$_WealthTrackPositionSelected>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_BuyStorageCopyWith<$Res> {
  factory _$$_BuyStorageCopyWith(
          _$_BuyStorage value, $Res Function(_$_BuyStorage) then) =
      __$$_BuyStorageCopyWithImpl<$Res>;
  @useResult
  $Res call({GoodOrServiceType type});
}

/// @nodoc
class __$$_BuyStorageCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_BuyStorage>
    implements _$$_BuyStorageCopyWith<$Res> {
  __$$_BuyStorageCopyWithImpl(
      _$_BuyStorage _value, $Res Function(_$_BuyStorage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
  }) {
    return _then(_$_BuyStorage(
      null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
    ));
  }
}

/// @nodoc

class _$_BuyStorage implements _BuyStorage {
  const _$_BuyStorage(this.type);

  @override
  final GoodOrServiceType type;

  @override
  String toString() {
    return 'CcEvent.buyStorage(type: $type)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BuyStorage &&
            (identical(other.type, type) || other.type == type));
  }

  @override
  int get hashCode => Object.hash(runtimeType, type);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BuyStorageCopyWith<_$_BuyStorage> get copyWith =>
      __$$_BuyStorageCopyWithImpl<_$_BuyStorage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return buyStorage(type);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return buyStorage?.call(type);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (buyStorage != null) {
      return buyStorage(type);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return buyStorage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return buyStorage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (buyStorage != null) {
      return buyStorage(this);
    }
    return orElse();
  }
}

abstract class _BuyStorage implements CcEvent {
  const factory _BuyStorage(final GoodOrServiceType type) = _$_BuyStorage;

  GoodOrServiceType get type;
  @JsonKey(ignore: true)
  _$$_BuyStorageCopyWith<_$_BuyStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetPriceCopyWith<$Res> {
  factory _$$_SetPriceCopyWith(
          _$_SetPrice value, $Res Function(_$_SetPrice) then) =
      __$$_SetPriceCopyWithImpl<$Res>;
  @useResult
  $Res call({GoodOrServiceType type, int price});
}

/// @nodoc
class __$$_SetPriceCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_SetPrice>
    implements _$$_SetPriceCopyWith<$Res> {
  __$$_SetPriceCopyWithImpl(
      _$_SetPrice _value, $Res Function(_$_SetPrice) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? price = null,
  }) {
    return _then(_$_SetPrice(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetPrice implements _SetPrice {
  const _$_SetPrice({required this.type, required this.price});

  @override
  final GoodOrServiceType type;
  @override
  final int price;

  @override
  String toString() {
    return 'CcEvent.setPrice(type: $type, price: $price)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetPrice &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.price, price) || other.price == price));
  }

  @override
  int get hashCode => Object.hash(runtimeType, type, price);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SetPriceCopyWith<_$_SetPrice> get copyWith =>
      __$$_SetPriceCopyWithImpl<_$_SetPrice>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return setPrice(type, price);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return setPrice?.call(type, price);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (setPrice != null) {
      return setPrice(type, price);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return setPrice(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return setPrice?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (setPrice != null) {
      return setPrice(this);
    }
    return orElse();
  }
}

abstract class _SetPrice implements CcEvent {
  const factory _SetPrice(
      {required final GoodOrServiceType type,
      required final int price}) = _$_SetPrice;

  GoodOrServiceType get type;
  int get price;
  @JsonKey(ignore: true)
  _$$_SetPriceCopyWith<_$_SetPrice> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DrawCompaniesCopyWith<$Res> {
  factory _$$_DrawCompaniesCopyWith(
          _$_DrawCompanies value, $Res Function(_$_DrawCompanies) then) =
      __$$_DrawCompaniesCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount});
}

/// @nodoc
class __$$_DrawCompaniesCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_DrawCompanies>
    implements _$$_DrawCompaniesCopyWith<$Res> {
  __$$_DrawCompaniesCopyWithImpl(
      _$_DrawCompanies _value, $Res Function(_$_DrawCompanies) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
  }) {
    return _then(_$_DrawCompanies(
      null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_DrawCompanies implements _DrawCompanies {
  const _$_DrawCompanies(this.amount);

  @override
  final int amount;

  @override
  String toString() {
    return 'CcEvent.drawCompanies(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DrawCompanies &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @override
  int get hashCode => Object.hash(runtimeType, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DrawCompaniesCopyWith<_$_DrawCompanies> get copyWith =>
      __$$_DrawCompaniesCopyWithImpl<_$_DrawCompanies>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return drawCompanies(amount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return drawCompanies?.call(amount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawCompanies != null) {
      return drawCompanies(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return drawCompanies(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return drawCompanies?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawCompanies != null) {
      return drawCompanies(this);
    }
    return orElse();
  }
}

abstract class _DrawCompanies implements CcEvent {
  const factory _DrawCompanies(final int amount) = _$_DrawCompanies;

  int get amount;
  @JsonKey(ignore: true)
  _$$_DrawCompaniesCopyWith<_$_DrawCompanies> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DrawCompanyOnIndexCopyWith<$Res> {
  factory _$$_DrawCompanyOnIndexCopyWith(_$_DrawCompanyOnIndex value,
          $Res Function(_$_DrawCompanyOnIndex) then) =
      __$$_DrawCompanyOnIndexCopyWithImpl<$Res>;
  @useResult
  $Res call({int index});
}

/// @nodoc
class __$$_DrawCompanyOnIndexCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_DrawCompanyOnIndex>
    implements _$$_DrawCompanyOnIndexCopyWith<$Res> {
  __$$_DrawCompanyOnIndexCopyWithImpl(
      _$_DrawCompanyOnIndex _value, $Res Function(_$_DrawCompanyOnIndex) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? index = null,
  }) {
    return _then(_$_DrawCompanyOnIndex(
      null == index
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_DrawCompanyOnIndex implements _DrawCompanyOnIndex {
  const _$_DrawCompanyOnIndex(this.index);

  @override
  final int index;

  @override
  String toString() {
    return 'CcEvent.drawCompanyOnIndex(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DrawCompanyOnIndex &&
            (identical(other.index, index) || other.index == index));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DrawCompanyOnIndexCopyWith<_$_DrawCompanyOnIndex> get copyWith =>
      __$$_DrawCompanyOnIndexCopyWithImpl<_$_DrawCompanyOnIndex>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return drawCompanyOnIndex(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return drawCompanyOnIndex?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawCompanyOnIndex != null) {
      return drawCompanyOnIndex(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return drawCompanyOnIndex(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return drawCompanyOnIndex?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawCompanyOnIndex != null) {
      return drawCompanyOnIndex(this);
    }
    return orElse();
  }
}

abstract class _DrawCompanyOnIndex implements CcEvent {
  const factory _DrawCompanyOnIndex(final int index) = _$_DrawCompanyOnIndex;

  int get index;
  @JsonKey(ignore: true)
  _$$_DrawCompanyOnIndexCopyWith<_$_DrawCompanyOnIndex> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_RemoveCompanyCopyWith<$Res> {
  factory _$$_RemoveCompanyCopyWith(
          _$_RemoveCompany value, $Res Function(_$_RemoveCompany) then) =
      __$$_RemoveCompanyCopyWithImpl<$Res>;
  @useResult
  $Res call({int index});
}

/// @nodoc
class __$$_RemoveCompanyCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_RemoveCompany>
    implements _$$_RemoveCompanyCopyWith<$Res> {
  __$$_RemoveCompanyCopyWithImpl(
      _$_RemoveCompany _value, $Res Function(_$_RemoveCompany) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? index = null,
  }) {
    return _then(_$_RemoveCompany(
      null == index
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_RemoveCompany implements _RemoveCompany {
  const _$_RemoveCompany(this.index);

  @override
  final int index;

  @override
  String toString() {
    return 'CcEvent.removeCompany(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RemoveCompany &&
            (identical(other.index, index) || other.index == index));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RemoveCompanyCopyWith<_$_RemoveCompany> get copyWith =>
      __$$_RemoveCompanyCopyWithImpl<_$_RemoveCompany>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return removeCompany(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return removeCompany?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (removeCompany != null) {
      return removeCompany(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return removeCompany(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return removeCompany?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (removeCompany != null) {
      return removeCompany(this);
    }
    return orElse();
  }
}

abstract class _RemoveCompany implements CcEvent {
  const factory _RemoveCompany(final int index) = _$_RemoveCompany;

  int get index;
  @JsonKey(ignore: true)
  _$$_RemoveCompanyCopyWith<_$_RemoveCompany> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ReshuffleDiscartedCompaniesCopyWith<$Res> {
  factory _$$_ReshuffleDiscartedCompaniesCopyWith(
          _$_ReshuffleDiscartedCompanies value,
          $Res Function(_$_ReshuffleDiscartedCompanies) then) =
      __$$_ReshuffleDiscartedCompaniesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ReshuffleDiscartedCompaniesCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_ReshuffleDiscartedCompanies>
    implements _$$_ReshuffleDiscartedCompaniesCopyWith<$Res> {
  __$$_ReshuffleDiscartedCompaniesCopyWithImpl(
      _$_ReshuffleDiscartedCompanies _value,
      $Res Function(_$_ReshuffleDiscartedCompanies) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ReshuffleDiscartedCompanies implements _ReshuffleDiscartedCompanies {
  const _$_ReshuffleDiscartedCompanies();

  @override
  String toString() {
    return 'CcEvent.reshuffleDiscartedCompanies()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ReshuffleDiscartedCompanies);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return reshuffleDiscartedCompanies();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return reshuffleDiscartedCompanies?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (reshuffleDiscartedCompanies != null) {
      return reshuffleDiscartedCompanies();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return reshuffleDiscartedCompanies(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return reshuffleDiscartedCompanies?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (reshuffleDiscartedCompanies != null) {
      return reshuffleDiscartedCompanies(this);
    }
    return orElse();
  }
}

abstract class _ReshuffleDiscartedCompanies implements CcEvent {
  const factory _ReshuffleDiscartedCompanies() = _$_ReshuffleDiscartedCompanies;
}

/// @nodoc
abstract class _$$_DrawAiCardCopyWith<$Res> {
  factory _$$_DrawAiCardCopyWith(
          _$_DrawAiCard value, $Res Function(_$_DrawAiCard) then) =
      __$$_DrawAiCardCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_DrawAiCardCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_DrawAiCard>
    implements _$$_DrawAiCardCopyWith<$Res> {
  __$$_DrawAiCardCopyWithImpl(
      _$_DrawAiCard _value, $Res Function(_$_DrawAiCard) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_DrawAiCard implements _DrawAiCard {
  const _$_DrawAiCard();

  @override
  String toString() {
    return 'CcEvent.drawAiCard()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_DrawAiCard);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return drawAiCard();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return drawAiCard?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawAiCard != null) {
      return drawAiCard();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return drawAiCard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return drawAiCard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (drawAiCard != null) {
      return drawAiCard(this);
    }
    return orElse();
  }
}

abstract class _DrawAiCard implements CcEvent {
  const factory _DrawAiCard() = _$_DrawAiCard;
}

/// @nodoc
abstract class _$$_CheckAiCardCopyWith<$Res> {
  factory _$$_CheckAiCardCopyWith(
          _$_CheckAiCard value, $Res Function(_$_CheckAiCard) then) =
      __$$_CheckAiCardCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CheckAiCardCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_CheckAiCard>
    implements _$$_CheckAiCardCopyWith<$Res> {
  __$$_CheckAiCardCopyWithImpl(
      _$_CheckAiCard _value, $Res Function(_$_CheckAiCard) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_CheckAiCard implements _CheckAiCard {
  const _$_CheckAiCard();

  @override
  String toString() {
    return 'CcEvent.checkAiCard()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CheckAiCard);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return checkAiCard();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return checkAiCard?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (checkAiCard != null) {
      return checkAiCard();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return checkAiCard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return checkAiCard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (checkAiCard != null) {
      return checkAiCard(this);
    }
    return orElse();
  }
}

abstract class _CheckAiCard implements CcEvent {
  const factory _CheckAiCard() = _$_CheckAiCard;
}

/// @nodoc
abstract class _$$_ReshuffleDiscartedAiCardsCopyWith<$Res> {
  factory _$$_ReshuffleDiscartedAiCardsCopyWith(
          _$_ReshuffleDiscartedAiCards value,
          $Res Function(_$_ReshuffleDiscartedAiCards) then) =
      __$$_ReshuffleDiscartedAiCardsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ReshuffleDiscartedAiCardsCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_ReshuffleDiscartedAiCards>
    implements _$$_ReshuffleDiscartedAiCardsCopyWith<$Res> {
  __$$_ReshuffleDiscartedAiCardsCopyWithImpl(
      _$_ReshuffleDiscartedAiCards _value,
      $Res Function(_$_ReshuffleDiscartedAiCards) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_ReshuffleDiscartedAiCards implements _ReshuffleDiscartedAiCards {
  const _$_ReshuffleDiscartedAiCards();

  @override
  String toString() {
    return 'CcEvent.reshuffleDiscartedAiCards()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ReshuffleDiscartedAiCards);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return reshuffleDiscartedAiCards();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return reshuffleDiscartedAiCards?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (reshuffleDiscartedAiCards != null) {
      return reshuffleDiscartedAiCards();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return reshuffleDiscartedAiCards(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return reshuffleDiscartedAiCards?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (reshuffleDiscartedAiCards != null) {
      return reshuffleDiscartedAiCards(this);
    }
    return orElse();
  }
}

abstract class _ReshuffleDiscartedAiCards implements CcEvent {
  const factory _ReshuffleDiscartedAiCards() = _$_ReshuffleDiscartedAiCards;
}

/// @nodoc
abstract class _$$_TakeLoanCopyWith<$Res> {
  factory _$$_TakeLoanCopyWith(
          _$_TakeLoan value, $Res Function(_$_TakeLoan) then) =
      __$$_TakeLoanCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_TakeLoanCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_TakeLoan>
    implements _$$_TakeLoanCopyWith<$Res> {
  __$$_TakeLoanCopyWithImpl(
      _$_TakeLoan _value, $Res Function(_$_TakeLoan) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_TakeLoan implements _TakeLoan {
  const _$_TakeLoan();

  @override
  String toString() {
    return 'CcEvent.takeLoan()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_TakeLoan);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return takeLoan();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return takeLoan?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (takeLoan != null) {
      return takeLoan();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return takeLoan(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return takeLoan?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (takeLoan != null) {
      return takeLoan(this);
    }
    return orElse();
  }
}

abstract class _TakeLoan implements CcEvent {
  const factory _TakeLoan() = _$_TakeLoan;
}

/// @nodoc
abstract class _$$_PayOffLoanCopyWith<$Res> {
  factory _$$_PayOffLoanCopyWith(
          _$_PayOffLoan value, $Res Function(_$_PayOffLoan) then) =
      __$$_PayOffLoanCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_PayOffLoanCopyWithImpl<$Res>
    extends _$CcEventCopyWithImpl<$Res, _$_PayOffLoan>
    implements _$$_PayOffLoanCopyWith<$Res> {
  __$$_PayOffLoanCopyWithImpl(
      _$_PayOffLoan _value, $Res Function(_$_PayOffLoan) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_PayOffLoan implements _PayOffLoan {
  const _$_PayOffLoan();

  @override
  String toString() {
    return 'CcEvent.payOffLoan()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_PayOffLoan);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() gameBoardUpdated,
    required TResult Function(int amount) setRevenue,
    required TResult Function(int amount) setCapital,
    required TResult Function() moveRevenueToCapital,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setFood,
    required TResult Function(
            int amount, bool toExtraStorage, bool toFreeTradeMarket)
        setLuxury,
    required TResult Function(int amount, bool toExtraStorage) setHealthcare,
    required TResult Function(int amount, bool toExtraStorage) setEducation,
    required TResult Function(int amount) setInfluence,
    required TResult Function(int position) wealthTrackPositionSelected,
    required TResult Function(GoodOrServiceType type) buyStorage,
    required TResult Function(GoodOrServiceType type, int price) setPrice,
    required TResult Function(int amount) drawCompanies,
    required TResult Function(int index) drawCompanyOnIndex,
    required TResult Function(int index) removeCompany,
    required TResult Function() reshuffleDiscartedCompanies,
    required TResult Function() drawAiCard,
    required TResult Function() checkAiCard,
    required TResult Function() reshuffleDiscartedAiCards,
    required TResult Function() takeLoan,
    required TResult Function() payOffLoan,
  }) {
    return payOffLoan();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? gameBoardUpdated,
    TResult? Function(int amount)? setRevenue,
    TResult? Function(int amount)? setCapital,
    TResult? Function()? moveRevenueToCapital,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult? Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult? Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult? Function(int amount, bool toExtraStorage)? setEducation,
    TResult? Function(int amount)? setInfluence,
    TResult? Function(int position)? wealthTrackPositionSelected,
    TResult? Function(GoodOrServiceType type)? buyStorage,
    TResult? Function(GoodOrServiceType type, int price)? setPrice,
    TResult? Function(int amount)? drawCompanies,
    TResult? Function(int index)? drawCompanyOnIndex,
    TResult? Function(int index)? removeCompany,
    TResult? Function()? reshuffleDiscartedCompanies,
    TResult? Function()? drawAiCard,
    TResult? Function()? checkAiCard,
    TResult? Function()? reshuffleDiscartedAiCards,
    TResult? Function()? takeLoan,
    TResult? Function()? payOffLoan,
  }) {
    return payOffLoan?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? gameBoardUpdated,
    TResult Function(int amount)? setRevenue,
    TResult Function(int amount)? setCapital,
    TResult Function()? moveRevenueToCapital,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setFood,
    TResult Function(int amount, bool toExtraStorage, bool toFreeTradeMarket)?
        setLuxury,
    TResult Function(int amount, bool toExtraStorage)? setHealthcare,
    TResult Function(int amount, bool toExtraStorage)? setEducation,
    TResult Function(int amount)? setInfluence,
    TResult Function(int position)? wealthTrackPositionSelected,
    TResult Function(GoodOrServiceType type)? buyStorage,
    TResult Function(GoodOrServiceType type, int price)? setPrice,
    TResult Function(int amount)? drawCompanies,
    TResult Function(int index)? drawCompanyOnIndex,
    TResult Function(int index)? removeCompany,
    TResult Function()? reshuffleDiscartedCompanies,
    TResult Function()? drawAiCard,
    TResult Function()? checkAiCard,
    TResult Function()? reshuffleDiscartedAiCards,
    TResult Function()? takeLoan,
    TResult Function()? payOffLoan,
    required TResult orElse(),
  }) {
    if (payOffLoan != null) {
      return payOffLoan();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_GameBoardUpdated value) gameBoardUpdated,
    required TResult Function(_SetRevenue value) setRevenue,
    required TResult Function(_SetCapital value) setCapital,
    required TResult Function(_MoveRevenueToCapital value) moveRevenueToCapital,
    required TResult Function(_SetFood value) setFood,
    required TResult Function(_SetLuxury value) setLuxury,
    required TResult Function(_SetHealthcare value) setHealthcare,
    required TResult Function(_SetEducation value) setEducation,
    required TResult Function(_SetInfluence value) setInfluence,
    required TResult Function(_WealthTrackPositionSelected value)
        wealthTrackPositionSelected,
    required TResult Function(_BuyStorage value) buyStorage,
    required TResult Function(_SetPrice value) setPrice,
    required TResult Function(_DrawCompanies value) drawCompanies,
    required TResult Function(_DrawCompanyOnIndex value) drawCompanyOnIndex,
    required TResult Function(_RemoveCompany value) removeCompany,
    required TResult Function(_ReshuffleDiscartedCompanies value)
        reshuffleDiscartedCompanies,
    required TResult Function(_DrawAiCard value) drawAiCard,
    required TResult Function(_CheckAiCard value) checkAiCard,
    required TResult Function(_ReshuffleDiscartedAiCards value)
        reshuffleDiscartedAiCards,
    required TResult Function(_TakeLoan value) takeLoan,
    required TResult Function(_PayOffLoan value) payOffLoan,
  }) {
    return payOffLoan(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult? Function(_SetRevenue value)? setRevenue,
    TResult? Function(_SetCapital value)? setCapital,
    TResult? Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult? Function(_SetFood value)? setFood,
    TResult? Function(_SetLuxury value)? setLuxury,
    TResult? Function(_SetHealthcare value)? setHealthcare,
    TResult? Function(_SetEducation value)? setEducation,
    TResult? Function(_SetInfluence value)? setInfluence,
    TResult? Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult? Function(_BuyStorage value)? buyStorage,
    TResult? Function(_SetPrice value)? setPrice,
    TResult? Function(_DrawCompanies value)? drawCompanies,
    TResult? Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult? Function(_RemoveCompany value)? removeCompany,
    TResult? Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult? Function(_DrawAiCard value)? drawAiCard,
    TResult? Function(_CheckAiCard value)? checkAiCard,
    TResult? Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult? Function(_TakeLoan value)? takeLoan,
    TResult? Function(_PayOffLoan value)? payOffLoan,
  }) {
    return payOffLoan?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_GameBoardUpdated value)? gameBoardUpdated,
    TResult Function(_SetRevenue value)? setRevenue,
    TResult Function(_SetCapital value)? setCapital,
    TResult Function(_MoveRevenueToCapital value)? moveRevenueToCapital,
    TResult Function(_SetFood value)? setFood,
    TResult Function(_SetLuxury value)? setLuxury,
    TResult Function(_SetHealthcare value)? setHealthcare,
    TResult Function(_SetEducation value)? setEducation,
    TResult Function(_SetInfluence value)? setInfluence,
    TResult Function(_WealthTrackPositionSelected value)?
        wealthTrackPositionSelected,
    TResult Function(_BuyStorage value)? buyStorage,
    TResult Function(_SetPrice value)? setPrice,
    TResult Function(_DrawCompanies value)? drawCompanies,
    TResult Function(_DrawCompanyOnIndex value)? drawCompanyOnIndex,
    TResult Function(_RemoveCompany value)? removeCompany,
    TResult Function(_ReshuffleDiscartedCompanies value)?
        reshuffleDiscartedCompanies,
    TResult Function(_DrawAiCard value)? drawAiCard,
    TResult Function(_CheckAiCard value)? checkAiCard,
    TResult Function(_ReshuffleDiscartedAiCards value)?
        reshuffleDiscartedAiCards,
    TResult Function(_TakeLoan value)? takeLoan,
    TResult Function(_PayOffLoan value)? payOffLoan,
    required TResult orElse(),
  }) {
    if (payOffLoan != null) {
      return payOffLoan(this);
    }
    return orElse();
  }
}

abstract class _PayOffLoan implements CcEvent {
  const factory _PayOffLoan() = _$_PayOffLoan;
}

/// @nodoc
mixin _$CcState {
  String? get showMessage => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  List<String> get reminders => throw _privateConstructorUsedError;
  int get corporateTax => throw _privateConstructorUsedError;
  CcPageVm get ccPageVm => throw _privateConstructorUsedError;
  List<InstructionCardVm> get instructionCards =>
      throw _privateConstructorUsedError;
  CcPlayerBoardVm get ccPlayerBoardVm => throw _privateConstructorUsedError;
  CcEndOfGameScoringVm get endOfGameScoring =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcStateCopyWith<CcState> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcStateCopyWith<$Res> {
  factory $CcStateCopyWith(CcState value, $Res Function(CcState) then) =
      _$CcStateCopyWithImpl<$Res, CcState>;
  @useResult
  $Res call(
      {String? showMessage,
      bool loading,
      List<String> reminders,
      int corporateTax,
      CcPageVm ccPageVm,
      List<InstructionCardVm> instructionCards,
      CcPlayerBoardVm ccPlayerBoardVm,
      CcEndOfGameScoringVm endOfGameScoring});

  $CcPageVmCopyWith<$Res> get ccPageVm;
  $CcPlayerBoardVmCopyWith<$Res> get ccPlayerBoardVm;
  $CcEndOfGameScoringVmCopyWith<$Res> get endOfGameScoring;
}

/// @nodoc
class _$CcStateCopyWithImpl<$Res, $Val extends CcState>
    implements $CcStateCopyWith<$Res> {
  _$CcStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showMessage = freezed,
    Object? loading = null,
    Object? reminders = null,
    Object? corporateTax = null,
    Object? ccPageVm = null,
    Object? instructionCards = null,
    Object? ccPlayerBoardVm = null,
    Object? endOfGameScoring = null,
  }) {
    return _then(_value.copyWith(
      showMessage: freezed == showMessage
          ? _value.showMessage
          : showMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      reminders: null == reminders
          ? _value.reminders
          : reminders // ignore: cast_nullable_to_non_nullable
              as List<String>,
      corporateTax: null == corporateTax
          ? _value.corporateTax
          : corporateTax // ignore: cast_nullable_to_non_nullable
              as int,
      ccPageVm: null == ccPageVm
          ? _value.ccPageVm
          : ccPageVm // ignore: cast_nullable_to_non_nullable
              as CcPageVm,
      instructionCards: null == instructionCards
          ? _value.instructionCards
          : instructionCards // ignore: cast_nullable_to_non_nullable
              as List<InstructionCardVm>,
      ccPlayerBoardVm: null == ccPlayerBoardVm
          ? _value.ccPlayerBoardVm
          : ccPlayerBoardVm // ignore: cast_nullable_to_non_nullable
              as CcPlayerBoardVm,
      endOfGameScoring: null == endOfGameScoring
          ? _value.endOfGameScoring
          : endOfGameScoring // ignore: cast_nullable_to_non_nullable
              as CcEndOfGameScoringVm,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $CcPageVmCopyWith<$Res> get ccPageVm {
    return $CcPageVmCopyWith<$Res>(_value.ccPageVm, (value) {
      return _then(_value.copyWith(ccPageVm: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $CcPlayerBoardVmCopyWith<$Res> get ccPlayerBoardVm {
    return $CcPlayerBoardVmCopyWith<$Res>(_value.ccPlayerBoardVm, (value) {
      return _then(_value.copyWith(ccPlayerBoardVm: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $CcEndOfGameScoringVmCopyWith<$Res> get endOfGameScoring {
    return $CcEndOfGameScoringVmCopyWith<$Res>(_value.endOfGameScoring,
        (value) {
      return _then(_value.copyWith(endOfGameScoring: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_CcStateCopyWith<$Res> implements $CcStateCopyWith<$Res> {
  factory _$$_CcStateCopyWith(
          _$_CcState value, $Res Function(_$_CcState) then) =
      __$$_CcStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? showMessage,
      bool loading,
      List<String> reminders,
      int corporateTax,
      CcPageVm ccPageVm,
      List<InstructionCardVm> instructionCards,
      CcPlayerBoardVm ccPlayerBoardVm,
      CcEndOfGameScoringVm endOfGameScoring});

  @override
  $CcPageVmCopyWith<$Res> get ccPageVm;
  @override
  $CcPlayerBoardVmCopyWith<$Res> get ccPlayerBoardVm;
  @override
  $CcEndOfGameScoringVmCopyWith<$Res> get endOfGameScoring;
}

/// @nodoc
class __$$_CcStateCopyWithImpl<$Res>
    extends _$CcStateCopyWithImpl<$Res, _$_CcState>
    implements _$$_CcStateCopyWith<$Res> {
  __$$_CcStateCopyWithImpl(_$_CcState _value, $Res Function(_$_CcState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? showMessage = freezed,
    Object? loading = null,
    Object? reminders = null,
    Object? corporateTax = null,
    Object? ccPageVm = null,
    Object? instructionCards = null,
    Object? ccPlayerBoardVm = null,
    Object? endOfGameScoring = null,
  }) {
    return _then(_$_CcState(
      showMessage: freezed == showMessage
          ? _value.showMessage
          : showMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      reminders: null == reminders
          ? _value._reminders
          : reminders // ignore: cast_nullable_to_non_nullable
              as List<String>,
      corporateTax: null == corporateTax
          ? _value.corporateTax
          : corporateTax // ignore: cast_nullable_to_non_nullable
              as int,
      ccPageVm: null == ccPageVm
          ? _value.ccPageVm
          : ccPageVm // ignore: cast_nullable_to_non_nullable
              as CcPageVm,
      instructionCards: null == instructionCards
          ? _value._instructionCards
          : instructionCards // ignore: cast_nullable_to_non_nullable
              as List<InstructionCardVm>,
      ccPlayerBoardVm: null == ccPlayerBoardVm
          ? _value.ccPlayerBoardVm
          : ccPlayerBoardVm // ignore: cast_nullable_to_non_nullable
              as CcPlayerBoardVm,
      endOfGameScoring: null == endOfGameScoring
          ? _value.endOfGameScoring
          : endOfGameScoring // ignore: cast_nullable_to_non_nullable
              as CcEndOfGameScoringVm,
    ));
  }
}

/// @nodoc

class _$_CcState implements _CcState {
  const _$_CcState(
      {this.showMessage = null,
      this.loading = false,
      required final List<String> reminders,
      required this.corporateTax,
      required this.ccPageVm,
      required final List<InstructionCardVm> instructionCards,
      required this.ccPlayerBoardVm,
      required this.endOfGameScoring})
      : _reminders = reminders,
        _instructionCards = instructionCards;

  @override
  @JsonKey()
  final String? showMessage;
  @override
  @JsonKey()
  final bool loading;
  final List<String> _reminders;
  @override
  List<String> get reminders {
    if (_reminders is EqualUnmodifiableListView) return _reminders;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_reminders);
  }

  @override
  final int corporateTax;
  @override
  final CcPageVm ccPageVm;
  final List<InstructionCardVm> _instructionCards;
  @override
  List<InstructionCardVm> get instructionCards {
    if (_instructionCards is EqualUnmodifiableListView)
      return _instructionCards;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_instructionCards);
  }

  @override
  final CcPlayerBoardVm ccPlayerBoardVm;
  @override
  final CcEndOfGameScoringVm endOfGameScoring;

  @override
  String toString() {
    return 'CcState(showMessage: $showMessage, loading: $loading, reminders: $reminders, corporateTax: $corporateTax, ccPageVm: $ccPageVm, instructionCards: $instructionCards, ccPlayerBoardVm: $ccPlayerBoardVm, endOfGameScoring: $endOfGameScoring)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcState &&
            (identical(other.showMessage, showMessage) ||
                other.showMessage == showMessage) &&
            (identical(other.loading, loading) || other.loading == loading) &&
            const DeepCollectionEquality()
                .equals(other._reminders, _reminders) &&
            (identical(other.corporateTax, corporateTax) ||
                other.corporateTax == corporateTax) &&
            (identical(other.ccPageVm, ccPageVm) ||
                other.ccPageVm == ccPageVm) &&
            const DeepCollectionEquality()
                .equals(other._instructionCards, _instructionCards) &&
            (identical(other.ccPlayerBoardVm, ccPlayerBoardVm) ||
                other.ccPlayerBoardVm == ccPlayerBoardVm) &&
            (identical(other.endOfGameScoring, endOfGameScoring) ||
                other.endOfGameScoring == endOfGameScoring));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      showMessage,
      loading,
      const DeepCollectionEquality().hash(_reminders),
      corporateTax,
      ccPageVm,
      const DeepCollectionEquality().hash(_instructionCards),
      ccPlayerBoardVm,
      endOfGameScoring);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcStateCopyWith<_$_CcState> get copyWith =>
      __$$_CcStateCopyWithImpl<_$_CcState>(this, _$identity);
}

abstract class _CcState implements CcState {
  const factory _CcState(
      {final String? showMessage,
      final bool loading,
      required final List<String> reminders,
      required final int corporateTax,
      required final CcPageVm ccPageVm,
      required final List<InstructionCardVm> instructionCards,
      required final CcPlayerBoardVm ccPlayerBoardVm,
      required final CcEndOfGameScoringVm endOfGameScoring}) = _$_CcState;

  @override
  String? get showMessage;
  @override
  bool get loading;
  @override
  List<String> get reminders;
  @override
  int get corporateTax;
  @override
  CcPageVm get ccPageVm;
  @override
  List<InstructionCardVm> get instructionCards;
  @override
  CcPlayerBoardVm get ccPlayerBoardVm;
  @override
  CcEndOfGameScoringVm get endOfGameScoring;
  @override
  @JsonKey(ignore: true)
  _$$_CcStateCopyWith<_$_CcState> get copyWith =>
      throw _privateConstructorUsedError;
}
