part of 'cc_bloc.dart';

@freezed
class CcEvent with _$CcEvent {
  const factory CcEvent.started() = _Started;
  const factory CcEvent.gameBoardUpdated() = _GameBoardUpdated;
  const factory CcEvent.setRevenue(int amount) = _SetRevenue;
  const factory CcEvent.setCapital(int amount) = _SetCapital;
  const factory CcEvent.moveRevenueToCapital() = _MoveRevenueToCapital;

  const factory CcEvent.setFood(
    int amount, {
    @Default(false) bool toExtraStorage,
    @Default(false) bool toFreeTradeMarket,
  }) = _SetFood;
  const factory CcEvent.setLuxury(
    int amount, {
    @Default(false) bool toExtraStorage,
    @Default(false) bool toFreeTradeMarket,
  }) = _SetLuxury;
  const factory CcEvent.setHealthcare(
    int amount, {
    @Default(false) bool toExtraStorage,
  }) = _SetHealthcare;
  const factory CcEvent.setEducation(
    int amount, {
    @Default(false) bool toExtraStorage,
  }) = _SetEducation;
  const factory CcEvent.setInfluence(int amount) = _SetInfluence;
  const factory CcEvent.wealthTrackPositionSelected(int position) = _WealthTrackPositionSelected;
  const factory CcEvent.buyStorage(GoodOrServiceType type) = _BuyStorage;
  const factory CcEvent.setPrice({
    required GoodOrServiceType type,
    required int price,
  }) = _SetPrice;
  const factory CcEvent.drawCompanies(int amount) = _DrawCompanies;
  const factory CcEvent.drawCompanyOnIndex(int index) = _DrawCompanyOnIndex;
  const factory CcEvent.removeCompany(int index) = _RemoveCompany;
  const factory CcEvent.reshuffleDiscartedCompanies() = _ReshuffleDiscartedCompanies;
  const factory CcEvent.drawAiCard() = _DrawAiCard;
  const factory CcEvent.checkAiCard() = _CheckAiCard;
  const factory CcEvent.reshuffleDiscartedAiCards() = _ReshuffleDiscartedAiCards;
  const factory CcEvent.takeLoan() = _TakeLoan;
  const factory CcEvent.payOffLoan() = _PayOffLoan;
}
