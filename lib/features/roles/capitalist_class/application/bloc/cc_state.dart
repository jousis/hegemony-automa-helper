part of 'cc_bloc.dart';

@freezed
class CcState with _$CcState {
  const factory CcState({
    @Default(null) String? showMessage,
    @Default(false) bool loading,
    required List<String> reminders,
    required int corporateTax,
    required CcPageVm ccPageVm,
    required List<InstructionCardVm> instructionCards,
    required CcPlayerBoardVm ccPlayerBoardVm,
    required CcEndOfGameScoringVm endOfGameScoring,
  }) = _CcState;

  factory CcState.initial() => CcState(
        showMessage: null,
        loading: true,
        reminders: [],
        instructionCards: [],
        corporateTax: 0,
        ccPageVm: CcPageVm.defaults(),
        ccPlayerBoardVm: CcPlayerBoardVm.defaults(
          [],
        ),
        endOfGameScoring: CcEndOfGameScoringVm.defaults(),
      );
}
