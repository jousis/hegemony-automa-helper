// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:core';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:dartz/dartz.dart';
import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';
import 'package:hegemony_automa/features/ai_card/domain/i_ai_card_repository.dart';
import 'package:hegemony_automa/features/company_card/domain/company_card.dart';
import 'package:hegemony_automa/features/company_card/domain/i_company_card_repository.dart';
import 'package:hegemony_automa/features/game_board/domain/i_game_board_facade.dart';
import 'package:hegemony_automa/features/game_board/domain/policy.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/cc_game_board.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/i_cc_facade.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';
import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

const _lhePrices = [
  GoodOrServicePrice(level: PriceLevel.low, price: 5),
  GoodOrServicePrice(level: PriceLevel.mid, price: 8),
  GoodOrServicePrice(level: PriceLevel.high, price: 10),
];

@LazySingleton(as: ICcFacade)
class CCFacade implements ICcFacade {
  CCFacade(
    this._gameBoardFacade,
    this._companyCardRepository,
    this._aiCardRepository,
  );

  final Map<PolicyStatus, List<int>> _corporateTaxTable = {
    PolicyStatus.A: [0, 1, 5, 12, 24, 40, 100, 160],
    PolicyStatus.B: [0, 2, 5, 10, 15, 30, 70, 120],
    PolicyStatus.C: [0, 2, 4, 7, 10, 20, 40, 60],
  };

  int _revenueBracket() {
    if (gameBoard.revenue < 5) return 0;
    if (gameBoard.revenue < 10) return 1;
    if (gameBoard.revenue < 25) return 2;
    if (gameBoard.revenue < 50) return 3;
    if (gameBoard.revenue < 100) return 4;
    if (gameBoard.revenue < 200) return 5;
    if (gameBoard.revenue < 300) return 6;
    return 7;
  }

  final _priceLists = [
    const GoodOrServicePricelist(
      type: GoodOrServiceType.food,
      prices: [
        GoodOrServicePrice(level: PriceLevel.low, price: 9),
        GoodOrServicePrice(level: PriceLevel.mid, price: 12),
        GoodOrServicePrice(level: PriceLevel.high, price: 15),
      ],
    ),
    const GoodOrServicePricelist(
      type: GoodOrServiceType.luxury,
      prices: _lhePrices,
    ),
    const GoodOrServicePricelist(
      type: GoodOrServiceType.healthcare,
      prices: _lhePrices,
    ),
    const GoodOrServicePricelist(
      type: GoodOrServiceType.education,
      prices: _lhePrices,
    ),
  ];

  final IGameBoardFacade _gameBoardFacade;
  final ICompanyCardRepository _companyCardRepository;
  final IAiCardRepository _aiCardRepository;

  Option<CcGameBoard> _gameBoard = none();

  final CompanyCard _dummyCompany = const CompanyCard(
    role: RoleName.capitalistClass,
    filename: '',
  );

  List<CompanyCard> _companyCardDrawPile = [];
  List<CompanyCard> _companyCardDisplay = [];
  List<CompanyCard> _companyCardDiscardPile = [];

  List<AiCard> _aiCardDrawPile = [];
  AiCard? _currentAiCard;
  List<AiCard> _aiCardDiscardPile = [];
  // final List<int> _aiDiscardPileIds = [];

  @override
  CcGameBoard get gameBoard => _gameBoard.fold(
        () {
          _gameBoard = some(defaultCcGameBoard);
          return defaultCcGameBoard;
        },
        id,
      );

  @override
  List<int> get wealthTrackMoney => [
        0,
        10,
        25,
        50,
        75,
        100,
        125,
        150,
        175,
        200,
        250,
        300,
        350,
        400,
        450,
        500,
      ];

  @override
  void setCapital(int amount) {
    _gameBoard = some(gameBoard.copyWith(capital: amount));
  }

  @override
  void setRevenue(int amount) {
    _gameBoard = some(gameBoard.copyWith(revenue: amount));
  }

  @override
  int get corporateTax {
    final tp = _gameBoardFacade.board.policies
        .firstWhereOrNull(
          (p) => p.id == PolicyId.taxation,
        )
        ?.status;
    if (tp == null) return -1;
    final taxAmountForPolicyOrNull = _corporateTaxTable[tp];
    if (taxAmountForPolicyOrNull == null) {
      return -1;
    }
    return taxAmountForPolicyOrNull[_revenueBracket()];
  }

  @override
  void moveRevenueToCapital() {
    setCapital(gameBoard.revenue + gameBoard.capital);
    _gameBoard = some(gameBoard.copyWith(revenue: 0));
  }

  @override
  void reshuffleDiscartedAiCards() {
    _aiCardDrawPile = _aiCardRepository.ccAiCards..shuffle();
    _aiCardDiscardPile = [];
  }

  @override
  void drawAiCard() {
    final currentAiCard = _currentAiCard;
    if (currentAiCard != null) {
      _aiCardDiscardPile.insert(0, currentAiCard);
    }

    if (_aiCardDrawPile.isNotEmpty) {
      _currentAiCard = _aiCardDrawPile.first;
      _aiCardDrawPile.removeAt(0);
    } else {
      _currentAiCard = null;
    }

    _gameBoard = some(
      gameBoard.copyWith(
        currentAiCard: _currentAiCard,
        discartedAiCards: _aiCardDiscardPile,
      ),
    );
  }

  @override
  AiCard? checkAiCard() {
    if (_aiCardDrawPile.isEmpty) {
      return null;
    }

    final cardToShow = _aiCardDrawPile.first;
    _aiCardDrawPile.removeAt(0);
    _aiCardDrawPile.add(cardToShow);
    return cardToShow;
  }

  @override
  void reshuffleDiscartedCompanies() {
    _companyCardDrawPile = _companyCardRepository.ccCompanyCards..shuffle();
    _companyCardDiscardPile = [];
  }

  @override
  void newCompanyDisplay(int companies) {
    for (var cc in _companyCardDisplay) {
      if (cc.filename.isNotEmpty) {
        _companyCardDiscardPile.insert(0, cc);
      }
    }

    _companyCardDisplay.clear();
    final drawThisMany = min(companies, _companyCardDrawPile.length);
    for (var i = 0; i < drawThisMany; i++) {
      _companyCardDisplay.add(_companyCardDrawPile.first);
      _companyCardDrawPile.removeAt(0);
    }

    _gameBoard = some(
      gameBoard.copyWith(
        companyDisplay: _companyCardDisplay,
      ),
    );
  }

  @override
  void drawCompanyOnIndex(int index) {
    if (_companyCardDrawPile.isEmpty) {
      return;
    }
    _companyCardDisplay = _companyCardDisplay.mapIndexed(
      (i, cc) {
        if (i == index) {
          final newCompany = _companyCardDrawPile.first;
          _companyCardDrawPile.removeAt(0);
          return newCompany;
        }
        return cc;
      },
    ).toList();

    _gameBoard = some(
      gameBoard.copyWith(
        companyDisplay: _companyCardDisplay,
      ),
    );
  }

  @override
  void removeCompany(int index) {
    _companyCardDisplay = _companyCardDisplay.mapIndexed(
      (i, c) {
        if (i == index) {
          _companyCardDiscardPile.insert(0, c);
          return _dummyCompany;
        }
        return c;
      },
    ).toList();
    _gameBoard = some(
      gameBoard.copyWith(
        companyDisplay: _companyCardDisplay,
      ),
    );
  }

  @override
  set operationalCompanies(int number) {
    _gameBoard = some(
      gameBoard.copyWith(operationalCompanies: number),
    );
  }

  @override
  set wealthPosition(int position) {
    _gameBoard = some(
      gameBoard.copyWith(wealthTrackPosition: position),
    );
  }

  CcGameBoard get defaultCcGameBoard {
    final goods = CcGameBoard.defaults().goodsOrServices.map(
      (g) {
        switch (g.type) {
          case GoodOrServiceType.food:
            return g.copyWith(quantity: 1);
          case GoodOrServiceType.luxury:
          case GoodOrServiceType.education:
            return g.copyWith(quantity: 2);
          default:
            return g;
        }
      },
    ).toList();
    return CcGameBoard.defaults().copyWith(
      goodsOrServices: goods,
      influence: 1,
    );
  }

  @override
  void reset() {
    reshuffleDiscartedAiCards();
    reshuffleDiscartedCompanies();
    _companyCardDisplay = <CompanyCard>[];
    _gameBoard = some(defaultCcGameBoard);
  }

  @override
  void takeLoan() {
    setCapital(gameBoard.capital + 50);
    _gameBoard = some(
      gameBoard.copyWith(
        activeLoans: gameBoard.activeLoans + 1,
      ),
    );
  }

  @override
  void payOffLoan() {
    if (gameBoard.activeLoans <= 0) return;
    int remainingCapital = gameBoard.capital - 50;
    int remainingRevenue = gameBoard.revenue;
    if (remainingCapital > 0) {
      //enough capital to repay without touching revenue
      setCapital(remainingCapital);
      _gameBoard = some(
        gameBoard.copyWith(
          activeLoans: gameBoard.activeLoans - 1,
        ),
      );
      return;
    }
    // remainingCapital < 0 , will have to take money from revenue
    remainingRevenue += remainingCapital;
    if (remainingRevenue < 0) {
      //not possible to pay, not enough money in both buckets
      return;
    }
    remainingCapital = 0; //might be <0
    //can repay a loan but will need to take some money from revenue
    setRevenue(remainingRevenue);
    setCapital(remainingCapital);
    _gameBoard = some(
      gameBoard.copyWith(
        activeLoans: gameBoard.activeLoans - 1,
      ),
    );
  }

  @override
  void setPriceLevel({
    required GoodOrServiceType type,
    required PriceLevel level,
  }) {
    final updatedGoodsOrServices = gameBoard.goodsOrServices.map(
      (g) {
        if (g.type == type) {
          g = g.copyWith(priceLevel: level);
        }
        return g;
      },
    ).toList();
    _gameBoard = some(
      gameBoard.copyWith(
        goodsOrServices: updatedGoodsOrServices,
      ),
    );
  }

  @override
  void setQuantity({
    required GoodOrServiceType type,
    int? main,
    int? extraStorage,
    int? freeTradeZone,
  }) {
    final updatedGoodsOrServices = gameBoard.goodsOrServices.map(
      (g) {
        if (g.type == type) {
          if (main != null) {
            g = g.copyWith(quantity: main);
          }
          if (extraStorage != null) {
            g = g.copyWith(extraQuantity: some(extraStorage));
          }
        }
        return g;
      },
    ).toList();

    _gameBoard = some(
      gameBoard.copyWith(
        goodsOrServices: updatedGoodsOrServices,
      ),
    );
    if (freeTradeZone != null) {
      if (type == GoodOrServiceType.food) {
        final ftz = gameBoard.freeTradeZone.copyWith(food: freeTradeZone);
        _gameBoard = some(gameBoard.copyWith(freeTradeZone: ftz));
      }
      if (type == GoodOrServiceType.luxury) {
        final ftz = gameBoard.freeTradeZone.copyWith(luxury: freeTradeZone);
        _gameBoard = some(gameBoard.copyWith(freeTradeZone: ftz));
      }
    }
  }

  @override
  set influence(int quantity) {
    _gameBoard = some(gameBoard.copyWith(influence: quantity));
  }

  @override
  GoodOrServicePricelist? priceList(GoodOrServiceType type) => _priceLists.firstWhereOrNull(
        (p) => p.type == type,
      );

  @override
  int price(GoodOrServiceType type) {
    final pricelistOrNull = priceList(type);
    if (pricelistOrNull == null) {
      return 0;
    }
    final priceLevel = gameBoard.goodsOrServices
        .firstWhereOrNull(
          (g) => g.type == type,
        )
        ?.priceLevel;
    if (priceLevel == null) {
      return 0;
    }
    return pricelistOrNull.prices
            .firstWhereOrNull(
              (p) => p.level == priceLevel,
            )
            ?.price ??
        0;
  }

  @override
  void setPrice(GoodOrServiceType type, int price) {
    final pricelistOrNull = priceList(type);
    if (pricelistOrNull == null) {
      return;
    }
    final level = pricelistOrNull.prices
            .firstWhereOrNull(
              (p) => p.price == price,
            )
            ?.level ??
        PriceLevel.mid;
    setPriceLevel(type: type, level: level);
  }

  @override
  bool hasExtraStorage(GoodOrServiceType type) =>
      gameBoard.goodsOrServices
          .firstWhereOrNull(
            (g) => g.type == type,
          )
          ?.extraQuantity
          .isSome() ??
      false;
}
