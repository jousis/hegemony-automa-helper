import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';
import 'package:hegemony_automa/features/company_card/domain/company_card.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';

part 'cc_game_board.freezed.dart';

@freezed
class CcGoodOrService with _$CcGoodOrService {
  const CcGoodOrService._();
  const factory CcGoodOrService({
    required GoodOrServiceType type,
    required int quantity,
    required Option<int> extraQuantity,
    required PriceLevel priceLevel,
  }) = _CcGoodOrService;

  factory CcGoodOrService.defaults(
    GoodOrServiceType type,
  ) =>
      CcGoodOrService(
        type: type,
        quantity: 0,
        extraQuantity: none(),
        priceLevel: PriceLevel.mid,
      );
}

@freezed
class CcGameBoard with _$CcGameBoard {
  const CcGameBoard._();
  const factory CcGameBoard({
    required int capital,
    required int revenue,
    required int activeLoans,
    required int wealthTrackPosition,
    required List<CcGoodOrService> goodsOrServices,
    required int influence,
    required FreeTradeZone freeTradeZone,
    required int operationalCompanies,
    required List<CompanyCard> companyDisplay,
    required List<CompanyCard> discartedCompanies,
    required AiCard? currentAiCard,
    required List<AiCard> discartedAiCards,
  }) = _CcGameBoard;

  factory CcGameBoard.defaults() => CcGameBoard(
        capital: 0,
        revenue: 120,
        activeLoans: 0,
        wealthTrackPosition: 0,
        goodsOrServices: [
          CcGoodOrService.defaults(GoodOrServiceType.food),
          CcGoodOrService.defaults(GoodOrServiceType.luxury),
          CcGoodOrService.defaults(GoodOrServiceType.healthcare),
          CcGoodOrService.defaults(GoodOrServiceType.education),
        ],
        influence: 1,
        freeTradeZone: const FreeTradeZone(food: 0, luxury: 0),
        operationalCompanies: 0,
        companyDisplay: [],
        discartedCompanies: [],
        currentAiCard: null,
        discartedAiCards: [],
      );
}

@freezed
class FreeTradeZone with _$FreeTradeZone {
  const factory FreeTradeZone({
    required int food,
    required int luxury,
  }) = _FreeTradeZone;
}
