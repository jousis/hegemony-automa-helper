// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cc_game_board.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CcGoodOrService {
  GoodOrServiceType get type => throw _privateConstructorUsedError;
  int get quantity => throw _privateConstructorUsedError;
  Option<int> get extraQuantity => throw _privateConstructorUsedError;
  PriceLevel get priceLevel => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcGoodOrServiceCopyWith<CcGoodOrService> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcGoodOrServiceCopyWith<$Res> {
  factory $CcGoodOrServiceCopyWith(
          CcGoodOrService value, $Res Function(CcGoodOrService) then) =
      _$CcGoodOrServiceCopyWithImpl<$Res, CcGoodOrService>;
  @useResult
  $Res call(
      {GoodOrServiceType type,
      int quantity,
      Option<int> extraQuantity,
      PriceLevel priceLevel});
}

/// @nodoc
class _$CcGoodOrServiceCopyWithImpl<$Res, $Val extends CcGoodOrService>
    implements $CcGoodOrServiceCopyWith<$Res> {
  _$CcGoodOrServiceCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? quantity = null,
    Object? extraQuantity = null,
    Object? priceLevel = null,
  }) {
    return _then(_value.copyWith(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      extraQuantity: null == extraQuantity
          ? _value.extraQuantity
          : extraQuantity // ignore: cast_nullable_to_non_nullable
              as Option<int>,
      priceLevel: null == priceLevel
          ? _value.priceLevel
          : priceLevel // ignore: cast_nullable_to_non_nullable
              as PriceLevel,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CcGoodOrServiceCopyWith<$Res>
    implements $CcGoodOrServiceCopyWith<$Res> {
  factory _$$_CcGoodOrServiceCopyWith(
          _$_CcGoodOrService value, $Res Function(_$_CcGoodOrService) then) =
      __$$_CcGoodOrServiceCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {GoodOrServiceType type,
      int quantity,
      Option<int> extraQuantity,
      PriceLevel priceLevel});
}

/// @nodoc
class __$$_CcGoodOrServiceCopyWithImpl<$Res>
    extends _$CcGoodOrServiceCopyWithImpl<$Res, _$_CcGoodOrService>
    implements _$$_CcGoodOrServiceCopyWith<$Res> {
  __$$_CcGoodOrServiceCopyWithImpl(
      _$_CcGoodOrService _value, $Res Function(_$_CcGoodOrService) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? quantity = null,
    Object? extraQuantity = null,
    Object? priceLevel = null,
  }) {
    return _then(_$_CcGoodOrService(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
      quantity: null == quantity
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      extraQuantity: null == extraQuantity
          ? _value.extraQuantity
          : extraQuantity // ignore: cast_nullable_to_non_nullable
              as Option<int>,
      priceLevel: null == priceLevel
          ? _value.priceLevel
          : priceLevel // ignore: cast_nullable_to_non_nullable
              as PriceLevel,
    ));
  }
}

/// @nodoc

class _$_CcGoodOrService extends _CcGoodOrService {
  const _$_CcGoodOrService(
      {required this.type,
      required this.quantity,
      required this.extraQuantity,
      required this.priceLevel})
      : super._();

  @override
  final GoodOrServiceType type;
  @override
  final int quantity;
  @override
  final Option<int> extraQuantity;
  @override
  final PriceLevel priceLevel;

  @override
  String toString() {
    return 'CcGoodOrService(type: $type, quantity: $quantity, extraQuantity: $extraQuantity, priceLevel: $priceLevel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcGoodOrService &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.quantity, quantity) ||
                other.quantity == quantity) &&
            (identical(other.extraQuantity, extraQuantity) ||
                other.extraQuantity == extraQuantity) &&
            (identical(other.priceLevel, priceLevel) ||
                other.priceLevel == priceLevel));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, type, quantity, extraQuantity, priceLevel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcGoodOrServiceCopyWith<_$_CcGoodOrService> get copyWith =>
      __$$_CcGoodOrServiceCopyWithImpl<_$_CcGoodOrService>(this, _$identity);
}

abstract class _CcGoodOrService extends CcGoodOrService {
  const factory _CcGoodOrService(
      {required final GoodOrServiceType type,
      required final int quantity,
      required final Option<int> extraQuantity,
      required final PriceLevel priceLevel}) = _$_CcGoodOrService;
  const _CcGoodOrService._() : super._();

  @override
  GoodOrServiceType get type;
  @override
  int get quantity;
  @override
  Option<int> get extraQuantity;
  @override
  PriceLevel get priceLevel;
  @override
  @JsonKey(ignore: true)
  _$$_CcGoodOrServiceCopyWith<_$_CcGoodOrService> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CcGameBoard {
  int get capital => throw _privateConstructorUsedError;
  int get revenue => throw _privateConstructorUsedError;
  int get activeLoans => throw _privateConstructorUsedError;
  int get wealthTrackPosition => throw _privateConstructorUsedError;
  List<CcGoodOrService> get goodsOrServices =>
      throw _privateConstructorUsedError;
  int get influence => throw _privateConstructorUsedError;
  FreeTradeZone get freeTradeZone => throw _privateConstructorUsedError;
  int get operationalCompanies => throw _privateConstructorUsedError;
  List<CompanyCard> get companyDisplay => throw _privateConstructorUsedError;
  List<CompanyCard> get discartedCompanies =>
      throw _privateConstructorUsedError;
  AiCard? get currentAiCard => throw _privateConstructorUsedError;
  List<AiCard> get discartedAiCards => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcGameBoardCopyWith<CcGameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcGameBoardCopyWith<$Res> {
  factory $CcGameBoardCopyWith(
          CcGameBoard value, $Res Function(CcGameBoard) then) =
      _$CcGameBoardCopyWithImpl<$Res, CcGameBoard>;
  @useResult
  $Res call(
      {int capital,
      int revenue,
      int activeLoans,
      int wealthTrackPosition,
      List<CcGoodOrService> goodsOrServices,
      int influence,
      FreeTradeZone freeTradeZone,
      int operationalCompanies,
      List<CompanyCard> companyDisplay,
      List<CompanyCard> discartedCompanies,
      AiCard? currentAiCard,
      List<AiCard> discartedAiCards});

  $FreeTradeZoneCopyWith<$Res> get freeTradeZone;
  $AiCardCopyWith<$Res>? get currentAiCard;
}

/// @nodoc
class _$CcGameBoardCopyWithImpl<$Res, $Val extends CcGameBoard>
    implements $CcGameBoardCopyWith<$Res> {
  _$CcGameBoardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? capital = null,
    Object? revenue = null,
    Object? activeLoans = null,
    Object? wealthTrackPosition = null,
    Object? goodsOrServices = null,
    Object? influence = null,
    Object? freeTradeZone = null,
    Object? operationalCompanies = null,
    Object? companyDisplay = null,
    Object? discartedCompanies = null,
    Object? currentAiCard = freezed,
    Object? discartedAiCards = null,
  }) {
    return _then(_value.copyWith(
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as int,
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as int,
      activeLoans: null == activeLoans
          ? _value.activeLoans
          : activeLoans // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackPosition: null == wealthTrackPosition
          ? _value.wealthTrackPosition
          : wealthTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      goodsOrServices: null == goodsOrServices
          ? _value.goodsOrServices
          : goodsOrServices // ignore: cast_nullable_to_non_nullable
              as List<CcGoodOrService>,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZone: null == freeTradeZone
          ? _value.freeTradeZone
          : freeTradeZone // ignore: cast_nullable_to_non_nullable
              as FreeTradeZone,
      operationalCompanies: null == operationalCompanies
          ? _value.operationalCompanies
          : operationalCompanies // ignore: cast_nullable_to_non_nullable
              as int,
      companyDisplay: null == companyDisplay
          ? _value.companyDisplay
          : companyDisplay // ignore: cast_nullable_to_non_nullable
              as List<CompanyCard>,
      discartedCompanies: null == discartedCompanies
          ? _value.discartedCompanies
          : discartedCompanies // ignore: cast_nullable_to_non_nullable
              as List<CompanyCard>,
      currentAiCard: freezed == currentAiCard
          ? _value.currentAiCard
          : currentAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
      discartedAiCards: null == discartedAiCards
          ? _value.discartedAiCards
          : discartedAiCards // ignore: cast_nullable_to_non_nullable
              as List<AiCard>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $FreeTradeZoneCopyWith<$Res> get freeTradeZone {
    return $FreeTradeZoneCopyWith<$Res>(_value.freeTradeZone, (value) {
      return _then(_value.copyWith(freeTradeZone: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $AiCardCopyWith<$Res>? get currentAiCard {
    if (_value.currentAiCard == null) {
      return null;
    }

    return $AiCardCopyWith<$Res>(_value.currentAiCard!, (value) {
      return _then(_value.copyWith(currentAiCard: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_CcGameBoardCopyWith<$Res>
    implements $CcGameBoardCopyWith<$Res> {
  factory _$$_CcGameBoardCopyWith(
          _$_CcGameBoard value, $Res Function(_$_CcGameBoard) then) =
      __$$_CcGameBoardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int capital,
      int revenue,
      int activeLoans,
      int wealthTrackPosition,
      List<CcGoodOrService> goodsOrServices,
      int influence,
      FreeTradeZone freeTradeZone,
      int operationalCompanies,
      List<CompanyCard> companyDisplay,
      List<CompanyCard> discartedCompanies,
      AiCard? currentAiCard,
      List<AiCard> discartedAiCards});

  @override
  $FreeTradeZoneCopyWith<$Res> get freeTradeZone;
  @override
  $AiCardCopyWith<$Res>? get currentAiCard;
}

/// @nodoc
class __$$_CcGameBoardCopyWithImpl<$Res>
    extends _$CcGameBoardCopyWithImpl<$Res, _$_CcGameBoard>
    implements _$$_CcGameBoardCopyWith<$Res> {
  __$$_CcGameBoardCopyWithImpl(
      _$_CcGameBoard _value, $Res Function(_$_CcGameBoard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? capital = null,
    Object? revenue = null,
    Object? activeLoans = null,
    Object? wealthTrackPosition = null,
    Object? goodsOrServices = null,
    Object? influence = null,
    Object? freeTradeZone = null,
    Object? operationalCompanies = null,
    Object? companyDisplay = null,
    Object? discartedCompanies = null,
    Object? currentAiCard = freezed,
    Object? discartedAiCards = null,
  }) {
    return _then(_$_CcGameBoard(
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as int,
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as int,
      activeLoans: null == activeLoans
          ? _value.activeLoans
          : activeLoans // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackPosition: null == wealthTrackPosition
          ? _value.wealthTrackPosition
          : wealthTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      goodsOrServices: null == goodsOrServices
          ? _value._goodsOrServices
          : goodsOrServices // ignore: cast_nullable_to_non_nullable
              as List<CcGoodOrService>,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZone: null == freeTradeZone
          ? _value.freeTradeZone
          : freeTradeZone // ignore: cast_nullable_to_non_nullable
              as FreeTradeZone,
      operationalCompanies: null == operationalCompanies
          ? _value.operationalCompanies
          : operationalCompanies // ignore: cast_nullable_to_non_nullable
              as int,
      companyDisplay: null == companyDisplay
          ? _value._companyDisplay
          : companyDisplay // ignore: cast_nullable_to_non_nullable
              as List<CompanyCard>,
      discartedCompanies: null == discartedCompanies
          ? _value._discartedCompanies
          : discartedCompanies // ignore: cast_nullable_to_non_nullable
              as List<CompanyCard>,
      currentAiCard: freezed == currentAiCard
          ? _value.currentAiCard
          : currentAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
      discartedAiCards: null == discartedAiCards
          ? _value._discartedAiCards
          : discartedAiCards // ignore: cast_nullable_to_non_nullable
              as List<AiCard>,
    ));
  }
}

/// @nodoc

class _$_CcGameBoard extends _CcGameBoard {
  const _$_CcGameBoard(
      {required this.capital,
      required this.revenue,
      required this.activeLoans,
      required this.wealthTrackPosition,
      required final List<CcGoodOrService> goodsOrServices,
      required this.influence,
      required this.freeTradeZone,
      required this.operationalCompanies,
      required final List<CompanyCard> companyDisplay,
      required final List<CompanyCard> discartedCompanies,
      required this.currentAiCard,
      required final List<AiCard> discartedAiCards})
      : _goodsOrServices = goodsOrServices,
        _companyDisplay = companyDisplay,
        _discartedCompanies = discartedCompanies,
        _discartedAiCards = discartedAiCards,
        super._();

  @override
  final int capital;
  @override
  final int revenue;
  @override
  final int activeLoans;
  @override
  final int wealthTrackPosition;
  final List<CcGoodOrService> _goodsOrServices;
  @override
  List<CcGoodOrService> get goodsOrServices {
    if (_goodsOrServices is EqualUnmodifiableListView) return _goodsOrServices;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_goodsOrServices);
  }

  @override
  final int influence;
  @override
  final FreeTradeZone freeTradeZone;
  @override
  final int operationalCompanies;
  final List<CompanyCard> _companyDisplay;
  @override
  List<CompanyCard> get companyDisplay {
    if (_companyDisplay is EqualUnmodifiableListView) return _companyDisplay;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_companyDisplay);
  }

  final List<CompanyCard> _discartedCompanies;
  @override
  List<CompanyCard> get discartedCompanies {
    if (_discartedCompanies is EqualUnmodifiableListView)
      return _discartedCompanies;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_discartedCompanies);
  }

  @override
  final AiCard? currentAiCard;
  final List<AiCard> _discartedAiCards;
  @override
  List<AiCard> get discartedAiCards {
    if (_discartedAiCards is EqualUnmodifiableListView)
      return _discartedAiCards;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_discartedAiCards);
  }

  @override
  String toString() {
    return 'CcGameBoard(capital: $capital, revenue: $revenue, activeLoans: $activeLoans, wealthTrackPosition: $wealthTrackPosition, goodsOrServices: $goodsOrServices, influence: $influence, freeTradeZone: $freeTradeZone, operationalCompanies: $operationalCompanies, companyDisplay: $companyDisplay, discartedCompanies: $discartedCompanies, currentAiCard: $currentAiCard, discartedAiCards: $discartedAiCards)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcGameBoard &&
            (identical(other.capital, capital) || other.capital == capital) &&
            (identical(other.revenue, revenue) || other.revenue == revenue) &&
            (identical(other.activeLoans, activeLoans) ||
                other.activeLoans == activeLoans) &&
            (identical(other.wealthTrackPosition, wealthTrackPosition) ||
                other.wealthTrackPosition == wealthTrackPosition) &&
            const DeepCollectionEquality()
                .equals(other._goodsOrServices, _goodsOrServices) &&
            (identical(other.influence, influence) ||
                other.influence == influence) &&
            (identical(other.freeTradeZone, freeTradeZone) ||
                other.freeTradeZone == freeTradeZone) &&
            (identical(other.operationalCompanies, operationalCompanies) ||
                other.operationalCompanies == operationalCompanies) &&
            const DeepCollectionEquality()
                .equals(other._companyDisplay, _companyDisplay) &&
            const DeepCollectionEquality()
                .equals(other._discartedCompanies, _discartedCompanies) &&
            (identical(other.currentAiCard, currentAiCard) ||
                other.currentAiCard == currentAiCard) &&
            const DeepCollectionEquality()
                .equals(other._discartedAiCards, _discartedAiCards));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      capital,
      revenue,
      activeLoans,
      wealthTrackPosition,
      const DeepCollectionEquality().hash(_goodsOrServices),
      influence,
      freeTradeZone,
      operationalCompanies,
      const DeepCollectionEquality().hash(_companyDisplay),
      const DeepCollectionEquality().hash(_discartedCompanies),
      currentAiCard,
      const DeepCollectionEquality().hash(_discartedAiCards));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcGameBoardCopyWith<_$_CcGameBoard> get copyWith =>
      __$$_CcGameBoardCopyWithImpl<_$_CcGameBoard>(this, _$identity);
}

abstract class _CcGameBoard extends CcGameBoard {
  const factory _CcGameBoard(
      {required final int capital,
      required final int revenue,
      required final int activeLoans,
      required final int wealthTrackPosition,
      required final List<CcGoodOrService> goodsOrServices,
      required final int influence,
      required final FreeTradeZone freeTradeZone,
      required final int operationalCompanies,
      required final List<CompanyCard> companyDisplay,
      required final List<CompanyCard> discartedCompanies,
      required final AiCard? currentAiCard,
      required final List<AiCard> discartedAiCards}) = _$_CcGameBoard;
  const _CcGameBoard._() : super._();

  @override
  int get capital;
  @override
  int get revenue;
  @override
  int get activeLoans;
  @override
  int get wealthTrackPosition;
  @override
  List<CcGoodOrService> get goodsOrServices;
  @override
  int get influence;
  @override
  FreeTradeZone get freeTradeZone;
  @override
  int get operationalCompanies;
  @override
  List<CompanyCard> get companyDisplay;
  @override
  List<CompanyCard> get discartedCompanies;
  @override
  AiCard? get currentAiCard;
  @override
  List<AiCard> get discartedAiCards;
  @override
  @JsonKey(ignore: true)
  _$$_CcGameBoardCopyWith<_$_CcGameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FreeTradeZone {
  int get food => throw _privateConstructorUsedError;
  int get luxury => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FreeTradeZoneCopyWith<FreeTradeZone> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FreeTradeZoneCopyWith<$Res> {
  factory $FreeTradeZoneCopyWith(
          FreeTradeZone value, $Res Function(FreeTradeZone) then) =
      _$FreeTradeZoneCopyWithImpl<$Res, FreeTradeZone>;
  @useResult
  $Res call({int food, int luxury});
}

/// @nodoc
class _$FreeTradeZoneCopyWithImpl<$Res, $Val extends FreeTradeZone>
    implements $FreeTradeZoneCopyWith<$Res> {
  _$FreeTradeZoneCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? food = null,
    Object? luxury = null,
  }) {
    return _then(_value.copyWith(
      food: null == food
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as int,
      luxury: null == luxury
          ? _value.luxury
          : luxury // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FreeTradeZoneCopyWith<$Res>
    implements $FreeTradeZoneCopyWith<$Res> {
  factory _$$_FreeTradeZoneCopyWith(
          _$_FreeTradeZone value, $Res Function(_$_FreeTradeZone) then) =
      __$$_FreeTradeZoneCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int food, int luxury});
}

/// @nodoc
class __$$_FreeTradeZoneCopyWithImpl<$Res>
    extends _$FreeTradeZoneCopyWithImpl<$Res, _$_FreeTradeZone>
    implements _$$_FreeTradeZoneCopyWith<$Res> {
  __$$_FreeTradeZoneCopyWithImpl(
      _$_FreeTradeZone _value, $Res Function(_$_FreeTradeZone) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? food = null,
    Object? luxury = null,
  }) {
    return _then(_$_FreeTradeZone(
      food: null == food
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as int,
      luxury: null == luxury
          ? _value.luxury
          : luxury // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_FreeTradeZone implements _FreeTradeZone {
  const _$_FreeTradeZone({required this.food, required this.luxury});

  @override
  final int food;
  @override
  final int luxury;

  @override
  String toString() {
    return 'FreeTradeZone(food: $food, luxury: $luxury)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FreeTradeZone &&
            (identical(other.food, food) || other.food == food) &&
            (identical(other.luxury, luxury) || other.luxury == luxury));
  }

  @override
  int get hashCode => Object.hash(runtimeType, food, luxury);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FreeTradeZoneCopyWith<_$_FreeTradeZone> get copyWith =>
      __$$_FreeTradeZoneCopyWithImpl<_$_FreeTradeZone>(this, _$identity);
}

abstract class _FreeTradeZone implements FreeTradeZone {
  const factory _FreeTradeZone(
      {required final int food, required final int luxury}) = _$_FreeTradeZone;

  @override
  int get food;
  @override
  int get luxury;
  @override
  @JsonKey(ignore: true)
  _$$_FreeTradeZoneCopyWith<_$_FreeTradeZone> get copyWith =>
      throw _privateConstructorUsedError;
}
