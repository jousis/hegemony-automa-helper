import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/cc_game_board.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';

abstract class ICcFacade {
  CcGameBoard get gameBoard;

  // void setPriceLevel(GoodOrServiceType type, PriceLevel level);

  // set foodPriceLevel(PriceLevel level);
  // set luxuryPriceLevel(PriceLevel level);
  // set healthcarePriceLevel(PriceLevel level);
  // set educationPriceLevel(PriceLevel level);

  set influence(int quantity);

  void setPrice(GoodOrServiceType type, int price);
  void setPriceLevel({
    required GoodOrServiceType type,
    required PriceLevel level,
  });
  void setQuantity({
    required GoodOrServiceType type,
    int? main,
    int? extraStorage,
    int? freeTradeZone,
  });

  bool hasExtraStorage(GoodOrServiceType type);

  GoodOrServicePricelist? priceList(GoodOrServiceType type);

  int price(GoodOrServiceType type);

  void setRevenue(int amount);
  void setCapital(int amount);

  void moveRevenueToCapital();
  void reset();
  void takeLoan();

  ///WARNING: if revenue is not enough, it will subtract from capital
  void payOffLoan();

  int get corporateTax;

  List<int> get wealthTrackMoney;

  void newCompanyDisplay(int companies);
  void drawCompanyOnIndex(int index);
  void removeCompany(int index);
  void reshuffleDiscartedCompanies();

  void drawAiCard();

  ///It will draw 1 and put it at the back of the draw stack.
  AiCard? checkAiCard();

  void reshuffleDiscartedAiCards();

  set operationalCompanies(int number);
  set wealthPosition(int position);
}
