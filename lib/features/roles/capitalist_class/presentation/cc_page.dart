import 'package:another_flushbar/flushbar.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_player_board.dart';
import 'package:hegemony_automa/features/shared/presentation/ai_display_widget.dart';
import 'package:hegemony_automa/features/shared/presentation/card_popup_dialog_widget.dart';
import 'package:hegemony_automa/features/shared/presentation/company_display_widget.dart';

class CCPage extends StatelessWidget {
  const CCPage({super.key});

  final Duration timeout = const Duration(seconds: 1);

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: BlocProvider.of<CcBloc>(context),
      child: BlocConsumer<CcBloc, CcState>(
        listener: (context, state) async {
          final message = state.showMessage;
          if (message != null && context.mounted) {
            await Flushbar(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 32),
              message: message,
              duration: const Duration(seconds: 3),
            ).show(context);
          }

          if (state.ccPageVm.checkAiCardAssetPath.isNotEmpty && context.mounted) {
            showDialog(
              context: context,
              builder: (context) => Dialog(
                backgroundColor: Colors.black,
                child: CardPopupDialogWidget(
                  imagePath: state.ccPageVm.checkAiCardAssetPath,
                  allowRemoval: false,
                ),
              ),
            );
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: Colors.blue.shade900,
            appBar: AppBar(
              title: const Text(
                'Capitalist Class Automa Helper',
              ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CompanyDisplayWidget(
                      color: Colors.white,
                      companies: state.ccPageVm.companyDisplayAssetPaths,
                      drawCardOnIndex: (index) {
                        context.read<CcBloc>().add(
                              CcEvent.drawCompanyOnIndex(index),
                            );
                      },
                      onRemoveCard: (index) {
                        context.read<CcBloc>().add(
                              CcEvent.removeCompany(index),
                            );
                      },
                      onDraw: () {
                        context.read<CcBloc>().add(
                              const CcEvent.drawCompanies(4),
                            );
                      },
                      onReshuffle: () {
                        context.read<CcBloc>().add(
                              const CcEvent.reshuffleDiscartedCompanies(),
                            );
                      },
                    ),
                    Divider(
                      color: Colors.white.withAlpha(50),
                      thickness: 5,
                      height: 30,
                    ),
                    AiDisplayWidget(
                      color: Colors.white,
                      currentAiCard: state.ccPageVm.currentAiCardAssetPath,
                      discartedAiCards: state.ccPageVm.discartedAiCardsAssetPaths,
                      instructionCards: state.instructionCards,
                      onDraw: () {
                        context.read<CcBloc>().add(
                              const CcEvent.drawAiCard(),
                            );
                      },
                      onReshuffle: () {
                        context.read<CcBloc>().add(
                              const CcEvent.reshuffleDiscartedAiCards(),
                            );
                      },
                    ),
                    Divider(
                      color: Colors.white.withAlpha(50),
                      thickness: 5,
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: state.reminders
                            .map(
                              (r) => Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Icon(
                                    Icons.warning,
                                    size: 32,
                                    color: Colors.orange,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: AutoSizeText(
                                        r,
                                        maxLines: 2,
                                        minFontSize: 4,
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 32,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                      ),
                    ),
                    if (!state.loading)
                      CcPlayerBoard(
                        playerBoard: state.ccPlayerBoardVm,
                        corporateTax: state.corporateTax,
                        endOfGameScoringVm: state.endOfGameScoring,
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
