import 'package:freezed_annotation/freezed_annotation.dart';

part 'cc_end_of_game_scoring.freezed.dart';

@freezed
class CcEndOfGameScoringVm with _$CcEndOfGameScoringVm {
  const CcEndOfGameScoringVm._();
  const factory CcEndOfGameScoringVm({
    required String fromPolicies,
    required String fromFood,
    required String fromLuxury,
    required String fromHealthcare,
    required String fromEducation,
    required String fromLoans,
    required String total,
  }) = _CcEndOfGameScoringVm;

  factory CcEndOfGameScoringVm.defaults() => const CcEndOfGameScoringVm(
        fromPolicies: '',
        fromFood: '',
        fromLuxury: '',
        fromHealthcare: '',
        fromEducation: '',
        fromLoans: '',
        total: '',
      );
}
