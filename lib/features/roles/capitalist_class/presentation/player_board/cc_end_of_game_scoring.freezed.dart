// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cc_end_of_game_scoring.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CcEndOfGameScoringVm {
  String get fromPolicies => throw _privateConstructorUsedError;
  String get fromFood => throw _privateConstructorUsedError;
  String get fromLuxury => throw _privateConstructorUsedError;
  String get fromHealthcare => throw _privateConstructorUsedError;
  String get fromEducation => throw _privateConstructorUsedError;
  String get fromLoans => throw _privateConstructorUsedError;
  String get total => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcEndOfGameScoringVmCopyWith<CcEndOfGameScoringVm> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcEndOfGameScoringVmCopyWith<$Res> {
  factory $CcEndOfGameScoringVmCopyWith(CcEndOfGameScoringVm value,
          $Res Function(CcEndOfGameScoringVm) then) =
      _$CcEndOfGameScoringVmCopyWithImpl<$Res, CcEndOfGameScoringVm>;
  @useResult
  $Res call(
      {String fromPolicies,
      String fromFood,
      String fromLuxury,
      String fromHealthcare,
      String fromEducation,
      String fromLoans,
      String total});
}

/// @nodoc
class _$CcEndOfGameScoringVmCopyWithImpl<$Res,
        $Val extends CcEndOfGameScoringVm>
    implements $CcEndOfGameScoringVmCopyWith<$Res> {
  _$CcEndOfGameScoringVmCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromPolicies = null,
    Object? fromFood = null,
    Object? fromLuxury = null,
    Object? fromHealthcare = null,
    Object? fromEducation = null,
    Object? fromLoans = null,
    Object? total = null,
  }) {
    return _then(_value.copyWith(
      fromPolicies: null == fromPolicies
          ? _value.fromPolicies
          : fromPolicies // ignore: cast_nullable_to_non_nullable
              as String,
      fromFood: null == fromFood
          ? _value.fromFood
          : fromFood // ignore: cast_nullable_to_non_nullable
              as String,
      fromLuxury: null == fromLuxury
          ? _value.fromLuxury
          : fromLuxury // ignore: cast_nullable_to_non_nullable
              as String,
      fromHealthcare: null == fromHealthcare
          ? _value.fromHealthcare
          : fromHealthcare // ignore: cast_nullable_to_non_nullable
              as String,
      fromEducation: null == fromEducation
          ? _value.fromEducation
          : fromEducation // ignore: cast_nullable_to_non_nullable
              as String,
      fromLoans: null == fromLoans
          ? _value.fromLoans
          : fromLoans // ignore: cast_nullable_to_non_nullable
              as String,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CcEndOfGameScoringVmCopyWith<$Res>
    implements $CcEndOfGameScoringVmCopyWith<$Res> {
  factory _$$_CcEndOfGameScoringVmCopyWith(_$_CcEndOfGameScoringVm value,
          $Res Function(_$_CcEndOfGameScoringVm) then) =
      __$$_CcEndOfGameScoringVmCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String fromPolicies,
      String fromFood,
      String fromLuxury,
      String fromHealthcare,
      String fromEducation,
      String fromLoans,
      String total});
}

/// @nodoc
class __$$_CcEndOfGameScoringVmCopyWithImpl<$Res>
    extends _$CcEndOfGameScoringVmCopyWithImpl<$Res, _$_CcEndOfGameScoringVm>
    implements _$$_CcEndOfGameScoringVmCopyWith<$Res> {
  __$$_CcEndOfGameScoringVmCopyWithImpl(_$_CcEndOfGameScoringVm _value,
      $Res Function(_$_CcEndOfGameScoringVm) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fromPolicies = null,
    Object? fromFood = null,
    Object? fromLuxury = null,
    Object? fromHealthcare = null,
    Object? fromEducation = null,
    Object? fromLoans = null,
    Object? total = null,
  }) {
    return _then(_$_CcEndOfGameScoringVm(
      fromPolicies: null == fromPolicies
          ? _value.fromPolicies
          : fromPolicies // ignore: cast_nullable_to_non_nullable
              as String,
      fromFood: null == fromFood
          ? _value.fromFood
          : fromFood // ignore: cast_nullable_to_non_nullable
              as String,
      fromLuxury: null == fromLuxury
          ? _value.fromLuxury
          : fromLuxury // ignore: cast_nullable_to_non_nullable
              as String,
      fromHealthcare: null == fromHealthcare
          ? _value.fromHealthcare
          : fromHealthcare // ignore: cast_nullable_to_non_nullable
              as String,
      fromEducation: null == fromEducation
          ? _value.fromEducation
          : fromEducation // ignore: cast_nullable_to_non_nullable
              as String,
      fromLoans: null == fromLoans
          ? _value.fromLoans
          : fromLoans // ignore: cast_nullable_to_non_nullable
              as String,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_CcEndOfGameScoringVm extends _CcEndOfGameScoringVm {
  const _$_CcEndOfGameScoringVm(
      {required this.fromPolicies,
      required this.fromFood,
      required this.fromLuxury,
      required this.fromHealthcare,
      required this.fromEducation,
      required this.fromLoans,
      required this.total})
      : super._();

  @override
  final String fromPolicies;
  @override
  final String fromFood;
  @override
  final String fromLuxury;
  @override
  final String fromHealthcare;
  @override
  final String fromEducation;
  @override
  final String fromLoans;
  @override
  final String total;

  @override
  String toString() {
    return 'CcEndOfGameScoringVm(fromPolicies: $fromPolicies, fromFood: $fromFood, fromLuxury: $fromLuxury, fromHealthcare: $fromHealthcare, fromEducation: $fromEducation, fromLoans: $fromLoans, total: $total)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcEndOfGameScoringVm &&
            (identical(other.fromPolicies, fromPolicies) ||
                other.fromPolicies == fromPolicies) &&
            (identical(other.fromFood, fromFood) ||
                other.fromFood == fromFood) &&
            (identical(other.fromLuxury, fromLuxury) ||
                other.fromLuxury == fromLuxury) &&
            (identical(other.fromHealthcare, fromHealthcare) ||
                other.fromHealthcare == fromHealthcare) &&
            (identical(other.fromEducation, fromEducation) ||
                other.fromEducation == fromEducation) &&
            (identical(other.fromLoans, fromLoans) ||
                other.fromLoans == fromLoans) &&
            (identical(other.total, total) || other.total == total));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fromPolicies, fromFood,
      fromLuxury, fromHealthcare, fromEducation, fromLoans, total);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcEndOfGameScoringVmCopyWith<_$_CcEndOfGameScoringVm> get copyWith =>
      __$$_CcEndOfGameScoringVmCopyWithImpl<_$_CcEndOfGameScoringVm>(
          this, _$identity);
}

abstract class _CcEndOfGameScoringVm extends CcEndOfGameScoringVm {
  const factory _CcEndOfGameScoringVm(
      {required final String fromPolicies,
      required final String fromFood,
      required final String fromLuxury,
      required final String fromHealthcare,
      required final String fromEducation,
      required final String fromLoans,
      required final String total}) = _$_CcEndOfGameScoringVm;
  const _CcEndOfGameScoringVm._() : super._();

  @override
  String get fromPolicies;
  @override
  String get fromFood;
  @override
  String get fromLuxury;
  @override
  String get fromHealthcare;
  @override
  String get fromEducation;
  @override
  String get fromLoans;
  @override
  String get total;
  @override
  @JsonKey(ignore: true)
  _$$_CcEndOfGameScoringVmCopyWith<_$_CcEndOfGameScoringVm> get copyWith =>
      throw _privateConstructorUsedError;
}
