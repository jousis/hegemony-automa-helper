import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_player_board_vm.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';
import 'package:hegemony_automa/features/shared/presentation/main_storage_tile.dart';

class CcMainStorages extends StatelessWidget {
  const CcMainStorages({
    required this.playerBoard,
    super.key,
  });

  final CcPlayerBoardVm playerBoard;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        MainStorageTile(
          storageType: 'Food',
          color: Colors.green,
          quantity: playerBoard.food,
          extraQuantity: playerBoard.foodStorageTile ? playerBoard.extraFood : null,
          maxQuantity: 8,
          maxExtraQuantity: 8,
          price: playerBoard.foodPrice,
          prices: const [9, 12, 15],
          onChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setFood(value),
              ),
          extraQuantityOnChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setFood(value, toExtraStorage: true),
              ),
          addStorageOnPressed: () => context.read<CcBloc>().add(
                const CcEvent.buyStorage(
                  GoodOrServiceType.food,
                ),
              ),
          onPriceTap: (value) => context.read<CcBloc>().add(
                CcEvent.setPrice(
                  type: GoodOrServiceType.food,
                  price: value,
                ),
              ),
        ),
        MainStorageTile(
          storageType: 'Luxury',
          color: Colors.blue.shade800,
          quantity: playerBoard.luxury,
          extraQuantity: playerBoard.luxuryStorageTile ? playerBoard.extraLuxury : null,
          maxQuantity: 12,
          maxExtraQuantity: 12,
          price: playerBoard.luxuryPrice,
          prices: const [5, 8, 10],
          onChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setLuxury(value),
              ),
          extraQuantityOnChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setLuxury(value, toExtraStorage: true),
              ),
          addStorageOnPressed: () => context.read<CcBloc>().add(
                const CcEvent.buyStorage(
                  GoodOrServiceType.luxury,
                ),
              ),
          onPriceTap: (value) => context.read<CcBloc>().add(
                CcEvent.setPrice(
                  type: GoodOrServiceType.luxury,
                  price: value,
                ),
              ),
        ),
        MainStorageTile(
          storageType: 'Health',
          color: const Color(0xFFC00E0E),
          quantity: playerBoard.healthcare,
          extraQuantity: playerBoard.healthcareStorageTile ? playerBoard.extraHealthcare : null,
          maxQuantity: 12,
          maxExtraQuantity: 12,
          price: playerBoard.healthcarePrice,
          prices: const [5, 8, 10],
          onChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setHealthcare(value),
              ),
          extraQuantityOnChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setHealthcare(value, toExtraStorage: true),
              ),
          addStorageOnPressed: () => context.read<CcBloc>().add(
                const CcEvent.buyStorage(
                  GoodOrServiceType.healthcare,
                ),
              ),
          onPriceTap: (value) => context.read<CcBloc>().add(
                CcEvent.setPrice(
                  type: GoodOrServiceType.healthcare,
                  price: value,
                ),
              ),
        ),
        MainStorageTile(
          storageType: 'Education',
          color: const Color(0xFFEA8D01),
          quantity: playerBoard.education,
          extraQuantity: playerBoard.educationStorageTile ? playerBoard.extraEducation : null,
          maxQuantity: 12,
          maxExtraQuantity: 12,
          price: playerBoard.educationPrice,
          prices: const [5, 8, 10],
          onChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setEducation(value),
              ),
          extraQuantityOnChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setEducation(value, toExtraStorage: true),
              ),
          addStorageOnPressed: () => context.read<CcBloc>().add(
                const CcEvent.buyStorage(
                  GoodOrServiceType.education,
                ),
              ),
          onPriceTap: (value) => context.read<CcBloc>().add(
                CcEvent.setPrice(
                  type: GoodOrServiceType.education,
                  price: value,
                ),
              ),
        ),
        MainStorageTile(
          storageType: 'Influence',
          color: Colors.purple,
          quantity: playerBoard.influence,
          extraQuantity: null,
          maxQuantity: 100,
          maxExtraQuantity: 0,
          price: 0,
          prices: const [],
          onChanged: (value) => context.read<CcBloc>().add(
                CcEvent.setInfluence(value),
              ),
          extraQuantityOnChanged: (_) => () {},
          onPriceTap: (_) => () {},
          addStorageOnPressed: null,
        ),
      ],
    );
  }
}
