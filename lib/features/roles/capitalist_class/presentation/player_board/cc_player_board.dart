import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_end_of_game_scoring.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_main_storages.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_player_board_vm.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_revenue_widget.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/wealth_track.dart';
import 'package:hegemony_automa/features/shared/presentation/free_trade_zone_tile.dart';

class CcPlayerBoard extends StatelessWidget {
  const CcPlayerBoard({
    required this.playerBoard,
    required this.endOfGameScoringVm,
    required this.corporateTax,
    super.key,
  });

  final CcPlayerBoardVm playerBoard;
  final CcEndOfGameScoringVm endOfGameScoringVm;
  final int corporateTax;

  final TextStyle titleTextStyle = const TextStyle(color: Colors.white);
  final TextStyle contentTextStyle = const TextStyle(
    color: Colors.yellow,
    fontSize: 46,
    fontWeight: FontWeight.bold,
  );
  final TextStyle bgTextStyle = const TextStyle(color: Colors.lightBlue);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.black,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CcRevenueWidget(
            playerBoard: playerBoard,
            titleTextStyle: titleTextStyle,
            contentTextStyle: contentTextStyle,
          ),
          Text(
            'Corporate Tax: $corporateTax',
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
          WealthTrack(
            wealthList: playerBoard.wealthTrackMoney,
            position: playerBoard.wealthTrackPosition,
          ),
          CcMainStorages(
            playerBoard: playerBoard,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FreeTradeZoneTile(
                foodQuantity: playerBoard.freeTradeZoneFood,
                maxFoodQuantity: 8,
                luxuryQuantity: playerBoard.freeTradeZoneLuxury,
                maxLuxuryQuantity: 12,
                foodOnChanged: (value) => context.read<CcBloc>().add(
                      CcEvent.setFood(value, toFreeTradeMarket: true),
                    ),
                luxuryOnChanged: (value) => context.read<CcBloc>().add(
                      CcEvent.setLuxury(value, toFreeTradeMarket: true),
                    ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                  onPressed: () {
                    const textStyle = TextStyle(color: Colors.white, fontSize: 24);

                    showDialog(
                      context: context,
                      builder: (context) => Dialog(
                        backgroundColor: Colors.black,
                        child: FractionallySizedBox(
                          widthFactor: 0.4,
                          heightFactor: 0.4,
                          child: Padding(
                            padding: const EdgeInsets.all(32.0),
                            child: Row(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      const AutoSizeText(
                                        'Policies:',
                                        style: textStyle,
                                      ),
                                      const AutoSizeText(
                                        'Food:',
                                        style: textStyle,
                                      ),
                                      const AutoSizeText(
                                        'Luxury:',
                                        style: textStyle,
                                      ),
                                      const AutoSizeText(
                                        'Health:',
                                        style: textStyle,
                                      ),
                                      const AutoSizeText(
                                        'Education:',
                                        style: textStyle,
                                      ),
                                      if (endOfGameScoringVm.fromLoans.isNotEmpty)
                                        const AutoSizeText(
                                          'Loans:',
                                          style: textStyle,
                                        ),
                                      const SizedBox(
                                        height: 50,
                                      ),
                                      const AutoSizeText(
                                        'Total:',
                                        style: textStyle,
                                      ),
                                    ],
                                  ),
                                ),
                                Column(
                                  children: [
                                    Text(
                                      endOfGameScoringVm.fromPolicies,
                                      style: textStyle,
                                    ),
                                    Text(
                                      endOfGameScoringVm.fromFood,
                                      style: textStyle,
                                    ),
                                    Text(
                                      endOfGameScoringVm.fromLuxury,
                                      style: textStyle,
                                    ),
                                    Text(
                                      endOfGameScoringVm.fromHealthcare,
                                      style: textStyle,
                                    ),
                                    Text(
                                      endOfGameScoringVm.fromEducation,
                                      style: textStyle,
                                    ),
                                    if (endOfGameScoringVm.fromLoans.isNotEmpty)
                                      Text(
                                        endOfGameScoringVm.fromLoans,
                                        style: textStyle,
                                      ),
                                    const SizedBox(
                                      height: 50,
                                    ),
                                    Text(
                                      endOfGameScoringVm.total,
                                      style: textStyle,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  child: const AutoSizeText(
                    'End Of Game Scoring',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
