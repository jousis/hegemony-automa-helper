import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/cc_game_board.dart';

part 'cc_player_board_vm.freezed.dart';

@freezed
class CcPlayerBoardVm with _$CcPlayerBoardVm {
  const CcPlayerBoardVm._();
  const factory CcPlayerBoardVm({
    required int revenue,
    required int capital,
    required int wealthTrackPosition,
    required List<int> wealthTrackMoney,
    required int food,
    required int extraFood,
    required bool foodStorageTile,
    required int luxury,
    required bool luxuryStorageTile,
    required int extraLuxury,
    required int healthcare,
    required bool healthcareStorageTile,
    required int extraHealthcare,
    required int education,
    required bool educationStorageTile,
    required int extraEducation,
    required int influence,
    required int freeTradeZoneFood,
    required int freeTradeZoneLuxury,
    required int foodPrice,
    required int luxuryPrice,
    required int healthcarePrice,
    required int educationPrice,
    required int loans,
  }) = _CcPlayerBoardVm;

  factory CcPlayerBoardVm.fromDomain({
    required CcGameBoard gameBoard,
    required List<int> wealthTrackMoney,
    required CcGoodOrService food,
    required CcGoodOrService luxury,
    required CcGoodOrService healthcare,
    required CcGoodOrService education,
    required int foodPrice,
    required int luxuryPrice,
    required int healthcarePrice,
    required int educationPrice,
  }) =>
      CcPlayerBoardVm(
        revenue: gameBoard.revenue,
        capital: gameBoard.capital,
        wealthTrackPosition: gameBoard.wealthTrackPosition,
        wealthTrackMoney: wealthTrackMoney,
        food: food.quantity,
        foodStorageTile: food.extraQuantity.isSome(),
        extraFood: food.extraQuantity.toNullable() ?? 0,
        luxury: luxury.quantity,
        luxuryStorageTile: luxury.extraQuantity.isSome(),
        extraLuxury: luxury.extraQuantity.toNullable() ?? 0,
        healthcare: healthcare.quantity,
        healthcareStorageTile: healthcare.extraQuantity.isSome(),
        extraHealthcare: healthcare.extraQuantity.toNullable() ?? 0,
        education: education.quantity,
        educationStorageTile: education.extraQuantity.isSome(),
        extraEducation: education.extraQuantity.toNullable() ?? 0,
        influence: gameBoard.influence,
        freeTradeZoneFood: gameBoard.freeTradeZone.food,
        freeTradeZoneLuxury: gameBoard.freeTradeZone.luxury,
        foodPrice: foodPrice,
        luxuryPrice: luxuryPrice,
        healthcarePrice: healthcarePrice,
        educationPrice: educationPrice,
        loans: gameBoard.activeLoans,
      );

  factory CcPlayerBoardVm.defaults(
    List<int> wealthTrackMoney,
  ) =>
      CcPlayerBoardVm(
        revenue: 0,
        capital: 0,
        wealthTrackPosition: 0,
        wealthTrackMoney: wealthTrackMoney,
        food: 0,
        foodStorageTile: false,
        extraFood: 0,
        luxury: 0,
        luxuryStorageTile: false,
        extraLuxury: 0,
        healthcare: 0,
        healthcareStorageTile: false,
        extraHealthcare: 0,
        education: 0,
        educationStorageTile: false,
        extraEducation: 0,
        influence: 0,
        freeTradeZoneFood: 0,
        freeTradeZoneLuxury: 0,
        foodPrice: 0,
        luxuryPrice: 0,
        healthcarePrice: 0,
        educationPrice: 0,
        loans: 0,
      );
}
