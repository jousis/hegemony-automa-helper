// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cc_player_board_vm.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CcPlayerBoardVm {
  int get revenue => throw _privateConstructorUsedError;
  int get capital => throw _privateConstructorUsedError;
  int get wealthTrackPosition => throw _privateConstructorUsedError;
  List<int> get wealthTrackMoney => throw _privateConstructorUsedError;
  int get food => throw _privateConstructorUsedError;
  int get extraFood => throw _privateConstructorUsedError;
  bool get foodStorageTile => throw _privateConstructorUsedError;
  int get luxury => throw _privateConstructorUsedError;
  bool get luxuryStorageTile => throw _privateConstructorUsedError;
  int get extraLuxury => throw _privateConstructorUsedError;
  int get healthcare => throw _privateConstructorUsedError;
  bool get healthcareStorageTile => throw _privateConstructorUsedError;
  int get extraHealthcare => throw _privateConstructorUsedError;
  int get education => throw _privateConstructorUsedError;
  bool get educationStorageTile => throw _privateConstructorUsedError;
  int get extraEducation => throw _privateConstructorUsedError;
  int get influence => throw _privateConstructorUsedError;
  int get freeTradeZoneFood => throw _privateConstructorUsedError;
  int get freeTradeZoneLuxury => throw _privateConstructorUsedError;
  int get foodPrice => throw _privateConstructorUsedError;
  int get luxuryPrice => throw _privateConstructorUsedError;
  int get healthcarePrice => throw _privateConstructorUsedError;
  int get educationPrice => throw _privateConstructorUsedError;
  int get loans => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcPlayerBoardVmCopyWith<CcPlayerBoardVm> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcPlayerBoardVmCopyWith<$Res> {
  factory $CcPlayerBoardVmCopyWith(
          CcPlayerBoardVm value, $Res Function(CcPlayerBoardVm) then) =
      _$CcPlayerBoardVmCopyWithImpl<$Res, CcPlayerBoardVm>;
  @useResult
  $Res call(
      {int revenue,
      int capital,
      int wealthTrackPosition,
      List<int> wealthTrackMoney,
      int food,
      int extraFood,
      bool foodStorageTile,
      int luxury,
      bool luxuryStorageTile,
      int extraLuxury,
      int healthcare,
      bool healthcareStorageTile,
      int extraHealthcare,
      int education,
      bool educationStorageTile,
      int extraEducation,
      int influence,
      int freeTradeZoneFood,
      int freeTradeZoneLuxury,
      int foodPrice,
      int luxuryPrice,
      int healthcarePrice,
      int educationPrice,
      int loans});
}

/// @nodoc
class _$CcPlayerBoardVmCopyWithImpl<$Res, $Val extends CcPlayerBoardVm>
    implements $CcPlayerBoardVmCopyWith<$Res> {
  _$CcPlayerBoardVmCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? revenue = null,
    Object? capital = null,
    Object? wealthTrackPosition = null,
    Object? wealthTrackMoney = null,
    Object? food = null,
    Object? extraFood = null,
    Object? foodStorageTile = null,
    Object? luxury = null,
    Object? luxuryStorageTile = null,
    Object? extraLuxury = null,
    Object? healthcare = null,
    Object? healthcareStorageTile = null,
    Object? extraHealthcare = null,
    Object? education = null,
    Object? educationStorageTile = null,
    Object? extraEducation = null,
    Object? influence = null,
    Object? freeTradeZoneFood = null,
    Object? freeTradeZoneLuxury = null,
    Object? foodPrice = null,
    Object? luxuryPrice = null,
    Object? healthcarePrice = null,
    Object? educationPrice = null,
    Object? loans = null,
  }) {
    return _then(_value.copyWith(
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as int,
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackPosition: null == wealthTrackPosition
          ? _value.wealthTrackPosition
          : wealthTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackMoney: null == wealthTrackMoney
          ? _value.wealthTrackMoney
          : wealthTrackMoney // ignore: cast_nullable_to_non_nullable
              as List<int>,
      food: null == food
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as int,
      extraFood: null == extraFood
          ? _value.extraFood
          : extraFood // ignore: cast_nullable_to_non_nullable
              as int,
      foodStorageTile: null == foodStorageTile
          ? _value.foodStorageTile
          : foodStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      luxury: null == luxury
          ? _value.luxury
          : luxury // ignore: cast_nullable_to_non_nullable
              as int,
      luxuryStorageTile: null == luxuryStorageTile
          ? _value.luxuryStorageTile
          : luxuryStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraLuxury: null == extraLuxury
          ? _value.extraLuxury
          : extraLuxury // ignore: cast_nullable_to_non_nullable
              as int,
      healthcare: null == healthcare
          ? _value.healthcare
          : healthcare // ignore: cast_nullable_to_non_nullable
              as int,
      healthcareStorageTile: null == healthcareStorageTile
          ? _value.healthcareStorageTile
          : healthcareStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraHealthcare: null == extraHealthcare
          ? _value.extraHealthcare
          : extraHealthcare // ignore: cast_nullable_to_non_nullable
              as int,
      education: null == education
          ? _value.education
          : education // ignore: cast_nullable_to_non_nullable
              as int,
      educationStorageTile: null == educationStorageTile
          ? _value.educationStorageTile
          : educationStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraEducation: null == extraEducation
          ? _value.extraEducation
          : extraEducation // ignore: cast_nullable_to_non_nullable
              as int,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZoneFood: null == freeTradeZoneFood
          ? _value.freeTradeZoneFood
          : freeTradeZoneFood // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZoneLuxury: null == freeTradeZoneLuxury
          ? _value.freeTradeZoneLuxury
          : freeTradeZoneLuxury // ignore: cast_nullable_to_non_nullable
              as int,
      foodPrice: null == foodPrice
          ? _value.foodPrice
          : foodPrice // ignore: cast_nullable_to_non_nullable
              as int,
      luxuryPrice: null == luxuryPrice
          ? _value.luxuryPrice
          : luxuryPrice // ignore: cast_nullable_to_non_nullable
              as int,
      healthcarePrice: null == healthcarePrice
          ? _value.healthcarePrice
          : healthcarePrice // ignore: cast_nullable_to_non_nullable
              as int,
      educationPrice: null == educationPrice
          ? _value.educationPrice
          : educationPrice // ignore: cast_nullable_to_non_nullable
              as int,
      loans: null == loans
          ? _value.loans
          : loans // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CcPlayerBoardVmCopyWith<$Res>
    implements $CcPlayerBoardVmCopyWith<$Res> {
  factory _$$_CcPlayerBoardVmCopyWith(
          _$_CcPlayerBoardVm value, $Res Function(_$_CcPlayerBoardVm) then) =
      __$$_CcPlayerBoardVmCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int revenue,
      int capital,
      int wealthTrackPosition,
      List<int> wealthTrackMoney,
      int food,
      int extraFood,
      bool foodStorageTile,
      int luxury,
      bool luxuryStorageTile,
      int extraLuxury,
      int healthcare,
      bool healthcareStorageTile,
      int extraHealthcare,
      int education,
      bool educationStorageTile,
      int extraEducation,
      int influence,
      int freeTradeZoneFood,
      int freeTradeZoneLuxury,
      int foodPrice,
      int luxuryPrice,
      int healthcarePrice,
      int educationPrice,
      int loans});
}

/// @nodoc
class __$$_CcPlayerBoardVmCopyWithImpl<$Res>
    extends _$CcPlayerBoardVmCopyWithImpl<$Res, _$_CcPlayerBoardVm>
    implements _$$_CcPlayerBoardVmCopyWith<$Res> {
  __$$_CcPlayerBoardVmCopyWithImpl(
      _$_CcPlayerBoardVm _value, $Res Function(_$_CcPlayerBoardVm) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? revenue = null,
    Object? capital = null,
    Object? wealthTrackPosition = null,
    Object? wealthTrackMoney = null,
    Object? food = null,
    Object? extraFood = null,
    Object? foodStorageTile = null,
    Object? luxury = null,
    Object? luxuryStorageTile = null,
    Object? extraLuxury = null,
    Object? healthcare = null,
    Object? healthcareStorageTile = null,
    Object? extraHealthcare = null,
    Object? education = null,
    Object? educationStorageTile = null,
    Object? extraEducation = null,
    Object? influence = null,
    Object? freeTradeZoneFood = null,
    Object? freeTradeZoneLuxury = null,
    Object? foodPrice = null,
    Object? luxuryPrice = null,
    Object? healthcarePrice = null,
    Object? educationPrice = null,
    Object? loans = null,
  }) {
    return _then(_$_CcPlayerBoardVm(
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as int,
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackPosition: null == wealthTrackPosition
          ? _value.wealthTrackPosition
          : wealthTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      wealthTrackMoney: null == wealthTrackMoney
          ? _value._wealthTrackMoney
          : wealthTrackMoney // ignore: cast_nullable_to_non_nullable
              as List<int>,
      food: null == food
          ? _value.food
          : food // ignore: cast_nullable_to_non_nullable
              as int,
      extraFood: null == extraFood
          ? _value.extraFood
          : extraFood // ignore: cast_nullable_to_non_nullable
              as int,
      foodStorageTile: null == foodStorageTile
          ? _value.foodStorageTile
          : foodStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      luxury: null == luxury
          ? _value.luxury
          : luxury // ignore: cast_nullable_to_non_nullable
              as int,
      luxuryStorageTile: null == luxuryStorageTile
          ? _value.luxuryStorageTile
          : luxuryStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraLuxury: null == extraLuxury
          ? _value.extraLuxury
          : extraLuxury // ignore: cast_nullable_to_non_nullable
              as int,
      healthcare: null == healthcare
          ? _value.healthcare
          : healthcare // ignore: cast_nullable_to_non_nullable
              as int,
      healthcareStorageTile: null == healthcareStorageTile
          ? _value.healthcareStorageTile
          : healthcareStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraHealthcare: null == extraHealthcare
          ? _value.extraHealthcare
          : extraHealthcare // ignore: cast_nullable_to_non_nullable
              as int,
      education: null == education
          ? _value.education
          : education // ignore: cast_nullable_to_non_nullable
              as int,
      educationStorageTile: null == educationStorageTile
          ? _value.educationStorageTile
          : educationStorageTile // ignore: cast_nullable_to_non_nullable
              as bool,
      extraEducation: null == extraEducation
          ? _value.extraEducation
          : extraEducation // ignore: cast_nullable_to_non_nullable
              as int,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZoneFood: null == freeTradeZoneFood
          ? _value.freeTradeZoneFood
          : freeTradeZoneFood // ignore: cast_nullable_to_non_nullable
              as int,
      freeTradeZoneLuxury: null == freeTradeZoneLuxury
          ? _value.freeTradeZoneLuxury
          : freeTradeZoneLuxury // ignore: cast_nullable_to_non_nullable
              as int,
      foodPrice: null == foodPrice
          ? _value.foodPrice
          : foodPrice // ignore: cast_nullable_to_non_nullable
              as int,
      luxuryPrice: null == luxuryPrice
          ? _value.luxuryPrice
          : luxuryPrice // ignore: cast_nullable_to_non_nullable
              as int,
      healthcarePrice: null == healthcarePrice
          ? _value.healthcarePrice
          : healthcarePrice // ignore: cast_nullable_to_non_nullable
              as int,
      educationPrice: null == educationPrice
          ? _value.educationPrice
          : educationPrice // ignore: cast_nullable_to_non_nullable
              as int,
      loans: null == loans
          ? _value.loans
          : loans // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_CcPlayerBoardVm extends _CcPlayerBoardVm {
  const _$_CcPlayerBoardVm(
      {required this.revenue,
      required this.capital,
      required this.wealthTrackPosition,
      required final List<int> wealthTrackMoney,
      required this.food,
      required this.extraFood,
      required this.foodStorageTile,
      required this.luxury,
      required this.luxuryStorageTile,
      required this.extraLuxury,
      required this.healthcare,
      required this.healthcareStorageTile,
      required this.extraHealthcare,
      required this.education,
      required this.educationStorageTile,
      required this.extraEducation,
      required this.influence,
      required this.freeTradeZoneFood,
      required this.freeTradeZoneLuxury,
      required this.foodPrice,
      required this.luxuryPrice,
      required this.healthcarePrice,
      required this.educationPrice,
      required this.loans})
      : _wealthTrackMoney = wealthTrackMoney,
        super._();

  @override
  final int revenue;
  @override
  final int capital;
  @override
  final int wealthTrackPosition;
  final List<int> _wealthTrackMoney;
  @override
  List<int> get wealthTrackMoney {
    if (_wealthTrackMoney is EqualUnmodifiableListView)
      return _wealthTrackMoney;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_wealthTrackMoney);
  }

  @override
  final int food;
  @override
  final int extraFood;
  @override
  final bool foodStorageTile;
  @override
  final int luxury;
  @override
  final bool luxuryStorageTile;
  @override
  final int extraLuxury;
  @override
  final int healthcare;
  @override
  final bool healthcareStorageTile;
  @override
  final int extraHealthcare;
  @override
  final int education;
  @override
  final bool educationStorageTile;
  @override
  final int extraEducation;
  @override
  final int influence;
  @override
  final int freeTradeZoneFood;
  @override
  final int freeTradeZoneLuxury;
  @override
  final int foodPrice;
  @override
  final int luxuryPrice;
  @override
  final int healthcarePrice;
  @override
  final int educationPrice;
  @override
  final int loans;

  @override
  String toString() {
    return 'CcPlayerBoardVm(revenue: $revenue, capital: $capital, wealthTrackPosition: $wealthTrackPosition, wealthTrackMoney: $wealthTrackMoney, food: $food, extraFood: $extraFood, foodStorageTile: $foodStorageTile, luxury: $luxury, luxuryStorageTile: $luxuryStorageTile, extraLuxury: $extraLuxury, healthcare: $healthcare, healthcareStorageTile: $healthcareStorageTile, extraHealthcare: $extraHealthcare, education: $education, educationStorageTile: $educationStorageTile, extraEducation: $extraEducation, influence: $influence, freeTradeZoneFood: $freeTradeZoneFood, freeTradeZoneLuxury: $freeTradeZoneLuxury, foodPrice: $foodPrice, luxuryPrice: $luxuryPrice, healthcarePrice: $healthcarePrice, educationPrice: $educationPrice, loans: $loans)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcPlayerBoardVm &&
            (identical(other.revenue, revenue) || other.revenue == revenue) &&
            (identical(other.capital, capital) || other.capital == capital) &&
            (identical(other.wealthTrackPosition, wealthTrackPosition) ||
                other.wealthTrackPosition == wealthTrackPosition) &&
            const DeepCollectionEquality()
                .equals(other._wealthTrackMoney, _wealthTrackMoney) &&
            (identical(other.food, food) || other.food == food) &&
            (identical(other.extraFood, extraFood) ||
                other.extraFood == extraFood) &&
            (identical(other.foodStorageTile, foodStorageTile) ||
                other.foodStorageTile == foodStorageTile) &&
            (identical(other.luxury, luxury) || other.luxury == luxury) &&
            (identical(other.luxuryStorageTile, luxuryStorageTile) ||
                other.luxuryStorageTile == luxuryStorageTile) &&
            (identical(other.extraLuxury, extraLuxury) ||
                other.extraLuxury == extraLuxury) &&
            (identical(other.healthcare, healthcare) ||
                other.healthcare == healthcare) &&
            (identical(other.healthcareStorageTile, healthcareStorageTile) ||
                other.healthcareStorageTile == healthcareStorageTile) &&
            (identical(other.extraHealthcare, extraHealthcare) ||
                other.extraHealthcare == extraHealthcare) &&
            (identical(other.education, education) ||
                other.education == education) &&
            (identical(other.educationStorageTile, educationStorageTile) ||
                other.educationStorageTile == educationStorageTile) &&
            (identical(other.extraEducation, extraEducation) ||
                other.extraEducation == extraEducation) &&
            (identical(other.influence, influence) ||
                other.influence == influence) &&
            (identical(other.freeTradeZoneFood, freeTradeZoneFood) ||
                other.freeTradeZoneFood == freeTradeZoneFood) &&
            (identical(other.freeTradeZoneLuxury, freeTradeZoneLuxury) ||
                other.freeTradeZoneLuxury == freeTradeZoneLuxury) &&
            (identical(other.foodPrice, foodPrice) ||
                other.foodPrice == foodPrice) &&
            (identical(other.luxuryPrice, luxuryPrice) ||
                other.luxuryPrice == luxuryPrice) &&
            (identical(other.healthcarePrice, healthcarePrice) ||
                other.healthcarePrice == healthcarePrice) &&
            (identical(other.educationPrice, educationPrice) ||
                other.educationPrice == educationPrice) &&
            (identical(other.loans, loans) || other.loans == loans));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        revenue,
        capital,
        wealthTrackPosition,
        const DeepCollectionEquality().hash(_wealthTrackMoney),
        food,
        extraFood,
        foodStorageTile,
        luxury,
        luxuryStorageTile,
        extraLuxury,
        healthcare,
        healthcareStorageTile,
        extraHealthcare,
        education,
        educationStorageTile,
        extraEducation,
        influence,
        freeTradeZoneFood,
        freeTradeZoneLuxury,
        foodPrice,
        luxuryPrice,
        healthcarePrice,
        educationPrice,
        loans
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcPlayerBoardVmCopyWith<_$_CcPlayerBoardVm> get copyWith =>
      __$$_CcPlayerBoardVmCopyWithImpl<_$_CcPlayerBoardVm>(this, _$identity);
}

abstract class _CcPlayerBoardVm extends CcPlayerBoardVm {
  const factory _CcPlayerBoardVm(
      {required final int revenue,
      required final int capital,
      required final int wealthTrackPosition,
      required final List<int> wealthTrackMoney,
      required final int food,
      required final int extraFood,
      required final bool foodStorageTile,
      required final int luxury,
      required final bool luxuryStorageTile,
      required final int extraLuxury,
      required final int healthcare,
      required final bool healthcareStorageTile,
      required final int extraHealthcare,
      required final int education,
      required final bool educationStorageTile,
      required final int extraEducation,
      required final int influence,
      required final int freeTradeZoneFood,
      required final int freeTradeZoneLuxury,
      required final int foodPrice,
      required final int luxuryPrice,
      required final int healthcarePrice,
      required final int educationPrice,
      required final int loans}) = _$_CcPlayerBoardVm;
  const _CcPlayerBoardVm._() : super._();

  @override
  int get revenue;
  @override
  int get capital;
  @override
  int get wealthTrackPosition;
  @override
  List<int> get wealthTrackMoney;
  @override
  int get food;
  @override
  int get extraFood;
  @override
  bool get foodStorageTile;
  @override
  int get luxury;
  @override
  bool get luxuryStorageTile;
  @override
  int get extraLuxury;
  @override
  int get healthcare;
  @override
  bool get healthcareStorageTile;
  @override
  int get extraHealthcare;
  @override
  int get education;
  @override
  bool get educationStorageTile;
  @override
  int get extraEducation;
  @override
  int get influence;
  @override
  int get freeTradeZoneFood;
  @override
  int get freeTradeZoneLuxury;
  @override
  int get foodPrice;
  @override
  int get luxuryPrice;
  @override
  int get healthcarePrice;
  @override
  int get educationPrice;
  @override
  int get loans;
  @override
  @JsonKey(ignore: true)
  _$$_CcPlayerBoardVmCopyWith<_$_CcPlayerBoardVm> get copyWith =>
      throw _privateConstructorUsedError;
}
