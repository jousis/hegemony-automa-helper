import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/player_board/cc_player_board_vm.dart';
import 'package:hegemony_automa/features/shared/presentation/change_money_dialog.dart';
import 'package:hegemony_automa/features/shared/presentation/loans_widget.dart';

class CcRevenueWidget extends StatelessWidget {
  const CcRevenueWidget({
    super.key,
    required this.playerBoard,
    required this.titleTextStyle,
    required this.contentTextStyle,
  });

  final CcPlayerBoardVm playerBoard;
  final TextStyle titleTextStyle;
  final TextStyle contentTextStyle;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: LoansWidget(
            count: playerBoard.loans,
            take: () {
              context.read<CcBloc>().add(const CcEvent.takeLoan());
            },
            payOff: () {
              context.read<CcBloc>().add(const CcEvent.payOffLoan());
            },
          ),
        ),
        Expanded(
          child: Container(
            constraints: const BoxConstraints(minHeight: 140),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AutoSizeText(
                    'REVENUE',
                    minFontSize: 10,
                    maxLines: 1,
                    style: titleTextStyle,
                  ),
                ),
                InkWell(
                  onTap: () async {
                    showDialog<int>(
                      context: context,
                      builder: (context) => Dialog(
                        backgroundColor: Colors.black,
                        child: ChangeMoneyDialog(
                          title: 'Change Revenue',
                          currentAmount: playerBoard.revenue,
                        ),
                      ),
                    ).then(
                      (vardis) {
                        if (vardis != null) {
                          context.read<CcBloc>().add(
                                CcEvent.setRevenue(
                                  vardis,
                                ),
                              );
                        }
                      },
                    );
                  },
                  child: AutoSizeText(
                    '${playerBoard.revenue.toString()}∀',
                    minFontSize: 12,
                    maxLines: 1,
                    style: contentTextStyle,
                  ),
                )
              ],
            ),
          ),
        ),
        Center(
          child: IconButton(
            color: Colors.white,
            iconSize: 64,
            icon: const Icon(
              Icons.arrow_right_alt,
            ),
            onPressed: () {
              context.read<CcBloc>().add(const CcEvent.moveRevenueToCapital());
            },
          ),
        ),
        Expanded(
          child: Container(
            constraints: const BoxConstraints(minHeight: 140),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText(
                      minFontSize: 10,
                      maxLines: 1,
                      'CAPITAL',
                      style: titleTextStyle,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    showDialog<int>(
                      context: context,
                      builder: (context) => Dialog(
                        backgroundColor: Colors.black,
                        child: ChangeMoneyDialog(
                          title: 'Change Capital',
                          currentAmount: playerBoard.capital,
                        ),
                      ),
                    ).then(
                      (vardis) {
                        if (vardis != null) {
                          context.read<CcBloc>().add(
                                CcEvent.setCapital(
                                  vardis,
                                ),
                              );
                        }
                      },
                    );
                  },
                  child: AutoSizeText(
                    '${playerBoard.capital.toString()}∀',
                    minFontSize: 12,
                    maxLines: 1,
                    style: contentTextStyle,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
