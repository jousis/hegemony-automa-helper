import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';

class WealthTrack extends StatelessWidget {
  const WealthTrack({
    required this.position,
    required this.wealthList,
    super.key,
  });

  final int position;
  final List<int> wealthList;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue.shade400,
      child: Wrap(
        spacing: 2,
        direction: Axis.horizontal,
        children: wealthList
            .mapIndexed(
              (index, wealth) => SizedBox(
                width: 50,
                height: 75,
                child: InkWell(
                  onTap: () {
                    context.read<CcBloc>().add(
                          CcEvent.wealthTrackPositionSelected(index),
                        );
                  },
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: index == position ? Colors.blue.shade800 : null,
                    ),
                    child: SizedBox(
                      width: 50,
                      height: 75,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            '$wealth∀',
                            style: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                const Icon(
                                  Icons.star_rounded,
                                  size: 50,
                                  color: Colors.white,
                                ),
                                Text(
                                  (index).toString(),
                                  style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
