import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/domain/cc_game_board.dart';

part 'cc_page_vm.freezed.dart';

@freezed
class CcPageVm with _$CcPageVm {
  const CcPageVm._();
  const factory CcPageVm({
    required String capital,
    required String revenue,
    required String employmentTax,
    required String corporateTax,
    required int wealthPosition,
    required int operationalCompanies,
    required List<String> companyDisplayAssetPaths,
    required List<String> discartedAiCardsAssetPaths,
    required String currentAiCardAssetPath,
    required String checkAiCardAssetPath,
    required int remainingCubes,
  }) = _CcPageVm;

  factory CcPageVm.defaults() => const CcPageVm(
        capital: '',
        revenue: '',
        employmentTax: '',
        corporateTax: '',
        wealthPosition: 0,
        operationalCompanies: 0,
        companyDisplayAssetPaths: ['', '', '', ''],
        discartedAiCardsAssetPaths: [],
        currentAiCardAssetPath: '',
        checkAiCardAssetPath: '',
        remainingCubes: -1,
      );

  factory CcPageVm.fromDomain(
    CcGameBoard data,
    int remainingCubes,
  ) =>
      CcPageVm(
        capital: data.capital.toStringAsFixed(0),
        revenue: data.revenue.toStringAsFixed(0),
        employmentTax: '',
        corporateTax: '',
        operationalCompanies: 0,
        wealthPosition: data.wealthTrackPosition,
        companyDisplayAssetPaths: data.companyDisplay
            .map(
              (c) => c.filename.isEmpty ? '' : 'assets/cc/companies/${c.filename}',
            )
            .toList(),
        discartedAiCardsAssetPaths: data.discartedAiCards
            .map(
              (c) => c.filename.isEmpty ? '' : 'assets/cc/ai/${c.filename}',
            )
            .toList(),
        currentAiCardAssetPath: data.currentAiCard != null ? 'assets/cc/ai/${data.currentAiCard?.filename}' : '',
        checkAiCardAssetPath: '',
        remainingCubes: remainingCubes,
      );
}
