// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cc_page_vm.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CcPageVm {
  String get capital => throw _privateConstructorUsedError;
  String get revenue => throw _privateConstructorUsedError;
  String get employmentTax => throw _privateConstructorUsedError;
  String get corporateTax => throw _privateConstructorUsedError;
  int get wealthPosition => throw _privateConstructorUsedError;
  int get operationalCompanies => throw _privateConstructorUsedError;
  List<String> get companyDisplayAssetPaths =>
      throw _privateConstructorUsedError;
  List<String> get discartedAiCardsAssetPaths =>
      throw _privateConstructorUsedError;
  String get currentAiCardAssetPath => throw _privateConstructorUsedError;
  String get checkAiCardAssetPath => throw _privateConstructorUsedError;
  int get remainingCubes => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CcPageVmCopyWith<CcPageVm> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CcPageVmCopyWith<$Res> {
  factory $CcPageVmCopyWith(CcPageVm value, $Res Function(CcPageVm) then) =
      _$CcPageVmCopyWithImpl<$Res, CcPageVm>;
  @useResult
  $Res call(
      {String capital,
      String revenue,
      String employmentTax,
      String corporateTax,
      int wealthPosition,
      int operationalCompanies,
      List<String> companyDisplayAssetPaths,
      List<String> discartedAiCardsAssetPaths,
      String currentAiCardAssetPath,
      String checkAiCardAssetPath,
      int remainingCubes});
}

/// @nodoc
class _$CcPageVmCopyWithImpl<$Res, $Val extends CcPageVm>
    implements $CcPageVmCopyWith<$Res> {
  _$CcPageVmCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? capital = null,
    Object? revenue = null,
    Object? employmentTax = null,
    Object? corporateTax = null,
    Object? wealthPosition = null,
    Object? operationalCompanies = null,
    Object? companyDisplayAssetPaths = null,
    Object? discartedAiCardsAssetPaths = null,
    Object? currentAiCardAssetPath = null,
    Object? checkAiCardAssetPath = null,
    Object? remainingCubes = null,
  }) {
    return _then(_value.copyWith(
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as String,
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as String,
      employmentTax: null == employmentTax
          ? _value.employmentTax
          : employmentTax // ignore: cast_nullable_to_non_nullable
              as String,
      corporateTax: null == corporateTax
          ? _value.corporateTax
          : corporateTax // ignore: cast_nullable_to_non_nullable
              as String,
      wealthPosition: null == wealthPosition
          ? _value.wealthPosition
          : wealthPosition // ignore: cast_nullable_to_non_nullable
              as int,
      operationalCompanies: null == operationalCompanies
          ? _value.operationalCompanies
          : operationalCompanies // ignore: cast_nullable_to_non_nullable
              as int,
      companyDisplayAssetPaths: null == companyDisplayAssetPaths
          ? _value.companyDisplayAssetPaths
          : companyDisplayAssetPaths // ignore: cast_nullable_to_non_nullable
              as List<String>,
      discartedAiCardsAssetPaths: null == discartedAiCardsAssetPaths
          ? _value.discartedAiCardsAssetPaths
          : discartedAiCardsAssetPaths // ignore: cast_nullable_to_non_nullable
              as List<String>,
      currentAiCardAssetPath: null == currentAiCardAssetPath
          ? _value.currentAiCardAssetPath
          : currentAiCardAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      checkAiCardAssetPath: null == checkAiCardAssetPath
          ? _value.checkAiCardAssetPath
          : checkAiCardAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      remainingCubes: null == remainingCubes
          ? _value.remainingCubes
          : remainingCubes // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CcPageVmCopyWith<$Res> implements $CcPageVmCopyWith<$Res> {
  factory _$$_CcPageVmCopyWith(
          _$_CcPageVm value, $Res Function(_$_CcPageVm) then) =
      __$$_CcPageVmCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String capital,
      String revenue,
      String employmentTax,
      String corporateTax,
      int wealthPosition,
      int operationalCompanies,
      List<String> companyDisplayAssetPaths,
      List<String> discartedAiCardsAssetPaths,
      String currentAiCardAssetPath,
      String checkAiCardAssetPath,
      int remainingCubes});
}

/// @nodoc
class __$$_CcPageVmCopyWithImpl<$Res>
    extends _$CcPageVmCopyWithImpl<$Res, _$_CcPageVm>
    implements _$$_CcPageVmCopyWith<$Res> {
  __$$_CcPageVmCopyWithImpl(
      _$_CcPageVm _value, $Res Function(_$_CcPageVm) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? capital = null,
    Object? revenue = null,
    Object? employmentTax = null,
    Object? corporateTax = null,
    Object? wealthPosition = null,
    Object? operationalCompanies = null,
    Object? companyDisplayAssetPaths = null,
    Object? discartedAiCardsAssetPaths = null,
    Object? currentAiCardAssetPath = null,
    Object? checkAiCardAssetPath = null,
    Object? remainingCubes = null,
  }) {
    return _then(_$_CcPageVm(
      capital: null == capital
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as String,
      revenue: null == revenue
          ? _value.revenue
          : revenue // ignore: cast_nullable_to_non_nullable
              as String,
      employmentTax: null == employmentTax
          ? _value.employmentTax
          : employmentTax // ignore: cast_nullable_to_non_nullable
              as String,
      corporateTax: null == corporateTax
          ? _value.corporateTax
          : corporateTax // ignore: cast_nullable_to_non_nullable
              as String,
      wealthPosition: null == wealthPosition
          ? _value.wealthPosition
          : wealthPosition // ignore: cast_nullable_to_non_nullable
              as int,
      operationalCompanies: null == operationalCompanies
          ? _value.operationalCompanies
          : operationalCompanies // ignore: cast_nullable_to_non_nullable
              as int,
      companyDisplayAssetPaths: null == companyDisplayAssetPaths
          ? _value._companyDisplayAssetPaths
          : companyDisplayAssetPaths // ignore: cast_nullable_to_non_nullable
              as List<String>,
      discartedAiCardsAssetPaths: null == discartedAiCardsAssetPaths
          ? _value._discartedAiCardsAssetPaths
          : discartedAiCardsAssetPaths // ignore: cast_nullable_to_non_nullable
              as List<String>,
      currentAiCardAssetPath: null == currentAiCardAssetPath
          ? _value.currentAiCardAssetPath
          : currentAiCardAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      checkAiCardAssetPath: null == checkAiCardAssetPath
          ? _value.checkAiCardAssetPath
          : checkAiCardAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      remainingCubes: null == remainingCubes
          ? _value.remainingCubes
          : remainingCubes // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_CcPageVm extends _CcPageVm {
  const _$_CcPageVm(
      {required this.capital,
      required this.revenue,
      required this.employmentTax,
      required this.corporateTax,
      required this.wealthPosition,
      required this.operationalCompanies,
      required final List<String> companyDisplayAssetPaths,
      required final List<String> discartedAiCardsAssetPaths,
      required this.currentAiCardAssetPath,
      required this.checkAiCardAssetPath,
      required this.remainingCubes})
      : _companyDisplayAssetPaths = companyDisplayAssetPaths,
        _discartedAiCardsAssetPaths = discartedAiCardsAssetPaths,
        super._();

  @override
  final String capital;
  @override
  final String revenue;
  @override
  final String employmentTax;
  @override
  final String corporateTax;
  @override
  final int wealthPosition;
  @override
  final int operationalCompanies;
  final List<String> _companyDisplayAssetPaths;
  @override
  List<String> get companyDisplayAssetPaths {
    if (_companyDisplayAssetPaths is EqualUnmodifiableListView)
      return _companyDisplayAssetPaths;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_companyDisplayAssetPaths);
  }

  final List<String> _discartedAiCardsAssetPaths;
  @override
  List<String> get discartedAiCardsAssetPaths {
    if (_discartedAiCardsAssetPaths is EqualUnmodifiableListView)
      return _discartedAiCardsAssetPaths;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_discartedAiCardsAssetPaths);
  }

  @override
  final String currentAiCardAssetPath;
  @override
  final String checkAiCardAssetPath;
  @override
  final int remainingCubes;

  @override
  String toString() {
    return 'CcPageVm(capital: $capital, revenue: $revenue, employmentTax: $employmentTax, corporateTax: $corporateTax, wealthPosition: $wealthPosition, operationalCompanies: $operationalCompanies, companyDisplayAssetPaths: $companyDisplayAssetPaths, discartedAiCardsAssetPaths: $discartedAiCardsAssetPaths, currentAiCardAssetPath: $currentAiCardAssetPath, checkAiCardAssetPath: $checkAiCardAssetPath, remainingCubes: $remainingCubes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CcPageVm &&
            (identical(other.capital, capital) || other.capital == capital) &&
            (identical(other.revenue, revenue) || other.revenue == revenue) &&
            (identical(other.employmentTax, employmentTax) ||
                other.employmentTax == employmentTax) &&
            (identical(other.corporateTax, corporateTax) ||
                other.corporateTax == corporateTax) &&
            (identical(other.wealthPosition, wealthPosition) ||
                other.wealthPosition == wealthPosition) &&
            (identical(other.operationalCompanies, operationalCompanies) ||
                other.operationalCompanies == operationalCompanies) &&
            const DeepCollectionEquality().equals(
                other._companyDisplayAssetPaths, _companyDisplayAssetPaths) &&
            const DeepCollectionEquality().equals(
                other._discartedAiCardsAssetPaths,
                _discartedAiCardsAssetPaths) &&
            (identical(other.currentAiCardAssetPath, currentAiCardAssetPath) ||
                other.currentAiCardAssetPath == currentAiCardAssetPath) &&
            (identical(other.checkAiCardAssetPath, checkAiCardAssetPath) ||
                other.checkAiCardAssetPath == checkAiCardAssetPath) &&
            (identical(other.remainingCubes, remainingCubes) ||
                other.remainingCubes == remainingCubes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      capital,
      revenue,
      employmentTax,
      corporateTax,
      wealthPosition,
      operationalCompanies,
      const DeepCollectionEquality().hash(_companyDisplayAssetPaths),
      const DeepCollectionEquality().hash(_discartedAiCardsAssetPaths),
      currentAiCardAssetPath,
      checkAiCardAssetPath,
      remainingCubes);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CcPageVmCopyWith<_$_CcPageVm> get copyWith =>
      __$$_CcPageVmCopyWithImpl<_$_CcPageVm>(this, _$identity);
}

abstract class _CcPageVm extends CcPageVm {
  const factory _CcPageVm(
      {required final String capital,
      required final String revenue,
      required final String employmentTax,
      required final String corporateTax,
      required final int wealthPosition,
      required final int operationalCompanies,
      required final List<String> companyDisplayAssetPaths,
      required final List<String> discartedAiCardsAssetPaths,
      required final String currentAiCardAssetPath,
      required final String checkAiCardAssetPath,
      required final int remainingCubes}) = _$_CcPageVm;
  const _CcPageVm._() : super._();

  @override
  String get capital;
  @override
  String get revenue;
  @override
  String get employmentTax;
  @override
  String get corporateTax;
  @override
  int get wealthPosition;
  @override
  int get operationalCompanies;
  @override
  List<String> get companyDisplayAssetPaths;
  @override
  List<String> get discartedAiCardsAssetPaths;
  @override
  String get currentAiCardAssetPath;
  @override
  String get checkAiCardAssetPath;
  @override
  int get remainingCubes;
  @override
  @JsonKey(ignore: true)
  _$$_CcPageVmCopyWith<_$_CcPageVm> get copyWith =>
      throw _privateConstructorUsedError;
}
