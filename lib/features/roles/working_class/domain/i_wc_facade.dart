import 'package:hegemony_automa/features/roles/working_class/domain/wc_game_board.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';
import 'package:hegemony_automa/features/shared/domain/industries.dart';

abstract class IWcFacade {
  WcGameBoard get gameBoard;

  set influence(int quantity);

  void setPrice(GoodOrServiceType type, int price);
  void setPriceLevel({
    required GoodOrServiceType type,
    required PriceLevel level,
  });

  void setTradeUnion({
    required Industries industry,
    required bool hasTradeUnion,
  });

  void setQuantity({
    required GoodOrServiceType type,
    required int quantity,
  });

  void setIncome(int amount);

  void reset();
  void takeLoan();

  void repayLoan();

  int get incomeTax;

  int get prosperity;
  set prosperity(int prosperity);

  int get workers;
  int get population;
  set workers(int workers);

  void drawAiCard();
  void reshuffleDiscartedAiCards();
}
