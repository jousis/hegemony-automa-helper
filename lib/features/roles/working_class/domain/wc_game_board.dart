import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hegemony_automa/features/ai_card/domain/ai_card.dart';
import 'package:hegemony_automa/features/shared/domain/good_or_service_pricelist.dart';

part 'wc_game_board.freezed.dart';

@freezed
class WcGameBoard with _$WcGameBoard {
  const WcGameBoard._();
  const factory WcGameBoard({
    required int income,
    required int activeLoans,
    required int workers,
    required int population,
    required int prosperityTrackPosition,
    required List<Map<GoodOrServiceType, int>> goodsOrServices,
    required int influence,
    required AiCard? previousAiCard,
    required AiCard? currentAiCard,
  }) = _WcGameBoard;

  factory WcGameBoard.defaults() => const WcGameBoard(
        income: 0,
        activeLoans: 0,
        workers: 10,
        population: 3,
        prosperityTrackPosition: 0,
        goodsOrServices: [],
        influence: 0,
        previousAiCard: null,
        currentAiCard: null,
      );
}
