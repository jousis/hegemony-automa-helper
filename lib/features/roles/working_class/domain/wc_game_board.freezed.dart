// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'wc_game_board.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$WcGameBoard {
  int get income => throw _privateConstructorUsedError;
  int get activeLoans => throw _privateConstructorUsedError;
  int get workers => throw _privateConstructorUsedError;
  int get population => throw _privateConstructorUsedError;
  int get prosperityTrackPosition => throw _privateConstructorUsedError;
  List<Map<GoodOrServiceType, int>> get goodsOrServices =>
      throw _privateConstructorUsedError;
  int get influence => throw _privateConstructorUsedError;
  AiCard? get previousAiCard => throw _privateConstructorUsedError;
  AiCard? get currentAiCard => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WcGameBoardCopyWith<WcGameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WcGameBoardCopyWith<$Res> {
  factory $WcGameBoardCopyWith(
          WcGameBoard value, $Res Function(WcGameBoard) then) =
      _$WcGameBoardCopyWithImpl<$Res, WcGameBoard>;
  @useResult
  $Res call(
      {int income,
      int activeLoans,
      int workers,
      int population,
      int prosperityTrackPosition,
      List<Map<GoodOrServiceType, int>> goodsOrServices,
      int influence,
      AiCard? previousAiCard,
      AiCard? currentAiCard});

  $AiCardCopyWith<$Res>? get previousAiCard;
  $AiCardCopyWith<$Res>? get currentAiCard;
}

/// @nodoc
class _$WcGameBoardCopyWithImpl<$Res, $Val extends WcGameBoard>
    implements $WcGameBoardCopyWith<$Res> {
  _$WcGameBoardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? income = null,
    Object? activeLoans = null,
    Object? workers = null,
    Object? population = null,
    Object? prosperityTrackPosition = null,
    Object? goodsOrServices = null,
    Object? influence = null,
    Object? previousAiCard = freezed,
    Object? currentAiCard = freezed,
  }) {
    return _then(_value.copyWith(
      income: null == income
          ? _value.income
          : income // ignore: cast_nullable_to_non_nullable
              as int,
      activeLoans: null == activeLoans
          ? _value.activeLoans
          : activeLoans // ignore: cast_nullable_to_non_nullable
              as int,
      workers: null == workers
          ? _value.workers
          : workers // ignore: cast_nullable_to_non_nullable
              as int,
      population: null == population
          ? _value.population
          : population // ignore: cast_nullable_to_non_nullable
              as int,
      prosperityTrackPosition: null == prosperityTrackPosition
          ? _value.prosperityTrackPosition
          : prosperityTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      goodsOrServices: null == goodsOrServices
          ? _value.goodsOrServices
          : goodsOrServices // ignore: cast_nullable_to_non_nullable
              as List<Map<GoodOrServiceType, int>>,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      previousAiCard: freezed == previousAiCard
          ? _value.previousAiCard
          : previousAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
      currentAiCard: freezed == currentAiCard
          ? _value.currentAiCard
          : currentAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $AiCardCopyWith<$Res>? get previousAiCard {
    if (_value.previousAiCard == null) {
      return null;
    }

    return $AiCardCopyWith<$Res>(_value.previousAiCard!, (value) {
      return _then(_value.copyWith(previousAiCard: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $AiCardCopyWith<$Res>? get currentAiCard {
    if (_value.currentAiCard == null) {
      return null;
    }

    return $AiCardCopyWith<$Res>(_value.currentAiCard!, (value) {
      return _then(_value.copyWith(currentAiCard: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_WcGameBoardCopyWith<$Res>
    implements $WcGameBoardCopyWith<$Res> {
  factory _$$_WcGameBoardCopyWith(
          _$_WcGameBoard value, $Res Function(_$_WcGameBoard) then) =
      __$$_WcGameBoardCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int income,
      int activeLoans,
      int workers,
      int population,
      int prosperityTrackPosition,
      List<Map<GoodOrServiceType, int>> goodsOrServices,
      int influence,
      AiCard? previousAiCard,
      AiCard? currentAiCard});

  @override
  $AiCardCopyWith<$Res>? get previousAiCard;
  @override
  $AiCardCopyWith<$Res>? get currentAiCard;
}

/// @nodoc
class __$$_WcGameBoardCopyWithImpl<$Res>
    extends _$WcGameBoardCopyWithImpl<$Res, _$_WcGameBoard>
    implements _$$_WcGameBoardCopyWith<$Res> {
  __$$_WcGameBoardCopyWithImpl(
      _$_WcGameBoard _value, $Res Function(_$_WcGameBoard) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? income = null,
    Object? activeLoans = null,
    Object? workers = null,
    Object? population = null,
    Object? prosperityTrackPosition = null,
    Object? goodsOrServices = null,
    Object? influence = null,
    Object? previousAiCard = freezed,
    Object? currentAiCard = freezed,
  }) {
    return _then(_$_WcGameBoard(
      income: null == income
          ? _value.income
          : income // ignore: cast_nullable_to_non_nullable
              as int,
      activeLoans: null == activeLoans
          ? _value.activeLoans
          : activeLoans // ignore: cast_nullable_to_non_nullable
              as int,
      workers: null == workers
          ? _value.workers
          : workers // ignore: cast_nullable_to_non_nullable
              as int,
      population: null == population
          ? _value.population
          : population // ignore: cast_nullable_to_non_nullable
              as int,
      prosperityTrackPosition: null == prosperityTrackPosition
          ? _value.prosperityTrackPosition
          : prosperityTrackPosition // ignore: cast_nullable_to_non_nullable
              as int,
      goodsOrServices: null == goodsOrServices
          ? _value._goodsOrServices
          : goodsOrServices // ignore: cast_nullable_to_non_nullable
              as List<Map<GoodOrServiceType, int>>,
      influence: null == influence
          ? _value.influence
          : influence // ignore: cast_nullable_to_non_nullable
              as int,
      previousAiCard: freezed == previousAiCard
          ? _value.previousAiCard
          : previousAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
      currentAiCard: freezed == currentAiCard
          ? _value.currentAiCard
          : currentAiCard // ignore: cast_nullable_to_non_nullable
              as AiCard?,
    ));
  }
}

/// @nodoc

class _$_WcGameBoard extends _WcGameBoard {
  const _$_WcGameBoard(
      {required this.income,
      required this.activeLoans,
      required this.workers,
      required this.population,
      required this.prosperityTrackPosition,
      required final List<Map<GoodOrServiceType, int>> goodsOrServices,
      required this.influence,
      required this.previousAiCard,
      required this.currentAiCard})
      : _goodsOrServices = goodsOrServices,
        super._();

  @override
  final int income;
  @override
  final int activeLoans;
  @override
  final int workers;
  @override
  final int population;
  @override
  final int prosperityTrackPosition;
  final List<Map<GoodOrServiceType, int>> _goodsOrServices;
  @override
  List<Map<GoodOrServiceType, int>> get goodsOrServices {
    if (_goodsOrServices is EqualUnmodifiableListView) return _goodsOrServices;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_goodsOrServices);
  }

  @override
  final int influence;
  @override
  final AiCard? previousAiCard;
  @override
  final AiCard? currentAiCard;

  @override
  String toString() {
    return 'WcGameBoard(income: $income, activeLoans: $activeLoans, workers: $workers, population: $population, prosperityTrackPosition: $prosperityTrackPosition, goodsOrServices: $goodsOrServices, influence: $influence, previousAiCard: $previousAiCard, currentAiCard: $currentAiCard)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WcGameBoard &&
            (identical(other.income, income) || other.income == income) &&
            (identical(other.activeLoans, activeLoans) ||
                other.activeLoans == activeLoans) &&
            (identical(other.workers, workers) || other.workers == workers) &&
            (identical(other.population, population) ||
                other.population == population) &&
            (identical(
                    other.prosperityTrackPosition, prosperityTrackPosition) ||
                other.prosperityTrackPosition == prosperityTrackPosition) &&
            const DeepCollectionEquality()
                .equals(other._goodsOrServices, _goodsOrServices) &&
            (identical(other.influence, influence) ||
                other.influence == influence) &&
            (identical(other.previousAiCard, previousAiCard) ||
                other.previousAiCard == previousAiCard) &&
            (identical(other.currentAiCard, currentAiCard) ||
                other.currentAiCard == currentAiCard));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      income,
      activeLoans,
      workers,
      population,
      prosperityTrackPosition,
      const DeepCollectionEquality().hash(_goodsOrServices),
      influence,
      previousAiCard,
      currentAiCard);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WcGameBoardCopyWith<_$_WcGameBoard> get copyWith =>
      __$$_WcGameBoardCopyWithImpl<_$_WcGameBoard>(this, _$identity);
}

abstract class _WcGameBoard extends WcGameBoard {
  const factory _WcGameBoard(
      {required final int income,
      required final int activeLoans,
      required final int workers,
      required final int population,
      required final int prosperityTrackPosition,
      required final List<Map<GoodOrServiceType, int>> goodsOrServices,
      required final int influence,
      required final AiCard? previousAiCard,
      required final AiCard? currentAiCard}) = _$_WcGameBoard;
  const _WcGameBoard._() : super._();

  @override
  int get income;
  @override
  int get activeLoans;
  @override
  int get workers;
  @override
  int get population;
  @override
  int get prosperityTrackPosition;
  @override
  List<Map<GoodOrServiceType, int>> get goodsOrServices;
  @override
  int get influence;
  @override
  AiCard? get previousAiCard;
  @override
  AiCard? get currentAiCard;
  @override
  @JsonKey(ignore: true)
  _$$_WcGameBoardCopyWith<_$_WcGameBoard> get copyWith =>
      throw _privateConstructorUsedError;
}
