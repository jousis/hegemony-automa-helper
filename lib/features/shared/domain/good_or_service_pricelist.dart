import 'package:freezed_annotation/freezed_annotation.dart';

part 'good_or_service_pricelist.freezed.dart';

enum PriceLevel { low, mid, high }

enum GoodOrServiceType { food, luxury, healthcare, education }

@freezed
class GoodOrServicePricelist with _$GoodOrServicePricelist {
  const factory GoodOrServicePricelist({
    required GoodOrServiceType type,
    required List<GoodOrServicePrice> prices,
  }) = _GoodOrServicePricelist;
}

@freezed
class GoodOrServicePrice with _$GoodOrServicePrice {
  const factory GoodOrServicePrice({
    required PriceLevel level,
    required int price,
  }) = _GoodOrServicePrice;
}
