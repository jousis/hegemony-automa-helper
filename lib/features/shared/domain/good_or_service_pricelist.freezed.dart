// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'good_or_service_pricelist.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$GoodOrServicePricelist {
  GoodOrServiceType get type => throw _privateConstructorUsedError;
  List<GoodOrServicePrice> get prices => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GoodOrServicePricelistCopyWith<GoodOrServicePricelist> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GoodOrServicePricelistCopyWith<$Res> {
  factory $GoodOrServicePricelistCopyWith(GoodOrServicePricelist value,
          $Res Function(GoodOrServicePricelist) then) =
      _$GoodOrServicePricelistCopyWithImpl<$Res, GoodOrServicePricelist>;
  @useResult
  $Res call({GoodOrServiceType type, List<GoodOrServicePrice> prices});
}

/// @nodoc
class _$GoodOrServicePricelistCopyWithImpl<$Res,
        $Val extends GoodOrServicePricelist>
    implements $GoodOrServicePricelistCopyWith<$Res> {
  _$GoodOrServicePricelistCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? prices = null,
  }) {
    return _then(_value.copyWith(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
      prices: null == prices
          ? _value.prices
          : prices // ignore: cast_nullable_to_non_nullable
              as List<GoodOrServicePrice>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GoodOrServicePricelistCopyWith<$Res>
    implements $GoodOrServicePricelistCopyWith<$Res> {
  factory _$$_GoodOrServicePricelistCopyWith(_$_GoodOrServicePricelist value,
          $Res Function(_$_GoodOrServicePricelist) then) =
      __$$_GoodOrServicePricelistCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({GoodOrServiceType type, List<GoodOrServicePrice> prices});
}

/// @nodoc
class __$$_GoodOrServicePricelistCopyWithImpl<$Res>
    extends _$GoodOrServicePricelistCopyWithImpl<$Res,
        _$_GoodOrServicePricelist>
    implements _$$_GoodOrServicePricelistCopyWith<$Res> {
  __$$_GoodOrServicePricelistCopyWithImpl(_$_GoodOrServicePricelist _value,
      $Res Function(_$_GoodOrServicePricelist) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? type = null,
    Object? prices = null,
  }) {
    return _then(_$_GoodOrServicePricelist(
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as GoodOrServiceType,
      prices: null == prices
          ? _value._prices
          : prices // ignore: cast_nullable_to_non_nullable
              as List<GoodOrServicePrice>,
    ));
  }
}

/// @nodoc

class _$_GoodOrServicePricelist implements _GoodOrServicePricelist {
  const _$_GoodOrServicePricelist(
      {required this.type, required final List<GoodOrServicePrice> prices})
      : _prices = prices;

  @override
  final GoodOrServiceType type;
  final List<GoodOrServicePrice> _prices;
  @override
  List<GoodOrServicePrice> get prices {
    if (_prices is EqualUnmodifiableListView) return _prices;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_prices);
  }

  @override
  String toString() {
    return 'GoodOrServicePricelist(type: $type, prices: $prices)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GoodOrServicePricelist &&
            (identical(other.type, type) || other.type == type) &&
            const DeepCollectionEquality().equals(other._prices, _prices));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, type, const DeepCollectionEquality().hash(_prices));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GoodOrServicePricelistCopyWith<_$_GoodOrServicePricelist> get copyWith =>
      __$$_GoodOrServicePricelistCopyWithImpl<_$_GoodOrServicePricelist>(
          this, _$identity);
}

abstract class _GoodOrServicePricelist implements GoodOrServicePricelist {
  const factory _GoodOrServicePricelist(
          {required final GoodOrServiceType type,
          required final List<GoodOrServicePrice> prices}) =
      _$_GoodOrServicePricelist;

  @override
  GoodOrServiceType get type;
  @override
  List<GoodOrServicePrice> get prices;
  @override
  @JsonKey(ignore: true)
  _$$_GoodOrServicePricelistCopyWith<_$_GoodOrServicePricelist> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GoodOrServicePrice {
  PriceLevel get level => throw _privateConstructorUsedError;
  int get price => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GoodOrServicePriceCopyWith<GoodOrServicePrice> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GoodOrServicePriceCopyWith<$Res> {
  factory $GoodOrServicePriceCopyWith(
          GoodOrServicePrice value, $Res Function(GoodOrServicePrice) then) =
      _$GoodOrServicePriceCopyWithImpl<$Res, GoodOrServicePrice>;
  @useResult
  $Res call({PriceLevel level, int price});
}

/// @nodoc
class _$GoodOrServicePriceCopyWithImpl<$Res, $Val extends GoodOrServicePrice>
    implements $GoodOrServicePriceCopyWith<$Res> {
  _$GoodOrServicePriceCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? level = null,
    Object? price = null,
  }) {
    return _then(_value.copyWith(
      level: null == level
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as PriceLevel,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_GoodOrServicePriceCopyWith<$Res>
    implements $GoodOrServicePriceCopyWith<$Res> {
  factory _$$_GoodOrServicePriceCopyWith(_$_GoodOrServicePrice value,
          $Res Function(_$_GoodOrServicePrice) then) =
      __$$_GoodOrServicePriceCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({PriceLevel level, int price});
}

/// @nodoc
class __$$_GoodOrServicePriceCopyWithImpl<$Res>
    extends _$GoodOrServicePriceCopyWithImpl<$Res, _$_GoodOrServicePrice>
    implements _$$_GoodOrServicePriceCopyWith<$Res> {
  __$$_GoodOrServicePriceCopyWithImpl(
      _$_GoodOrServicePrice _value, $Res Function(_$_GoodOrServicePrice) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? level = null,
    Object? price = null,
  }) {
    return _then(_$_GoodOrServicePrice(
      level: null == level
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as PriceLevel,
      price: null == price
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_GoodOrServicePrice implements _GoodOrServicePrice {
  const _$_GoodOrServicePrice({required this.level, required this.price});

  @override
  final PriceLevel level;
  @override
  final int price;

  @override
  String toString() {
    return 'GoodOrServicePrice(level: $level, price: $price)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GoodOrServicePrice &&
            (identical(other.level, level) || other.level == level) &&
            (identical(other.price, price) || other.price == price));
  }

  @override
  int get hashCode => Object.hash(runtimeType, level, price);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GoodOrServicePriceCopyWith<_$_GoodOrServicePrice> get copyWith =>
      __$$_GoodOrServicePriceCopyWithImpl<_$_GoodOrServicePrice>(
          this, _$identity);
}

abstract class _GoodOrServicePrice implements GoodOrServicePrice {
  const factory _GoodOrServicePrice(
      {required final PriceLevel level,
      required final int price}) = _$_GoodOrServicePrice;

  @override
  PriceLevel get level;
  @override
  int get price;
  @override
  @JsonKey(ignore: true)
  _$$_GoodOrServicePriceCopyWith<_$_GoodOrServicePrice> get copyWith =>
      throw _privateConstructorUsedError;
}
