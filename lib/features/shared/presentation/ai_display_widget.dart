import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hegemony_automa/features/instruction_card/presentation/instruction_card_vm.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/application/bloc/cc_bloc.dart';
import 'package:hegemony_automa/features/shared/presentation/card_popup_dialog_widget.dart';
import 'package:hegemony_automa/features/shared/presentation/stacked_list_popup_dialog_widget.dart';

class AiDisplayWidget extends StatelessWidget {
  const AiDisplayWidget({
    required this.color,
    required this.currentAiCard,
    required this.discartedAiCards,
    required this.instructionCards,
    required this.onDraw,
    required this.onReshuffle,
    super.key,
  });
  final String currentAiCard;
  final List<String> discartedAiCards;
  final List<InstructionCardVm> instructionCards;
  final void Function()? onDraw;
  final void Function()? onReshuffle;

  final Color? color;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => SizedBox(
        height: 200,
        child: Row(
          children: [
            Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(28.0),
                    child: IconButton(
                      iconSize: 64,
                      onPressed: onDraw,
                      icon: Icon(
                        Icons.addchart,
                        color: color,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: IconButton(
                    onPressed: onReshuffle,
                    icon: Icon(
                      Icons.refresh,
                      color: color,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Wrap(
                clipBehavior: Clip.hardEdge,
                alignment: WrapAlignment.start,
                children: [
                  if (currentAiCard.isEmpty)
                    SizedBox(
                      width: 130,
                      height: 180,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2,
                            color: Colors.white,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: AutoSizeText(
                              discartedAiCards.isEmpty
                                  ? 'Please click the draw button on the left to draw new AI card.'
                                  : 'No More AI Cards on draw pile.\n\nPlease reshuffle.',
                              maxFontSize: 32,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 32,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  if (currentAiCard.isNotEmpty)
                    InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) => Dialog(
                            backgroundColor: Colors.black,
                            child: CardPopupDialogWidget(
                              imagePath: currentAiCard,
                              allowRemoval: false,
                            ),
                          ),
                        );
                      },
                      child: SizedBox(
                        // width: 130,
                        height: 180,
                        child: Image.asset(
                          currentAiCard,
                          errorBuilder: (context, error, stackTrace) => const SizedBox(
                            width: 130,
                            height: 180,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    color: Colors.white.withAlpha(128),
                    iconSize: 46,
                    onPressed: () {
                      if (discartedAiCards.isEmpty) return;
                      showDialog(
                        context: context,
                        builder: (context) => Dialog(
                          backgroundColor: Colors.black,
                          child: StackedListPopupDialogWidget(
                            items: discartedAiCards,
                          ),
                        ),
                      );
                    },
                    icon: const Icon(
                      Icons.history,
                    ),
                  ),
                  IconButton(
                    color: Colors.white.withAlpha(128),
                    iconSize: 46,
                    onPressed: () {
                      context.read<CcBloc>().add(const CcEvent.checkAiCard());
                    },
                    icon: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        const Icon(
                          Icons.chat_bubble,
                        ),
                        Icon(
                          Icons.add,
                          size: 32,
                          color: Colors.blue.shade800,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  // constraints: BoxConstraints(
                  //   maxHeight: constraints.maxHeight,
                  //   maxWidth: 500,
                  // ),
                  width: 400,
                  height: constraints.maxHeight,
                  child: ListView.separated(
                    itemCount: instructionCards.length,
                    separatorBuilder: (context, index) => const Divider(
                      height: 10,
                      color: Colors.white,
                    ),
                    itemBuilder: (context, index) => InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) => Dialog(
                            backgroundColor: Colors.black,
                            child: CardPopupDialogWidget(
                              imagePath: instructionCards[index].assetPath,
                              allowRemoval: false,
                            ),
                          ),
                        );
                      },
                      child: ListTile(
                        trailing: const Icon(Icons.chevron_right),
                        title: AutoSizeText(
                          instructionCards[index].title,
                          maxLines: 3,
                          minFontSize: 10,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
