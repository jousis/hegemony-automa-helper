import 'package:flutter/material.dart';

class CardPlaceHolder extends StatelessWidget {
  const CardPlaceHolder({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Colors.white.withAlpha(125),
        ),
      ),
      child: Center(
        child: CircleAvatar(
          backgroundColor: Colors.white.withAlpha(125),
          foregroundColor: Colors.white,
          child: const Icon(
            Icons.add,
            // size: 54,
          ),
        ),
      ),
    );
  }
}
