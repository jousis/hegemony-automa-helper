import 'dart:math';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class CardPopupDialogWidget extends StatelessWidget {
  const CardPopupDialogWidget({
    required this.imagePath,
    required this.allowRemoval,
    super.key,
  });

  final String imagePath;
  final bool allowRemoval;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: min(
            400,
            constraints.maxWidth,
          ),
          maxHeight: min(
            800,
            constraints.maxHeight,
          ),
        ),
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            InkWell(
              onTap: () {
                context.pop();
              },
              child: Image.asset(imagePath),
            ),
            if (allowRemoval)
              SizedBox(
                width: 50,
                height: 50,
                child: CircleAvatar(
                  backgroundColor: Colors.red,
                  child: IconButton(
                    iconSize: 32,
                    onPressed: () async {
                      final dialog = AlertDialog(
                        title: const Text('Remove Card ?'),
                        content: const Text('Do you want to remove this card from the display ?'),
                        elevation: 24,
                        actions: [
                          TextButton(
                            onPressed: () {
                              context.pop('no');
                            },
                            child: const Text('No'),
                          ),
                          TextButton(
                            onPressed: () {
                              context.pop('yes');
                            },
                            child: const Text('Yes'),
                          ),
                        ],
                      );
                      final result = await showDialog<String>(
                        context: context,
                        builder: (_) => dialog,
                        barrierDismissible: false,
                      );
                      if (result != null && result == 'yes') {
                        //remove card !
                        if (context.mounted) {
                          context.pop(false);
                          return;
                        }
                      }
                      if (context.mounted) {
                        context.pop();
                      }
                    },
                    icon: const Icon(
                      Icons.delete_forever,
                      color: Colors.black,
                      size: 32,
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
