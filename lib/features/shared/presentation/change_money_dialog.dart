import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ChangeMoneyDialog extends StatefulWidget {
  const ChangeMoneyDialog({
    required this.title,
    required this.currentAmount,
    super.key,
  });

  final String title;
  final int currentAmount;

  @override
  State<ChangeMoneyDialog> createState() => _ChangeMoneyDialogState();
}

class _ChangeMoneyDialogState extends State<ChangeMoneyDialog> {
  int selected = 0;

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      height: 300,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                widget.title,
                style: const TextStyle(color: Colors.white),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () => setState(() {
                            selected = 0;
                          }),
                          child: ActionSelectorWidget(
                            isSelected: selected == 0,
                            text: '+',
                          ),
                        ),
                        InkWell(
                          onTap: () => setState(() {
                            selected = 1;
                          }),
                          child: ActionSelectorWidget(
                            isSelected: selected == 1,
                            text: '=',
                          ),
                        ),
                        InkWell(
                          onTap: () => setState(() {
                            selected = 2;
                          }),
                          child: ActionSelectorWidget(
                            isSelected: selected == 2,
                            text: '-',
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: TextField(
                      autofocus: true,
                      controller: _controller,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.right,
                      style: const TextStyle(fontSize: 33),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 100,
                height: 50,
                child: OutlinedButton(
                  onPressed: () {
                    int newAmount = widget.currentAmount;
                    final inputInt = int.tryParse(_controller.text) ?? 0;
                    switch (selected) {
                      case 0:
                        newAmount += inputInt;
                        break;
                      case 1:
                        newAmount = inputInt;
                        break;
                      case 2:
                        newAmount -= inputInt;
                        break;
                    }
                    context.pop(newAmount);
                  },
                  style: const ButtonStyle(
                    foregroundColor: MaterialStatePropertyAll<Color?>(
                      Colors.white,
                    ),
                  ),
                  child: const Text('OK'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ActionSelectorWidget extends StatelessWidget {
  const ActionSelectorWidget({
    super.key,
    required this.isSelected,
    required this.text,
  });

  final bool isSelected;
  final String text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 50,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: isSelected ? Colors.grey : null,
          border: Border.all(
            color: Colors.white.withAlpha(50),
          ),
        ),
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontFamily: 'Arial',
              color: Colors.white,
              fontSize: 32,
            ),
          ),
        ),
      ),
    );
  }
}
