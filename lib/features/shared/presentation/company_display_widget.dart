import 'package:flutter/material.dart';
import 'package:hegemony_automa/features/shared/presentation/card_placeholder.dart';
import 'package:hegemony_automa/features/shared/presentation/card_popup_dialog_widget.dart';

class CompanyDisplayWidget extends StatelessWidget {
  const CompanyDisplayWidget({
    required this.color,
    required this.companies,
    required this.onDraw,
    required this.onReshuffle,
    required this.onRemoveCard,
    required this.drawCardOnIndex,
    super.key,
  });

  final List<String> companies;
  final void Function()? onDraw;
  final void Function()? onReshuffle;
  final void Function(int) onRemoveCard;
  final void Function(int) drawCardOnIndex;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Row(
        children: [
          Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: IconButton(
                    iconSize: 64,
                    onPressed: onDraw,
                    icon: Icon(
                      Icons.add_business_outlined,
                      color: color,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: IconButton(
                  onPressed: onReshuffle,
                  icon: Icon(
                    Icons.refresh,
                    color: color,
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: companies.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CompanyCardWidget(
                    index: index,
                    companyAssetPath: companies[index],
                    onRemoveCard: onRemoveCard,
                    drawCardOnIndex: drawCardOnIndex,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CompanyCardWidget extends StatelessWidget {
  const CompanyCardWidget({
    required this.index,
    required this.companyAssetPath,
    required this.onRemoveCard,
    required this.drawCardOnIndex,
    super.key,
  });

  final String companyAssetPath;
  final int index;
  final void Function(int) onRemoveCard;
  final void Function(int) drawCardOnIndex;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 130,
      height: 200,
      child: InkWell(
        onTap: () async {
          if (companyAssetPath.isEmpty) {
            drawCardOnIndex(index);
            return;
          }
          final result = await showDialog<bool>(
            context: context,
            builder: (context) => Dialog(
              child: CardPopupDialogWidget(
                imagePath: companyAssetPath,
                allowRemoval: true,
              ),
            ),
          );
          //false == remove card
          if (result != null && !result) {
            onRemoveCard(index);
          }
        },
        child: Image.asset(
          companyAssetPath,
          // width: 150,
          // height: 200,
          errorBuilder: (
            context,
            error,
            stackTrace,
          ) =>
              const CardPlaceHolder(),
        ),
      ),
    );
  }
}
