import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class ExtraStorageTile extends StatelessWidget {
  const ExtraStorageTile({
    super.key,
    required this.quantity,
    required this.maxQuantity,
    required this.onChanged,
  });

  final int maxQuantity;
  final int quantity;
  final void Function(int) onChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: const Color(0xFF1F2B37),
      child: SizedBox(
        width: 120,
        height: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 120,
              height: 60,
              child: FlutterCarousel.builder(
                options: CarouselOptions(
                  viewportFraction: 0.9,
                  scrollDirection: Axis.vertical,
                  showIndicator: false,
                  enlargeCenterPage: true,
                  enlargeStrategy: CenterPageEnlargeStrategy.height,
                  initialPage: quantity,
                  onPageChanged: (index, reason) => onChanged(index),
                ),
                itemCount: maxQuantity + 1,
                itemBuilder: (context, index, realIndex) {
                  return Center(
                    child: Text(
                      '$index',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
