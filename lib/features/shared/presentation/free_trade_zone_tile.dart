import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class FreeTradeZoneTile extends StatelessWidget {
  const FreeTradeZoneTile({
    super.key,
    required this.foodQuantity,
    required this.maxFoodQuantity,
    required this.luxuryQuantity,
    required this.maxLuxuryQuantity,
    required this.foodOnChanged,
    required this.luxuryOnChanged,
  });

  final int foodQuantity;
  final int maxFoodQuantity;
  final int luxuryQuantity;
  final int maxLuxuryQuantity;
  final void Function(int) foodOnChanged;
  final void Function(int) luxuryOnChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: const Color(0xFF005455),
      child: SizedBox(
        width: 240,
        height: 100,
        child: Center(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Icon(
                Icons.travel_explore,
                size: 56,
                color: Colors.blueGrey.shade100.withAlpha(
                  12,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 60,
                    height: 80,
                    child: FlutterCarousel.builder(
                      options: CarouselOptions(
                        viewportFraction: 0.9,
                        scrollDirection: Axis.vertical,
                        showIndicator: false,
                        enlargeCenterPage: true,
                        enlargeStrategy: CenterPageEnlargeStrategy.height,
                        initialPage: foodQuantity,
                        onPageChanged: (index, reason) => foodOnChanged(index),
                      ),
                      itemCount: maxFoodQuantity + 1,
                      itemBuilder: (context, index, realIndex) {
                        return Center(
                          child: Text(
                            '$index',
                            style: const TextStyle(
                              color: Colors.green,
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    width: 60,
                    height: 80,
                    child: FlutterCarousel.builder(
                      options: CarouselOptions(
                        viewportFraction: 0.9,
                        scrollDirection: Axis.vertical,
                        showIndicator: false,
                        enlargeCenterPage: true,
                        enlargeStrategy: CenterPageEnlargeStrategy.height,
                        initialPage: luxuryQuantity,
                        onPageChanged: (index, reason) => luxuryOnChanged(index),
                      ),
                      itemCount: maxLuxuryQuantity + 1,
                      itemBuilder: (context, index, realIndex) {
                        return Center(
                          child: Text(
                            '$index',
                            style: const TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
