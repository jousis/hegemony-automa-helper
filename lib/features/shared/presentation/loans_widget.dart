import 'package:flutter/material.dart';

class LoansWidget extends StatelessWidget {
  const LoansWidget({
    required this.count,
    required this.take,
    required this.payOff,
    super.key,
  });

  final int count;
  final void Function()? take;
  final void Function()? payOff;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxHeight: 120,
        maxWidth: 200,
      ),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: take,
                icon: const Icon(
                  Icons.add,
                  color: Color(0xFFFCB42E),
                ),
              ),
              IconButton(
                onPressed: payOff,
                icon: const Icon(
                  Icons.payment,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Stack(
            alignment: Alignment.centerLeft,
            children: List.generate(
              count,
              (index) => Padding(
                padding: EdgeInsets.only(
                  left: index * 15,
                ),
                child: const LoanWidget(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LoanWidget extends StatelessWidget {
  const LoanWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: 60,
        maxHeight: 120,
      ),
      child: Card(
        elevation: 1,
        shadowColor: const Color(0xFFFCB42E),
        color: const Color(0xFF333A42),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // mainAxisSize: MainAxisSize.max,
          children: const [
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                'LOAN',
                style: TextStyle(
                  color: Color(0xFFFCB42E),
                  fontSize: 12,
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  '50∀',
                  style: TextStyle(
                    color: Color(0xFFFCB42E),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
