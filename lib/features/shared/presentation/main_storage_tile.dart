import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';
import 'package:hegemony_automa/features/shared/presentation/extra_storage_tile.dart';
import 'package:hegemony_automa/features/shared/presentation/selection_cube.dart';

class MainStorageTile extends StatelessWidget {
  const MainStorageTile({
    super.key,
    required this.color,
    required this.storageType,
    required this.quantity,
    required this.extraQuantity,
    required this.maxQuantity,
    required this.maxExtraQuantity,
    required this.price,
    required this.prices,
    required this.onChanged,
    required this.addStorageOnPressed,
    required this.extraQuantityOnChanged,
    required this.onPriceTap,
  });

  final Color color;
  final String storageType;
  final int maxQuantity;
  final int maxExtraQuantity;
  final int quantity;
  final int? extraQuantity;
  final int price;
  final List<int> prices;
  final void Function(int) onChanged;
  final void Function(int) extraQuantityOnChanged;
  final void Function(int) onPriceTap;
  final void Function()? addStorageOnPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blueGrey.shade800,
      child: SizedBox(
        width: 120,
        height: 250,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              storageType,
              style: TextStyle(
                color: color,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 80,
              width: 120,
              child: FlutterCarousel.builder(
                options: CarouselOptions(
                  viewportFraction: 0.9,
                  scrollDirection: Axis.vertical,
                  showIndicator: false,
                  enlargeCenterPage: true,
                  enlargeStrategy: CenterPageEnlargeStrategy.height,
                  initialPage: quantity,
                  onPageChanged: (index, reason) => onChanged(index),
                ),
                itemCount: maxQuantity + 1,
                itemBuilder: (context, index, realIndex) {
                  return Center(
                    child: Text(
                      '$index',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                      ),
                    ),
                  );
                },
              ),
            ),
            if (prices.isEmpty)
              const SizedBox(
                height: 42,
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: prices
                  .map(
                    (p) => Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 2,
                        vertical: 0,
                      ),
                      child: InkWell(
                        onTap: () => onPriceTap(p),
                        child: DecoratedBox(
                          decoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          child: SizedBox(
                            width: 32,
                            height: 32,
                            child: Stack(
                              children: [
                                if (p == price)
                                  const Center(
                                    child: SelectionCube(
                                      color: Colors.white,
                                    ),
                                  ),
                                Center(
                                  child: AutoSizeText(
                                    '$p',
                                    minFontSize: 5,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // child: PriceSelectorWidget(
                        //   price: p,
                        //   isSelected: p == price,
                        // ),
                      ),
                    ),
                  )
                  .toList(),
            ),
            if (addStorageOnPressed == null)
              const SizedBox(
                height: 56,
              ),
            if (addStorageOnPressed != null && extraQuantity == null)
              IconButton(
                padding: const EdgeInsets.fromLTRB(0, 14, 0, 30),
                onPressed: addStorageOnPressed,
                icon: Icon(
                  Icons.add_home_rounded,
                  color: color,
                  size: 36,
                ),
              ),
            if (extraQuantity != null)
              ExtraStorageTile(
                quantity: extraQuantity ?? 0,
                maxQuantity: maxExtraQuantity,
                onChanged: extraQuantityOnChanged,
              ),
          ],
        ),
      ),
    );
  }
}
