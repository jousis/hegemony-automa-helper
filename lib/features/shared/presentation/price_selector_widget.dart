import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class PriceSelectorWidget extends StatelessWidget {
  const PriceSelectorWidget({
    required this.price,
    required this.isSelected,
    super.key,
  });

  final int price;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black.withAlpha(43),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(4, 4, 4, 4),
        child: Container(
          decoration: BoxDecoration(
            color: isSelected ? Colors.blueGrey.shade300 : Colors.blueGrey.shade700,
          ),
          child: SizedBox(
            width: isSelected ? 32 : 14,
            height: isSelected ? 32 : 14,
            child: Center(
              child: AutoSizeText(
                '$price∀',
                minFontSize: 5,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
