import 'package:flutter/material.dart';

class SelectionCube extends StatelessWidget {
  const SelectionCube({
    required this.color,
    super.key,
  });

  final Color color;

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.8,
      heightFactor: 0.8,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: color.withAlpha(100),
          border: Border.all(
            color: color.withAlpha(164),
            width: 2,
          ),
          boxShadow: [
            BoxShadow(
              offset: const Offset(5, 5),
              color: Colors.black.withAlpha(25),
            ),
          ],
        ),
      ),
    );
  }
}
