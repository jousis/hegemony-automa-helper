import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class StackedListPopupDialogWidget extends StatelessWidget {
  const StackedListPopupDialogWidget({
    required this.items,
    super.key,
  });

  final List<String> items;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: min(
            600,
            constraints.maxWidth,
          ),
          maxHeight: min(
            800,
            constraints.maxHeight,
          ),
        ),
        child: FlutterCarousel(
          options: CarouselOptions(
            aspectRatio: 0.6,
            showIndicator: false,
            scrollDirection: Axis.vertical,
            slideIndicator: const CircularSlideIndicator(),
            viewportFraction: 0.8,
          ),
          items: items
              .mapIndexed(
                (index, item) => Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        Image.asset(item),
                        if (index == 0)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.black.withAlpha(200),
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  'Previous',
                                  style: TextStyle(
                                    fontFamily: 'Arial',
                                    color: Colors.white,
                                    fontSize: 32,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
          // items: items,
          // autoSlideDuration: const Duration(minutes: 30),
          // behavior: CarouselBehavior.consume,
          // animationDuration: const Duration(milliseconds: 200),
          // cardBuilder: (context, item, size) {
          // return ClipRRect(
          //   borderRadius: BorderRadius.circular(10.0),
          //   child: Image.asset(item),
          // );
          // },
          // innerCardsWrapper: (child) => Stack(
          //   children: [
          //     child,
          //     Positioned.fill(
          //       child: DecoratedBox(
          //         decoration: BoxDecoration(
          //           color: Colors.black.withOpacity(0.6),
          //           borderRadius: BorderRadius.circular(12),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          // emptyBuilder: (context) => const Center(
          //   child: Text(
          //     'No more cards',
          //     style: TextStyle(
          //       color: Colors.white,
          //     ),
          //   ),
          // ),
        ),
      ),
    );
  }
}
