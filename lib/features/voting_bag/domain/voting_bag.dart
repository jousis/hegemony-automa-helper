import 'package:hegemony_automa/features/shared/enums.dart';
import 'package:injectable/injectable.dart';

@singleton
class VotingBag {
  VotingBag();

  final int _maxCubes = 25;

  final List<RoleName> _cubes = [];

  int remainingCubes(RoleName role) =>
      _maxCubes -
      _cubes
          .where(
            (r) => r == role,
          )
          .length;

  Map<RoleName, int> get allRemainingCubes => _cubes.fold<Map<RoleName, int>>(
        {
          RoleName.workingClass: _maxCubes,
          RoleName.middleClass: _maxCubes,
          RoleName.capitalistClass: _maxCubes,
          RoleName.state: 0,
        },
        (remainingCubes, role) {
          remainingCubes[role] = (remainingCubes[role] ?? _maxCubes) - 1;
          return remainingCubes;
        },
      );

  void addCubes(List<RoleName> cubeList) {
    _cubes.addAll(cubeList);
    _cubes.shuffle();
  }

  bool get isEmpty => _cubes.isEmpty;

  // Will return null if there are not enough cubes
  List<RoleName>? draw(int number) {
    _cubes.shuffle();
    return _drawCubes(number);
  }

  List<RoleName>? _drawCubes(int number) {
    if (_cubes.length < number) {
      return null;
    }
    final currentDraw = <RoleName>[];
    for (var i = 0; i < number; i++) {
      currentDraw.add(_cubes.removeLast());
    }
    return currentDraw;
  }
}
