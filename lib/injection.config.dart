// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'features/ai_card/domain/i_ai_card_repository.dart' as _i4;
import 'features/ai_card/infrastructure/ai_card_repository.dart' as _i5;
import 'features/company_card/domain/i_company_card_repository.dart' as _i6;
import 'features/company_card/infrastructure/company_card_repository.dart'
    as _i7;
import 'features/game_board/application/bloc/game_board_bloc.dart' as _i15;
import 'features/game_board/domain/game_board_facade.dart' as _i14;
import 'features/game_board/domain/i_game_board_facade.dart' as _i13;
import 'features/game_board/domain/i_game_board_repository.dart' as _i8;
import 'features/game_board/insfrastructure/game_board_repository.dart' as _i9;
import 'features/instruction_card/domain/i_instruction_card_repository.dart'
    as _i10;
import 'features/instruction_card/infrastructure/instruction_card_repository.dart'
    as _i11;
import 'features/roles/capitalist_class/application/bloc/cc_bloc.dart' as _i18;
import 'features/roles/capitalist_class/domain/cc_facade.dart' as _i17;
import 'features/roles/capitalist_class/domain/i_cc_facade.dart' as _i16;
import 'features/voting_bag/domain/voting_bag.dart' as _i12;
import 'router/router.dart' as _i3; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  gh.singleton<_i3.AppRouter>(_i3.AppRouter());
  gh.lazySingleton<_i4.IAiCardRepository>(() => _i5.AiCardRepository());
  gh.lazySingleton<_i6.ICompanyCardRepository>(
      () => _i7.CompanyCardRepository());
  gh.lazySingleton<_i8.IGameBoardRepository>(() => _i9.GameBoardRepository());
  gh.lazySingleton<_i10.IInstructionCardRepository>(
      () => _i11.InstructionCardRepository());
  gh.singleton<_i12.VotingBag>(_i12.VotingBag());
  gh.lazySingleton<_i13.IGameBoardFacade>(
      () => _i14.GameBoardFacade(get<_i8.IGameBoardRepository>()));
  gh.factory<_i15.GameBoardBloc>(
      () => _i15.GameBoardBloc(get<_i13.IGameBoardFacade>()));
  gh.lazySingleton<_i16.ICcFacade>(() => _i17.CCFacade(
        get<_i13.IGameBoardFacade>(),
        get<_i6.ICompanyCardRepository>(),
        get<_i4.IAiCardRepository>(),
      ));
  gh.factory<_i18.CcBloc>(() => _i18.CcBloc(
        get<_i16.ICcFacade>(),
        get<_i13.IGameBoardFacade>(),
        get<_i10.IInstructionCardRepository>(),
        get<_i12.VotingBag>(),
      ));
  return get;
}
