import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hegemony_automa/injection.dart';
import 'package:hegemony_automa/router/router.dart';
import 'package:hegemony_automa/theme.dart';
import 'package:injectable/injectable.dart';

void main() {
  const isReleaseBuild = bool.fromEnvironment('dart.vm.product');
  configureDependencies(isReleaseBuild ? Environment.prod : Environment.dev);
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const RootWidget();
  }
}

class RootWidget extends StatefulWidget {
  const RootWidget({Key? key}) : super(key: key);

  @override
  State<RootWidget> createState() => _RootWidgetState();
}

class _RootWidgetState extends State<RootWidget> {
  final _appRouter = getIt<AppRouter>().router;

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      scrollBehavior: TouchScrollBehavior(),
      debugShowCheckedModeBanner: false,
      routerConfig: _appRouter,
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: ThemeMode.light,
// If you do not have a themeMode switch, uncomment this line
// to let the device system mode control the theme mode:
// themeMode: ThemeMode.system,
    );
  }
}

class TouchScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
        // etc.
      };
}
