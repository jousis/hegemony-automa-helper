import 'package:go_router/go_router.dart';
import 'package:hegemony_automa/router/navigator_keys.dart';
import 'package:hegemony_automa/router/routes.dart';
import 'package:injectable/injectable.dart';

@singleton
class AppRouter {
  final router = GoRouter(
    initialLocation: '/',
    navigatorKey: rootNavigatorKey,
    debugLogDiagnostics: true,
    routes: mainRoutes,
  );
}
