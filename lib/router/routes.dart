import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hegemony_automa/features/game/presentation/game_page.dart';
import 'package:hegemony_automa/features/game_board/presentation/game_board_page.dart';
import 'package:hegemony_automa/features/home/presentation/home_page.dart';
import 'package:hegemony_automa/features/roles/capitalist_class/presentation/cc_page.dart';
import 'package:hegemony_automa/features/roles/middle_class/presentation/middle_class_page.dart';
import 'package:hegemony_automa/features/roles/working_class/presentation/working_class_page.dart';
import 'package:hegemony_automa/router/navigator_keys.dart';

final List<RouteBase> mainRoutes = [
  GoRoute(
    path: '/',
    parentNavigatorKey: rootNavigatorKey,
    builder: (context, state) => const HomePage(),
  ),
  ShellRoute(
    navigatorKey: shellNavigatorKey,
    builder: (context, state, child) {
      return GamePage(
        child: child,
      );
    },
    routes: gamePageRoutes,
  ),
  // GoRoute(
  //   path: '/game',
  //   builder: (context, state) => const MainPage(),
  //   routes: [
  //   ],
  // ),
];

final List<RouteBase> gamePageRoutes = [
  GoRoute(
    path: '/game/wc',
    parentNavigatorKey: shellNavigatorKey,
    pageBuilder: (context, state) => CustomTransitionPage(
      child: const WorkingClassPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return FadeTransition(
          opacity: CurveTween(curve: Curves.easeInOutCirc).animate(animation),
          child: child,
        );
      },
    ),
    // builder: (context, state) => const WorkingClassPage(),
  ),
  GoRoute(
    path: '/game/mc',
    parentNavigatorKey: shellNavigatorKey,
    pageBuilder: (context, state) => CustomTransitionPage(
      child: const MiddleClassPage(),
      transitionDuration: const Duration(milliseconds: 500),
      reverseTransitionDuration: const Duration(milliseconds: 500),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return SlideTransition(
          position: Tween(
            begin: const Offset(-1.0, 0.0),
            end: Offset.zero,
          ).animate(animation),
          child: SlideTransition(
            position: Tween(
              begin: Offset.zero,
              end: const Offset(-1.0, 0.0),
            ).animate(secondaryAnimation),
            child: child,
          ),
        );
      },
    ),
  ),
  GoRoute(
    path: '/game/cc',
    parentNavigatorKey: shellNavigatorKey,
    pageBuilder: (context, state) => CustomTransitionPage(
      child: const CCPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return SlideTransition(
          position: Tween(begin: const Offset(1.0, 0.0), end: Offset.zero).animate(animation),
          child: child,
        );
      },
    ),
  ),
  GoRoute(
    path: '/game/policies',
    parentNavigatorKey: shellNavigatorKey,
    pageBuilder: (context, state) => CustomTransitionPage(
      child: const GameBoardPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return SlideTransition(
          position: Tween(begin: const Offset(1.0, 0.0), end: Offset.zero).animate(animation),
          child: child,
        );
      },
    ),
  ),
];
